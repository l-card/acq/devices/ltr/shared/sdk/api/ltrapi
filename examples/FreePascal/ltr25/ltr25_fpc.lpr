program ltr25_fpc;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}  
  cthreads,
  {$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, ltrapi, ltrapidefine, ltrapitypes, ltr25api, MainUnit,
  LTR25_ProcessThread
  { you can add units after this };

{$R *.res}

begin
  RequireDerivedFormResource := True;
  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.

