object MainForm: TMainForm
  Left = 278
  Top = 216
  Width = 561
  Height = 312
  Caption = 'ltreu marks'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 16
    Top = 8
    Width = 85
    Height = 13
    Caption = #1057#1087#1080#1089#1086#1082' '#1082#1088#1077#1081#1090#1086#1074':'
  end
  object lbl2: TLabel
    Left = 24
    Top = 144
    Width = 124
    Height = 13
    Caption = #1056#1077#1078#1080#1084' '#1089#1077#1082#1091#1085#1076#1085#1099#1093' '#1084#1077#1090#1086#1082
  end
  object lbl3: TLabel
    Left = 24
    Top = 208
    Width = 97
    Height = 13
    Caption = #1056#1077#1078#1080#1084' '#1084#1077#1090#1082#1080' '#1089#1090#1072#1088#1090
  end
  object btnStartSecondMarsk: TButton
    Left = 336
    Top = 144
    Width = 153
    Height = 25
    Caption = #1047#1072#1087#1091#1089#1082' '#1089#1077#1082#1091#1085#1076#1085#1099#1093' '#1084#1077#1090#1086#1082
    TabOrder = 0
    OnClick = btnStartSecondMarskClick
  end
  object cbbSecondMarkMode: TComboBox
    Left = 176
    Top = 144
    Width = 153
    Height = 21
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 1
    Text = #1042#1085#1091#1090#1088#1077#1085#1085#1103#1103
    Items.Strings = (
      #1042#1085#1091#1090#1088#1077#1085#1085#1103#1103
      #1060#1088#1086#1085#1090' DIGIN1'
      #1057#1087#1072#1076' DIGIN1'
      #1060#1088#1086#1085#1090' DIGIN2'
      #1057#1087#1072#1076' DIGIN2')
  end
  object btnStopSecondMarks: TButton
    Left = 336
    Top = 176
    Width = 153
    Height = 25
    Caption = #1054#1089#1090#1072#1085#1086#1074' '#1089#1077#1082#1091#1085#1076#1085#1099#1093' '#1084#1077#1090#1086#1082
    TabOrder = 2
    OnClick = btnStopSecondMarksClick
  end
  object lstCrates: TListBox
    Left = 16
    Top = 24
    Width = 313
    Height = 113
    ItemHeight = 13
    TabOrder = 3
  end
  object btnRefreshCratesList: TButton
    Left = 336
    Top = 24
    Width = 153
    Height = 25
    Caption = #1054#1073#1085#1086#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082' '#1082#1088#1077#1081#1090#1086#1074
    TabOrder = 4
    OnClick = btnRefreshCratesListClick
  end
  object cbbStartMarkMode: TComboBox
    Left = 176
    Top = 208
    Width = 153
    Height = 21
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 5
    Text = #1042#1085#1091#1090#1088#1077#1085#1085#1103#1103
    Items.Strings = (
      #1042#1085#1091#1090#1088#1077#1085#1085#1103#1103
      #1060#1088#1086#1085#1090' DIGIN1'
      #1057#1087#1072#1076' DIGIN1'
      #1060#1088#1086#1085#1090' DIGIN2'
      #1057#1087#1072#1076' DIGIN2')
  end
  object btnMakeStartMark: TButton
    Left = 336
    Top = 208
    Width = 153
    Height = 25
    Caption = #1043#1077#1085#1077#1088#1072#1094#1080#1103' '#1084#1077#1090#1082#1080' '#1057#1058#1040#1056#1058
    TabOrder = 6
    OnClick = btnMakeStartMarkClick
  end
end
