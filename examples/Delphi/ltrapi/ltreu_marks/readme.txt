������ ������ ������������� ������ ������� ltrapi ��� ��������� ������ �������
� ���������� ������� ����� � ������� ������� LTR-EU, LTR-CU, LTR-CEU �� ����� Delphi.

������ �������� ���� ������� ��� ����� "Delphi 7" (ltreu_marks_delphi.dpr) � ���
����� "Embarcadero RAD Studio" (ltreu_marks_delphi.dproj).

� ������� ���������� ������� ���� � ������ ltrapi.pas, ltrapidefine.pas,
ltrapitypes.pas ����� ������, ������� ��������������� ������
� ������������ ltr � ���������� ��� ������ ������� �������
(LTR_INSTALL_DIR/include/pascal2).

���� ����� ������ ��������� �������:
    Delphi 7    - "Project->Options->Directories/Conditionals->Search path",
    RAD Studio  - "Project->Options->Delphi Compiler->Search path"