object Form1: TForm1
  Left = 504
  Top = 162
  Caption = #1059#1087#1088#1072#1074#1083#1077#1085#1080#1077' IP-'#1072#1076#1088#1077#1089#1072#1084#1080
  ClientHeight = 317
  ClientWidth = 524
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object btnGetIpList: TButton
    Left = 16
    Top = 280
    Width = 145
    Height = 25
    Caption = #1054#1073#1085#1086#1074#1080#1090#1100' '#1072#1076#1088#1077#1089#1072
    TabOrder = 0
    OnClick = btnGetIpListClick
  end
  object lstIpAddr: TListBox
    Left = 8
    Top = 81
    Width = 508
    Height = 193
    ItemHeight = 13
    TabOrder = 1
  end
  object btnIpRem: TButton
    Left = 176
    Top = 280
    Width = 113
    Height = 25
    Caption = #1059#1076#1072#1083#1077#1085#1080#1077' '#1072#1076#1088#1077#1089#1072
    TabOrder = 2
    OnClick = btnIpRemClick
  end
  object btnIpConnect: TButton
    Left = 296
    Top = 280
    Width = 117
    Height = 25
    Caption = #1055#1086#1076#1082#1083#1102#1095#1080#1090#1100
    TabOrder = 3
    OnClick = btnIpConnectClick
  end
  object btnIpDisconnect: TButton
    Left = 419
    Top = 280
    Width = 97
    Height = 25
    Caption = #1054#1090#1082#1083#1102#1095#1080#1090#1100
    TabOrder = 4
    OnClick = btnIpDisconnectClick
  end
  object GroupBox1: TGroupBox
    Left = 16
    Top = 8
    Width = 500
    Height = 66
    Caption = #1053#1086#1074#1072#1103' '#1079#1072#1087#1080#1089#1100
    TabOrder = 5
    object btnAddIpAddr: TButton
      Left = 3
      Top = 16
      Width = 145
      Height = 25
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      TabOrder = 0
      OnClick = btnAddIpAddrClick
    end
    object edtNewIpAddr: TEdit
      Left = 154
      Top = 18
      Width = 137
      Height = 21
      TabOrder = 1
    end
    object chkNewIpAuto: TCheckBox
      Left = 310
      Top = 16
      Width = 180
      Height = 17
      Caption = #1040#1074#1090#1086#1087#1086#1076#1082#1083#1102#1095#1077#1085#1080#1077' '#1087#1088#1080' '#1089#1090#1072#1088#1090#1077
      TabOrder = 2
    end
    object chkNewIpRecon: TCheckBox
      Left = 310
      Top = 39
      Width = 180
      Height = 17
      Caption = #1055#1077#1088#1077#1087#1086#1076#1082#1083#1102#1095#1077#1085#1080#1077' '#1087#1086' '#1086#1096#1080#1073#1082#1077
      TabOrder = 3
    end
  end
end
