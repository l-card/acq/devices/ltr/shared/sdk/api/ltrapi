������ ������� ������ ��������� ltr �� ���� 1.31.1!

������ ������ ������������� ������ ������� ltrapi ��� ���������� �������� IP-������� ������� �� ����� Delphi.
������ �������� ���� ������� ��� ����� "Delphi 7" (ipctlprj.dpr) � ���
����� "Embarcadero RAD Studio" (ipctlprj.dproj).

� ������� ���������� ������� ���� � ������ ltrapi.pas, ltrapidefine.pas,
ltrapitypes.pas ����� ������, ������� ��������������� ������
� ������������ ltr � ���������� ��� ������ ������� �������
(LTR_INSTALL_DIR/include/pascal2).

���� ����� ������ ��������� �������:
    Delphi 7    - "Project->Options->Directories/Conditionals->Search path",
    RAD Studio  - "Project->Options->Delphi Compiler->Search path"