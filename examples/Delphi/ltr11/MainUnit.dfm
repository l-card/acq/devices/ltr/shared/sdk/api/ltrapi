object MainForm: TMainForm
  Left = 269
  Top = 174
  Caption = 'ltr11 example'
  ClientHeight = 468
  ClientWidth = 580
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object grpDevInfo: TGroupBox
    Left = 272
    Top = 56
    Width = 273
    Height = 81
    Caption = #1048#1085#1092#1086#1088#1084#1072#1094#1080#1103' '#1086' '#1084#1086#1076#1091#1083#1077
    TabOrder = 0
    object lblDevSerial: TLabel
      Left = 16
      Top = 24
      Width = 84
      Height = 13
      Caption = #1057#1077#1088#1080#1081#1085#1099#1081' '#1085#1086#1084#1077#1088
    end
    object lblVerPld: TLabel
      Left = 16
      Top = 48
      Width = 88
      Height = 13
      Caption = #1042#1077#1088#1089#1080#1103' '#1087#1088#1086#1096#1080#1074#1082#1080
    end
    object edtDevSerial: TEdit
      Left = 112
      Top = 24
      Width = 145
      Height = 21
      ReadOnly = True
      TabOrder = 0
    end
    object edtVerFirm: TEdit
      Left = 112
      Top = 48
      Width = 145
      Height = 21
      ReadOnly = True
      TabOrder = 1
    end
  end
  object cbbModulesList: TComboBox
    Left = 272
    Top = 16
    Width = 273
    Height = 21
    TabOrder = 1
  end
  object btnRefreshDevList: TButton
    Left = 32
    Top = 16
    Width = 185
    Height = 25
    Caption = #1054#1073#1085#1086#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082
    TabOrder = 2
    OnClick = btnRefreshDevListClick
  end
  object btnOpen: TButton
    Left = 32
    Top = 73
    Width = 185
    Height = 25
    Caption = #1054#1090#1082#1088#1099#1090#1100' '#1091#1089#1090#1088#1086#1081#1089#1090#1074#1086
    TabOrder = 3
    OnClick = btnOpenClick
  end
  object btnStart: TButton
    Left = 32
    Top = 104
    Width = 185
    Height = 25
    Caption = #1047#1072#1087#1091#1089#1082' '#1089#1073#1086#1088#1072' '#1076#1072#1085#1085#1099#1093
    TabOrder = 4
    OnClick = btnStartClick
  end
  object btnStop: TButton
    Left = 32
    Top = 136
    Width = 185
    Height = 25
    Caption = #1054#1089#1090#1072#1085#1086#1074' '#1089#1073#1086#1088#1072' '#1076#1072#1085#1085#1099#1093
    Enabled = False
    TabOrder = 5
    OnClick = btnStopClick
  end
  object grp1: TGroupBox
    Left = 32
    Top = 168
    Width = 505
    Height = 89
    Caption = #1063#1072#1089#1090#1086#1090#1072' '#1040#1062#1055
    TabOrder = 6
    object lbl1: TLabel
      Left = 12
      Top = 19
      Width = 68
      Height = 13
      Caption = #1055#1088#1077#1076#1077#1083#1080#1090#1077#1083#1100
    end
    object lbl2: TLabel
      Left = 159
      Top = 20
      Width = 50
      Height = 13
      Caption = #1044#1077#1083#1080#1090#1077#1083#1100
    end
    object lbl3: TLabel
      Left = 272
      Top = 59
      Width = 86
      Height = 13
      Caption = #1063#1072#1089#1090#1086#1090#1072' '#1040#1062#1055', '#1043#1094
    end
    object lbl4: TLabel
      Left = 10
      Top = 59
      Width = 109
      Height = 13
      Caption = #1063#1072#1089#1090#1086#1090#1072' '#1085#1072' '#1082#1072#1085#1072#1083', '#1043#1094
    end
    object cbbAdcPrescaler: TComboBox
      Left = 87
      Top = 16
      Width = 57
      Height = 21
      ItemIndex = 0
      TabOrder = 0
      Text = '1'
      Items.Strings = (
        '1'
        '8'
        '64'
        '256'
        '1024')
    end
    object edtAdcFreq: TEdit
      Left = 368
      Top = 56
      Width = 121
      Height = 21
      TabOrder = 1
      Text = '400000'
    end
    object btnFindAdcFreq: TButton
      Left = 373
      Top = 14
      Width = 91
      Height = 25
      Caption = #1055#1086#1076#1086#1073#1088#1072#1090#1100
      TabOrder = 2
      OnClick = btnFindAdcFreqClick
    end
    object seAdcDivider: TSpinEdit
      Left = 217
      Top = 16
      Width = 121
      Height = 22
      MaxValue = 65535
      MinValue = 2
      TabOrder = 3
      Value = 36
    end
  end
  object grp2: TGroupBox
    Left = 32
    Top = 272
    Width = 513
    Height = 153
    Caption = #1050#1072#1085#1072#1083#1099
    TabOrder = 7
    object lbl5: TLabel
      Left = 8
      Top = 40
      Width = 31
      Height = 13
      Caption = #1050#1072#1085#1072#1083
    end
    object lbl6: TLabel
      Left = 64
      Top = 40
      Width = 49
      Height = 13
      Caption = #1044#1080#1072#1087#1072#1079#1086#1085
    end
    object lbl7: TLabel
      Left = 152
      Top = 40
      Width = 88
      Height = 13
      Caption = #1056#1077#1078#1080#1084' '#1080#1079#1084#1077#1088#1077#1085#1080#1103
    end
    object lbl8: TLabel
      Left = 336
      Top = 40
      Width = 53
      Height = 13
      Caption = #1056#1077#1079#1091#1083#1100#1090#1072#1090
    end
    object lbl9: TLabel
      Left = 16
      Top = 16
      Width = 166
      Height = 13
      Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1083#1086#1075#1080#1095#1077#1089#1082#1080#1093' '#1082#1072#1085#1072#1083#1086#1074
    end
    object cbbLCh1_Channel: TComboBox
      Left = 8
      Top = 56
      Width = 41
      Height = 21
      ItemIndex = 0
      TabOrder = 0
      Text = '1'
      Items.Strings = (
        '1'
        '2'
        '3'
        '4'
        '5'
        '6'
        '7'
        '8'
        '9'
        '10'
        '11'
        '12'
        '13'
        '14'
        '15'
        '16'
        '17'
        '18'
        '19'
        '20'
        '21'
        '22'
        '23'
        '24'
        '25'
        '26'
        '27'
        '28'
        '29'
        '30'
        '31'
        '32')
    end
    object cbbLCh1_Range: TComboBox
      Left = 64
      Top = 56
      Width = 57
      Height = 21
      ItemIndex = 0
      TabOrder = 1
      Text = '10 '#1042
      Items.Strings = (
        '10 '#1042
        '2.5 '#1042
        '0.625 '#1042
        '0.156 '#1042)
    end
    object cbbLCh1_Mode: TComboBox
      Left = 136
      Top = 56
      Width = 169
      Height = 21
      ItemIndex = 0
      TabOrder = 2
      Text = #1044#1080#1092#1092#1077#1088#1077#1085#1094#1080#1072#1083#1100#1085#1099#1081
      Items.Strings = (
        #1044#1080#1092#1092#1077#1088#1077#1085#1094#1080#1072#1083#1100#1085#1099#1081
        #1057' '#1086#1073#1097#1077#1081' '#1079#1077#1084#1083#1077#1081
        #1048#1079#1084#1077#1088#1077#1085#1080#1077' '#1085#1091#1083#1103)
    end
    object edtLCh1_Result: TEdit
      Left = 328
      Top = 56
      Width = 121
      Height = 21
      TabOrder = 3
    end
    object cbbLCh2_Channel: TComboBox
      Left = 8
      Top = 80
      Width = 41
      Height = 21
      ItemIndex = 1
      TabOrder = 4
      Text = '2'
      Items.Strings = (
        '1'
        '2'
        '3'
        '4'
        '5'
        '6'
        '7'
        '8'
        '9'
        '10'
        '11'
        '12'
        '13'
        '14'
        '15'
        '16'
        '17'
        '18'
        '19'
        '20'
        '21'
        '22'
        '23'
        '24'
        '25'
        '26'
        '27'
        '28'
        '29'
        '30'
        '31'
        '32')
    end
    object cbbLCh2_Range: TComboBox
      Left = 64
      Top = 80
      Width = 57
      Height = 21
      ItemIndex = 0
      TabOrder = 5
      Text = '10 '#1042
      Items.Strings = (
        '10 '#1042
        '2.5 '#1042
        '0.625 '#1042
        '0.156 '#1042)
    end
    object cbbLCh2_Mode: TComboBox
      Left = 136
      Top = 80
      Width = 169
      Height = 21
      ItemIndex = 0
      TabOrder = 6
      Text = #1044#1080#1092#1092#1077#1088#1077#1085#1094#1080#1072#1083#1100#1085#1099#1081
      Items.Strings = (
        #1044#1080#1092#1092#1077#1088#1077#1085#1094#1080#1072#1083#1100#1085#1099#1081
        #1057' '#1086#1073#1097#1077#1081' '#1079#1077#1084#1083#1077#1081
        #1048#1079#1084#1077#1088#1077#1085#1080#1077' '#1085#1091#1083#1103)
    end
    object edtLCh2_Result: TEdit
      Left = 328
      Top = 80
      Width = 121
      Height = 21
      TabOrder = 7
    end
    object cbbLCh3_Channel: TComboBox
      Left = 8
      Top = 104
      Width = 41
      Height = 21
      ItemIndex = 2
      TabOrder = 8
      Text = '3'
      Items.Strings = (
        '1'
        '2'
        '3'
        '4'
        '5'
        '6'
        '7'
        '8'
        '9'
        '10'
        '11'
        '12'
        '13'
        '14'
        '15'
        '16'
        '17'
        '18'
        '19'
        '20'
        '21'
        '22'
        '23'
        '24'
        '25'
        '26'
        '27'
        '28'
        '29'
        '30'
        '31'
        '32')
    end
    object cbbLCh3_Range: TComboBox
      Left = 64
      Top = 104
      Width = 57
      Height = 21
      ItemIndex = 0
      TabOrder = 9
      Text = '10 '#1042
      Items.Strings = (
        '10 '#1042
        '2.5 '#1042
        '0.625 '#1042
        '0.156 '#1042)
    end
    object cbbLCh3_Mode: TComboBox
      Left = 136
      Top = 104
      Width = 169
      Height = 21
      ItemIndex = 0
      TabOrder = 10
      Text = #1044#1080#1092#1092#1077#1088#1077#1085#1094#1080#1072#1083#1100#1085#1099#1081
      Items.Strings = (
        #1044#1080#1092#1092#1077#1088#1077#1085#1094#1080#1072#1083#1100#1085#1099#1081
        #1057' '#1086#1073#1097#1077#1081' '#1079#1077#1084#1083#1077#1081
        #1048#1079#1084#1077#1088#1077#1085#1080#1077' '#1085#1091#1083#1103)
    end
    object edtLCh3_Result: TEdit
      Left = 328
      Top = 104
      Width = 121
      Height = 21
      TabOrder = 11
    end
    object seLChCnt: TSpinEdit
      Left = 184
      Top = 11
      Width = 121
      Height = 22
      MaxValue = 3
      MinValue = 1
      TabOrder = 12
      Value = 3
    end
  end
  object edtChFreq: TEdit
    Left = 168
    Top = 224
    Width = 121
    Height = 21
    Enabled = False
    TabOrder = 8
  end
end
