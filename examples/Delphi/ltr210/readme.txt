������ ������ ������������� ������ � ������� LTR210 �� ����� Delphi.
������ �������� ���� ������� ��� ����� "Delphi 7" (ltr210_delphi.dpr) � ���
����� "Embarcadero RAD Studio" (ltr210_delphi.dproj).

� ������� ���������� ������� ���� � ������ ltrapi.pas, ltrapidefine.pas,
ltrapitypes.pas, ltr210api.pas ����� ������, ������� ��������������� ������
� ������������ ltr � ���������� ��� ������ ������� �������
(LTR_INSTALL_DIR/include/pascal2).

���� ����� ������ ��������� �������:
    Delphi 7    - "Project->Options->Directories/Conditionals->Search path",
    RAD Studio  - "Project->Options->Delphi Compiler->Search path"

������ ������������� � ������ ������� ��� ����� �������� � ������� LTR210,
� �� ��� ����� ��������������� � ����� Delphi.

������ ������������� ���������� ����� � ���������� ������.
����� ������ ����������� � ��������� ������, ������������� � ������ TLTR210_ProcessThread.
�� ������ ���������� �������� �������� ������� � ��� ������� ��������� ������� ��������
�� ������� ������.