object MainForm: TMainForm
  Left = 502
  Top = 175
  Width = 591
  Height = 458
  Caption = 'ltr35 cycle'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lblFreq: TLabel
    Left = 216
    Top = 184
    Width = 61
    Height = 13
    Caption = #1063#1072#1089#1090#1086#1090#1072', '#1043#1094
  end
  object lblAmp: TLabel
    Left = 300
    Top = 184
    Width = 69
    Height = 13
    Caption = #1040#1084#1087#1083#1080#1090#1091#1076#1072', '#1042
  end
  object lblOffs: TLabel
    Left = 390
    Top = 184
    Width = 65
    Height = 13
    Caption = #1057#1084#1077#1097#1077#1085#1080#1077', '#1042
  end
  object lblOutput: TLabel
    Left = 486
    Top = 184
    Width = 33
    Height = 13
    Caption = #1042#1099#1093#1086#1076
  end
  object lbl1: TLabel
    Left = 112
    Top = 184
    Width = 36
    Height = 13
    Caption = #1057#1080#1075#1085#1072#1083
  end
  object cbbModulesList: TComboBox
    Left = 272
    Top = 16
    Width = 273
    Height = 21
    ItemHeight = 13
    TabOrder = 0
  end
  object grpDevInfo: TGroupBox
    Left = 272
    Top = 48
    Width = 273
    Height = 121
    Caption = #1048#1085#1092#1086#1088#1084#1072#1094#1080#1103' '#1086' '#1084#1086#1076#1091#1083#1077
    TabOrder = 1
    object lblDevSerial: TLabel
      Left = 16
      Top = 24
      Width = 84
      Height = 13
      Caption = #1057#1077#1088#1080#1081#1085#1099#1081' '#1085#1086#1084#1077#1088
    end
    object lblVerPld: TLabel
      Left = 16
      Top = 48
      Width = 71
      Height = 13
      Caption = #1052#1086#1076#1080#1092#1080#1082#1072#1094#1080#1103
    end
    object lblPldVer: TLabel
      Left = 32
      Top = 72
      Width = 56
      Height = 13
      Caption = #1042#1077#1088#1089#1080#1103' PLD'
    end
    object lblVerFPGA: TLabel
      Left = 32
      Top = 96
      Width = 64
      Height = 13
      Caption = #1042#1077#1088#1089#1080#1103' FPGA'
    end
    object edtDevSerial: TEdit
      Left = 112
      Top = 24
      Width = 145
      Height = 21
      ReadOnly = True
      TabOrder = 0
    end
    object edtModification: TEdit
      Left = 112
      Top = 48
      Width = 145
      Height = 21
      ReadOnly = True
      TabOrder = 1
    end
    object edtVerPld: TEdit
      Left = 112
      Top = 72
      Width = 145
      Height = 21
      ReadOnly = True
      TabOrder = 2
    end
    object edtVerFPGA: TEdit
      Left = 112
      Top = 96
      Width = 145
      Height = 21
      ReadOnly = True
      TabOrder = 3
    end
  end
  object btnRefreshDevList: TButton
    Left = 32
    Top = 16
    Width = 185
    Height = 25
    Caption = #1054#1073#1085#1086#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082
    TabOrder = 2
    OnClick = btnRefreshDevListClick
  end
  object btnOpen: TButton
    Left = 32
    Top = 73
    Width = 185
    Height = 25
    Caption = #1054#1090#1082#1088#1099#1090#1100' '#1091#1089#1090#1088#1086#1081#1089#1090#1074#1086
    TabOrder = 3
    OnClick = btnOpenClick
  end
  object cbbCh1Signal: TComboBox
    Left = 112
    Top = 208
    Width = 89
    Height = 21
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 4
    Text = #1057#1080#1085#1091#1089
    Items.Strings = (
      #1057#1080#1085#1091#1089)
  end
  object chkCh1En: TCheckBox
    Left = 32
    Top = 210
    Width = 73
    Height = 17
    Caption = #1050#1072#1085#1072#1083' 1'
    Checked = True
    State = cbChecked
    TabOrder = 5
  end
  object edtCh1Freq: TEdit
    Left = 208
    Top = 208
    Width = 73
    Height = 21
    TabOrder = 6
    Text = '96000'
  end
  object edtCh1Amp: TEdit
    Left = 296
    Top = 208
    Width = 73
    Height = 21
    TabOrder = 7
    Text = '10'
  end
  object cbbCh1Out: TComboBox
    Left = 472
    Top = 208
    Width = 73
    Height = 21
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 8
    Text = '1:1'
    Items.Strings = (
      '1:1'
      '1:5/1:10')
  end
  object chkCh2En: TCheckBox
    Left = 32
    Top = 234
    Width = 73
    Height = 17
    Caption = #1050#1072#1085#1072#1083' 2'
    Checked = True
    State = cbChecked
    TabOrder = 9
  end
  object cbbCh2Signal: TComboBox
    Left = 112
    Top = 232
    Width = 89
    Height = 21
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 10
    Text = #1057#1080#1085#1091#1089
    Items.Strings = (
      #1057#1080#1085#1091#1089)
  end
  object edtCh2Freq: TEdit
    Left = 208
    Top = 232
    Width = 73
    Height = 21
    TabOrder = 11
    Text = '64000'
  end
  object edtCh2Amp: TEdit
    Left = 296
    Top = 232
    Width = 73
    Height = 21
    TabOrder = 12
    Text = '9'
  end
  object cbbCh2Out: TComboBox
    Left = 472
    Top = 232
    Width = 73
    Height = 21
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 13
    Text = '1:1'
    Items.Strings = (
      '1:1'
      '1:5/1:10')
  end
  object edtCh1Offs: TEdit
    Left = 392
    Top = 208
    Width = 73
    Height = 21
    TabOrder = 14
    Text = '0'
  end
  object edtCh2Off: TEdit
    Left = 392
    Top = 232
    Width = 73
    Height = 21
    TabOrder = 15
    Text = '0'
  end
  object chkCh3En: TCheckBox
    Left = 32
    Top = 258
    Width = 73
    Height = 17
    Caption = #1050#1072#1085#1072#1083' 3'
    Checked = True
    State = cbChecked
    TabOrder = 16
  end
  object cbbCh3Signal: TComboBox
    Left = 112
    Top = 256
    Width = 89
    Height = 21
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 17
    Text = #1057#1080#1085#1091#1089
    Items.Strings = (
      #1057#1080#1085#1091#1089)
  end
  object edtCh3Freq: TEdit
    Left = 208
    Top = 256
    Width = 73
    Height = 21
    TabOrder = 18
    Text = '48000'
  end
  object edtCh3Amp: TEdit
    Left = 296
    Top = 256
    Width = 73
    Height = 21
    TabOrder = 19
    Text = '8'
  end
  object edtCh3Offs: TEdit
    Left = 392
    Top = 256
    Width = 73
    Height = 21
    TabOrder = 20
    Text = '0'
  end
  object cbbCh3Out: TComboBox
    Left = 472
    Top = 256
    Width = 73
    Height = 21
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 21
    Text = '1:1'
    Items.Strings = (
      '1:1'
      '1:5/1:10')
  end
  object chkCh4En: TCheckBox
    Left = 32
    Top = 282
    Width = 73
    Height = 17
    Caption = #1050#1072#1085#1072#1083' 4'
    Checked = True
    State = cbChecked
    TabOrder = 22
  end
  object cbbCh4Signal: TComboBox
    Left = 112
    Top = 280
    Width = 89
    Height = 21
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 23
    Text = #1057#1080#1085#1091#1089
    Items.Strings = (
      #1057#1080#1085#1091#1089)
  end
  object edtCh4Freq: TEdit
    Left = 208
    Top = 280
    Width = 73
    Height = 21
    TabOrder = 24
    Text = '38400'
  end
  object edtCh4Amp: TEdit
    Left = 296
    Top = 280
    Width = 73
    Height = 21
    TabOrder = 25
    Text = '7'
  end
  object edtCh4Offs: TEdit
    Left = 392
    Top = 280
    Width = 73
    Height = 21
    TabOrder = 26
    Text = '0'
  end
  object cbbCh4Out: TComboBox
    Left = 472
    Top = 280
    Width = 73
    Height = 21
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 27
    Text = '1:1'
    Items.Strings = (
      '1:1'
      '1:5/1:10')
  end
  object chkCh5En: TCheckBox
    Left = 32
    Top = 306
    Width = 73
    Height = 17
    Caption = #1050#1072#1085#1072#1083' 5'
    Checked = True
    State = cbChecked
    TabOrder = 28
  end
  object cbbCh5Signal: TComboBox
    Left = 112
    Top = 304
    Width = 89
    Height = 21
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 29
    Text = #1057#1080#1085#1091#1089
    Items.Strings = (
      #1057#1080#1085#1091#1089)
  end
  object edtCh5Freq: TEdit
    Left = 208
    Top = 304
    Width = 73
    Height = 21
    TabOrder = 30
    Text = '32000'
  end
  object edtCh5Amp: TEdit
    Left = 296
    Top = 304
    Width = 73
    Height = 21
    TabOrder = 31
    Text = '6'
  end
  object edtCh5Offs: TEdit
    Left = 392
    Top = 304
    Width = 73
    Height = 21
    TabOrder = 32
    Text = '0'
  end
  object cbbCh5Out: TComboBox
    Left = 472
    Top = 304
    Width = 73
    Height = 21
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 33
    Text = '1:1'
    Items.Strings = (
      '1:1'
      '1:5/1:10')
  end
  object chkCh6En: TCheckBox
    Left = 32
    Top = 330
    Width = 73
    Height = 17
    Caption = #1050#1072#1085#1072#1083' 6'
    Checked = True
    State = cbChecked
    TabOrder = 34
  end
  object cbbCh6Signal: TComboBox
    Left = 112
    Top = 328
    Width = 89
    Height = 21
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 35
    Text = #1057#1080#1085#1091#1089
    Items.Strings = (
      #1057#1080#1085#1091#1089)
  end
  object edtCh6Freq: TEdit
    Left = 208
    Top = 328
    Width = 73
    Height = 21
    TabOrder = 36
    Text = '24000'
  end
  object edtCh6Amp: TEdit
    Left = 296
    Top = 328
    Width = 73
    Height = 21
    TabOrder = 37
    Text = '5'
  end
  object edtCh6Offs: TEdit
    Left = 392
    Top = 328
    Width = 73
    Height = 21
    TabOrder = 38
    Text = '0'
  end
  object cbbCh6Out: TComboBox
    Left = 472
    Top = 328
    Width = 73
    Height = 21
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 39
    Text = '1:1'
    Items.Strings = (
      '1:1'
      '1:5/1:10')
  end
  object chkCh7En: TCheckBox
    Left = 32
    Top = 354
    Width = 73
    Height = 17
    Caption = #1050#1072#1085#1072#1083' 7'
    Checked = True
    State = cbChecked
    TabOrder = 40
  end
  object cbbCh7Signal: TComboBox
    Left = 112
    Top = 352
    Width = 89
    Height = 21
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 41
    Text = #1057#1080#1085#1091#1089
    Items.Strings = (
      #1057#1080#1085#1091#1089)
  end
  object edtCh7Freq: TEdit
    Left = 208
    Top = 352
    Width = 73
    Height = 21
    TabOrder = 42
    Text = '19200'
  end
  object edtCh7Amp: TEdit
    Left = 296
    Top = 352
    Width = 73
    Height = 21
    TabOrder = 43
    Text = '4'
  end
  object edtCh7Offs: TEdit
    Left = 392
    Top = 352
    Width = 73
    Height = 21
    TabOrder = 44
    Text = '0'
  end
  object cbbCh7Out: TComboBox
    Left = 472
    Top = 352
    Width = 73
    Height = 21
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 45
    Text = '1:1'
    Items.Strings = (
      '1:1'
      '1:5/1:10')
  end
  object chkCh8En: TCheckBox
    Left = 32
    Top = 378
    Width = 73
    Height = 17
    Caption = #1050#1072#1085#1072#1083' 8'
    Checked = True
    State = cbChecked
    TabOrder = 46
  end
  object cbbCh8Signal: TComboBox
    Left = 112
    Top = 376
    Width = 89
    Height = 21
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 47
    Text = #1057#1080#1085#1091#1089
    Items.Strings = (
      #1057#1080#1085#1091#1089)
  end
  object edtCh8Freq: TEdit
    Left = 208
    Top = 376
    Width = 73
    Height = 21
    TabOrder = 48
    Text = '16000'
  end
  object edtCh8Amp: TEdit
    Left = 296
    Top = 376
    Width = 73
    Height = 21
    TabOrder = 49
    Text = '3'
  end
  object edtCh8Offs: TEdit
    Left = 392
    Top = 376
    Width = 73
    Height = 21
    TabOrder = 50
    Text = '0'
  end
  object cbbCh8Out: TComboBox
    Left = 472
    Top = 376
    Width = 73
    Height = 21
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 51
    Text = '1:1'
    Items.Strings = (
      '1:1'
      '1:5/1:10')
  end
  object btnSetSignal: TButton
    Left = 32
    Top = 105
    Width = 185
    Height = 25
    Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1089#1080#1075#1085#1072#1083
    TabOrder = 52
    OnClick = btnSetSignalClick
  end
  object btnStopGen: TButton
    Left = 32
    Top = 136
    Width = 185
    Height = 25
    Caption = #1054#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1075#1077#1085#1077#1088#1072#1094#1080#1102' '#1089#1080#1075#1085#1072#1083#1072
    TabOrder = 53
    OnClick = btnStopGenClick
  end
end
