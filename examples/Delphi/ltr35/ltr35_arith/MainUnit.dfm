object MainForm: TMainForm
  Left = 502
  Top = 175
  Width = 646
  Height = 654
  Caption = 'ltr35 arith'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lblFreq: TLabel
    Left = 96
    Top = 192
    Width = 61
    Height = 13
    Caption = #1063#1072#1089#1090#1086#1090#1072', '#1043#1094
  end
  object lblAmp: TLabel
    Left = 300
    Top = 328
    Width = 69
    Height = 13
    Caption = #1040#1084#1087#1083#1080#1090#1091#1076#1072', '#1042
  end
  object lblOffs: TLabel
    Left = 398
    Top = 328
    Width = 65
    Height = 13
    Caption = #1057#1084#1077#1097#1077#1085#1080#1077', '#1042
  end
  object lblOutput: TLabel
    Left = 486
    Top = 328
    Width = 33
    Height = 13
    Caption = #1042#1099#1093#1086#1076
  end
  object Label1: TLabel
    Left = 24
    Top = 216
    Width = 63
    Height = 13
    Caption = #1043#1077#1085#1077#1088#1072#1090#1086#1088' 1'
  end
  object lbl1: TLabel
    Left = 176
    Top = 192
    Width = 114
    Height = 13
    Caption = #1053#1072#1095#1072#1083#1100#1085#1072#1103' '#1092#1072#1079#1072', '#1075#1088#1072#1076
  end
  object lbl2: TLabel
    Left = 24
    Top = 240
    Width = 63
    Height = 13
    Caption = #1043#1077#1085#1077#1088#1072#1090#1086#1088' 2'
  end
  object lbl3: TLabel
    Left = 24
    Top = 264
    Width = 63
    Height = 13
    Caption = #1043#1077#1085#1077#1088#1072#1090#1086#1088' 3'
  end
  object lbl4: TLabel
    Left = 24
    Top = 288
    Width = 63
    Height = 13
    Caption = #1043#1077#1085#1077#1088#1072#1090#1086#1088' 4'
  end
  object lbl5: TLabel
    Left = 120
    Top = 328
    Width = 91
    Height = 13
    Caption = #1048#1089#1090#1086#1095#1085#1080#1082' '#1089#1080#1075#1085#1072#1083#1072
  end
  object cbbModulesList: TComboBox
    Left = 272
    Top = 16
    Width = 273
    Height = 21
    ItemHeight = 13
    TabOrder = 0
  end
  object grpDevInfo: TGroupBox
    Left = 272
    Top = 48
    Width = 273
    Height = 121
    Caption = #1048#1085#1092#1086#1088#1084#1072#1094#1080#1103' '#1086' '#1084#1086#1076#1091#1083#1077
    TabOrder = 1
    object lblDevSerial: TLabel
      Left = 16
      Top = 24
      Width = 84
      Height = 13
      Caption = #1057#1077#1088#1080#1081#1085#1099#1081' '#1085#1086#1084#1077#1088
    end
    object lblVerPld: TLabel
      Left = 16
      Top = 48
      Width = 71
      Height = 13
      Caption = #1052#1086#1076#1080#1092#1080#1082#1072#1094#1080#1103
    end
    object lblPldVer: TLabel
      Left = 32
      Top = 72
      Width = 56
      Height = 13
      Caption = #1042#1077#1088#1089#1080#1103' PLD'
    end
    object lblVerFPGA: TLabel
      Left = 32
      Top = 96
      Width = 64
      Height = 13
      Caption = #1042#1077#1088#1089#1080#1103' FPGA'
    end
    object edtDevSerial: TEdit
      Left = 112
      Top = 24
      Width = 145
      Height = 21
      ReadOnly = True
      TabOrder = 0
    end
    object edtModification: TEdit
      Left = 112
      Top = 48
      Width = 145
      Height = 21
      ReadOnly = True
      TabOrder = 1
    end
    object edtVerPld: TEdit
      Left = 112
      Top = 72
      Width = 145
      Height = 21
      ReadOnly = True
      TabOrder = 2
    end
    object edtVerFPGA: TEdit
      Left = 112
      Top = 96
      Width = 145
      Height = 21
      ReadOnly = True
      TabOrder = 3
    end
  end
  object btnRefreshDevList: TButton
    Left = 32
    Top = 16
    Width = 185
    Height = 25
    Caption = #1054#1073#1085#1086#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082
    TabOrder = 2
    OnClick = btnRefreshDevListClick
  end
  object btnOpen: TButton
    Left = 32
    Top = 73
    Width = 185
    Height = 25
    Caption = #1054#1090#1082#1088#1099#1090#1100' '#1091#1089#1090#1088#1086#1081#1089#1090#1074#1086
    TabOrder = 3
    OnClick = btnOpenClick
  end
  object cbbCh1Src: TComboBox
    Left = 120
    Top = 352
    Width = 161
    Height = 21
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 4
    Text = #1043#1077#1085#1077#1088#1072#1090#1086#1088' 1. '#1057#1080#1085#1091#1089
    Items.Strings = (
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 1. '#1057#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 1. '#1050#1086#1089#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 2. '#1057#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 2. '#1050#1086#1089#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 3. '#1057#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 3. '#1050#1086#1089#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 4. '#1057#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 4. '#1050#1086#1089#1080#1085#1091#1089)
  end
  object chkCh1En: TCheckBox
    Left = 32
    Top = 354
    Width = 73
    Height = 17
    Caption = #1050#1072#1085#1072#1083' 1'
    Checked = True
    State = cbChecked
    TabOrder = 5
  end
  object edtSrc1Freq: TEdit
    Left = 96
    Top = 216
    Width = 73
    Height = 21
    TabOrder = 6
    Text = '64000'
  end
  object edtCh1Amp: TEdit
    Left = 296
    Top = 352
    Width = 73
    Height = 21
    TabOrder = 7
    Text = '10'
  end
  object cbbCh1Out: TComboBox
    Left = 472
    Top = 352
    Width = 73
    Height = 21
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 8
    Text = '1:1'
    Items.Strings = (
      '1:1'
      '1:5/1:10')
  end
  object chkCh2En: TCheckBox
    Left = 32
    Top = 378
    Width = 73
    Height = 17
    Caption = #1050#1072#1085#1072#1083' 2'
    Checked = True
    State = cbChecked
    TabOrder = 9
  end
  object edtSrc1Pha: TEdit
    Left = 176
    Top = 216
    Width = 73
    Height = 21
    TabOrder = 10
    Text = '0'
  end
  object edtCh2Amp: TEdit
    Left = 296
    Top = 376
    Width = 73
    Height = 21
    TabOrder = 11
    Text = '9'
  end
  object cbbCh2Out: TComboBox
    Left = 472
    Top = 376
    Width = 73
    Height = 21
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 12
    Text = '1:1'
    Items.Strings = (
      '1:1'
      '1:5/1:10')
  end
  object edtCh1Offs: TEdit
    Left = 392
    Top = 352
    Width = 73
    Height = 21
    TabOrder = 13
    Text = '0'
  end
  object edtCh2Off: TEdit
    Left = 392
    Top = 376
    Width = 73
    Height = 21
    TabOrder = 14
    Text = '0'
  end
  object chkCh3En: TCheckBox
    Left = 32
    Top = 402
    Width = 73
    Height = 17
    Caption = #1050#1072#1085#1072#1083' 3'
    Checked = True
    State = cbChecked
    TabOrder = 15
  end
  object edtSrc2Freq: TEdit
    Left = 96
    Top = 240
    Width = 73
    Height = 21
    TabOrder = 16
    Text = '48000'
  end
  object edtCh3Amp: TEdit
    Left = 296
    Top = 400
    Width = 73
    Height = 21
    TabOrder = 17
    Text = '8'
  end
  object edtCh3Offs: TEdit
    Left = 392
    Top = 400
    Width = 73
    Height = 21
    TabOrder = 18
    Text = '0'
  end
  object cbbCh3Out: TComboBox
    Left = 472
    Top = 400
    Width = 73
    Height = 21
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 19
    Text = '1:1'
    Items.Strings = (
      '1:1'
      '1:5/1:10')
  end
  object chkCh4En: TCheckBox
    Left = 32
    Top = 426
    Width = 73
    Height = 17
    Caption = #1050#1072#1085#1072#1083' 4'
    Checked = True
    State = cbChecked
    TabOrder = 20
  end
  object edtSrc3Freq: TEdit
    Left = 96
    Top = 264
    Width = 73
    Height = 21
    TabOrder = 21
    Text = '38400'
  end
  object edtCh4Amp: TEdit
    Left = 296
    Top = 424
    Width = 73
    Height = 21
    TabOrder = 22
    Text = '7'
  end
  object edtCh4Offs: TEdit
    Left = 392
    Top = 424
    Width = 73
    Height = 21
    TabOrder = 23
    Text = '0'
  end
  object cbbCh4Out: TComboBox
    Left = 472
    Top = 424
    Width = 73
    Height = 21
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 24
    Text = '1:1'
    Items.Strings = (
      '1:1'
      '1:5/1:10')
  end
  object chkCh5En: TCheckBox
    Left = 32
    Top = 450
    Width = 73
    Height = 17
    Caption = #1050#1072#1085#1072#1083' 5'
    Checked = True
    State = cbChecked
    TabOrder = 25
  end
  object edtCh5Amp: TEdit
    Left = 296
    Top = 448
    Width = 73
    Height = 21
    TabOrder = 26
    Text = '6'
  end
  object edtCh5Offs: TEdit
    Left = 392
    Top = 448
    Width = 73
    Height = 21
    TabOrder = 27
    Text = '0'
  end
  object cbbCh5Out: TComboBox
    Left = 472
    Top = 448
    Width = 73
    Height = 21
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 28
    Text = '1:1'
    Items.Strings = (
      '1:1'
      '1:5/1:10')
  end
  object chkCh6En: TCheckBox
    Left = 32
    Top = 474
    Width = 73
    Height = 17
    Caption = #1050#1072#1085#1072#1083' 6'
    Checked = True
    State = cbChecked
    TabOrder = 29
  end
  object edtCh6Amp: TEdit
    Left = 296
    Top = 472
    Width = 73
    Height = 21
    TabOrder = 30
    Text = '5'
  end
  object edtCh6Offs: TEdit
    Left = 392
    Top = 472
    Width = 73
    Height = 21
    TabOrder = 31
    Text = '0'
  end
  object cbbCh6Out: TComboBox
    Left = 472
    Top = 472
    Width = 73
    Height = 21
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 32
    Text = '1:1'
    Items.Strings = (
      '1:1'
      '1:5/1:10')
  end
  object chkCh7En: TCheckBox
    Left = 32
    Top = 498
    Width = 73
    Height = 17
    Caption = #1050#1072#1085#1072#1083' 7'
    Checked = True
    State = cbChecked
    TabOrder = 33
  end
  object edtCh7Amp: TEdit
    Left = 296
    Top = 496
    Width = 73
    Height = 21
    TabOrder = 34
    Text = '4'
  end
  object edtCh7Offs: TEdit
    Left = 392
    Top = 496
    Width = 73
    Height = 21
    TabOrder = 35
    Text = '0'
  end
  object cbbCh7Out: TComboBox
    Left = 472
    Top = 496
    Width = 73
    Height = 21
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 36
    Text = '1:1'
    Items.Strings = (
      '1:1'
      '1:5/1:10')
  end
  object chkCh8En: TCheckBox
    Left = 32
    Top = 522
    Width = 73
    Height = 17
    Caption = #1050#1072#1085#1072#1083' 8'
    Checked = True
    State = cbChecked
    TabOrder = 37
  end
  object edtCh8Amp: TEdit
    Left = 296
    Top = 520
    Width = 73
    Height = 21
    TabOrder = 38
    Text = '3'
  end
  object edtCh8Offs: TEdit
    Left = 392
    Top = 520
    Width = 73
    Height = 21
    TabOrder = 39
    Text = '0'
  end
  object cbbCh8Out: TComboBox
    Left = 472
    Top = 520
    Width = 73
    Height = 21
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 40
    Text = '1:1'
    Items.Strings = (
      '1:1'
      '1:5/1:10')
  end
  object btnSetSignal: TButton
    Left = 32
    Top = 105
    Width = 185
    Height = 25
    Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1089#1080#1075#1085#1072#1083
    TabOrder = 41
    OnClick = btnSetSignalClick
  end
  object btnStopGen: TButton
    Left = 32
    Top = 136
    Width = 185
    Height = 25
    Caption = #1054#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1075#1077#1085#1077#1088#1072#1094#1080#1102' '#1089#1080#1075#1085#1072#1083#1072
    TabOrder = 42
    OnClick = btnStopGenClick
  end
  object cbbCh2Src: TComboBox
    Left = 120
    Top = 376
    Width = 161
    Height = 21
    ItemHeight = 13
    ItemIndex = 2
    TabOrder = 43
    Text = #1043#1077#1085#1077#1088#1072#1090#1086#1088' 2. '#1057#1080#1085#1091#1089
    Items.Strings = (
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 1. '#1057#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 1. '#1050#1086#1089#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 2. '#1057#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 2. '#1050#1086#1089#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 3. '#1057#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 3. '#1050#1086#1089#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 4. '#1057#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 4. '#1050#1086#1089#1080#1085#1091#1089)
  end
  object cbbCh3Src: TComboBox
    Left = 120
    Top = 400
    Width = 161
    Height = 21
    ItemHeight = 13
    ItemIndex = 4
    TabOrder = 44
    Text = #1043#1077#1085#1077#1088#1072#1090#1086#1088' 3. '#1057#1080#1085#1091#1089
    Items.Strings = (
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 1. '#1057#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 1. '#1050#1086#1089#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 2. '#1057#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 2. '#1050#1086#1089#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 3. '#1057#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 3. '#1050#1086#1089#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 4. '#1057#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 4. '#1050#1086#1089#1080#1085#1091#1089)
  end
  object cbbCh4Src: TComboBox
    Left = 120
    Top = 424
    Width = 161
    Height = 21
    ItemHeight = 13
    ItemIndex = 6
    TabOrder = 45
    Text = #1043#1077#1085#1077#1088#1072#1090#1086#1088' 4. '#1057#1080#1085#1091#1089
    Items.Strings = (
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 1. '#1057#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 1. '#1050#1086#1089#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 2. '#1057#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 2. '#1050#1086#1089#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 3. '#1057#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 3. '#1050#1086#1089#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 4. '#1057#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 4. '#1050#1086#1089#1080#1085#1091#1089)
  end
  object cbbCh5Src: TComboBox
    Left = 120
    Top = 448
    Width = 161
    Height = 21
    ItemHeight = 13
    ItemIndex = 1
    TabOrder = 46
    Text = #1043#1077#1085#1077#1088#1072#1090#1086#1088' 1. '#1050#1086#1089#1080#1085#1091#1089
    Items.Strings = (
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 1. '#1057#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 1. '#1050#1086#1089#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 2. '#1057#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 2. '#1050#1086#1089#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 3. '#1057#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 3. '#1050#1086#1089#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 4. '#1057#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 4. '#1050#1086#1089#1080#1085#1091#1089)
  end
  object cbbCh6Src: TComboBox
    Left = 120
    Top = 472
    Width = 161
    Height = 21
    ItemHeight = 13
    ItemIndex = 3
    TabOrder = 47
    Text = #1043#1077#1085#1077#1088#1072#1090#1086#1088' 2. '#1050#1086#1089#1080#1085#1091#1089
    Items.Strings = (
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 1. '#1057#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 1. '#1050#1086#1089#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 2. '#1057#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 2. '#1050#1086#1089#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 3. '#1057#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 3. '#1050#1086#1089#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 4. '#1057#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 4. '#1050#1086#1089#1080#1085#1091#1089)
  end
  object cbbCh7Src: TComboBox
    Left = 120
    Top = 496
    Width = 161
    Height = 21
    ItemHeight = 13
    ItemIndex = 5
    TabOrder = 48
    Text = #1043#1077#1085#1077#1088#1072#1090#1086#1088' 3. '#1050#1086#1089#1080#1085#1091#1089
    Items.Strings = (
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 1. '#1057#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 1. '#1050#1086#1089#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 2. '#1057#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 2. '#1050#1086#1089#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 3. '#1057#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 3. '#1050#1086#1089#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 4. '#1057#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 4. '#1050#1086#1089#1080#1085#1091#1089)
  end
  object cbbCh8Src: TComboBox
    Left = 120
    Top = 520
    Width = 161
    Height = 21
    ItemHeight = 13
    ItemIndex = 7
    TabOrder = 49
    Text = #1043#1077#1085#1077#1088#1072#1090#1086#1088' 4. '#1050#1086#1089#1080#1085#1091#1089
    Items.Strings = (
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 1. '#1057#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 1. '#1050#1086#1089#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 2. '#1057#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 2. '#1050#1086#1089#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 3. '#1057#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 3. '#1050#1086#1089#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 4. '#1057#1080#1085#1091#1089
      #1043#1077#1085#1077#1088#1072#1090#1086#1088' 4. '#1050#1086#1089#1080#1085#1091#1089)
  end
  object edtSrc4Freq: TEdit
    Left = 96
    Top = 288
    Width = 73
    Height = 21
    TabOrder = 50
    Text = '32000'
  end
  object edtSrc2Pha: TEdit
    Left = 176
    Top = 240
    Width = 73
    Height = 21
    TabOrder = 51
    Text = '0'
  end
  object edtSrc3Pha: TEdit
    Left = 176
    Top = 264
    Width = 73
    Height = 21
    TabOrder = 52
    Text = '0'
  end
  object edtSrc4Pha: TEdit
    Left = 176
    Top = 288
    Width = 73
    Height = 21
    TabOrder = 53
    Text = '0'
  end
end
