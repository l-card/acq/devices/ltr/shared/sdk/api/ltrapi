//---------------------------------------------------------------------------

#ifndef MainUnitH
#define MainUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include "LTR11_ProcessThread.h"
#include "CSPIN.h"

/* ����������, ����������� ��� ������������ ���������� � ������� */
typedef struct  {
    AnsiString csn; //�������� ����� ������
    WORD slot; //����� �����
} TLTR_MODULE_LOCATION;

//---------------------------------------------------------------------------
class TMainForm : public TForm {
__published:	// IDE-managed Components
    TGroupBox *grpDevInfo;
    TLabel *lblDevSerial;
    TLabel *lblVerPld;
    TEdit *edtDevSerial;
    TEdit *edtVerFirm;
    TComboBox *cbbModulesList;
    TButton *btnRefreshDevList;
    TButton *btnOpen;
    TButton *btnStart;
    TButton *btnStop;
    TGroupBox *GroupBox1;
    TComboBox *cbbAdcPrescaler;
    TLabel *Label1;
    TCSpinEdit *seAdcDivider;
    TLabel *Label2;
    TEdit *edtAdcFreq;
    TGroupBox *GroupBox2;
    TLabel *Label3;
    TLabel *Label4;
    TLabel *Label5;
    TLabel *Label6;
    TLabel *Label9;
    TComboBox *cbbLCh1_Channel;
    TComboBox *cbbLCh1_Range;
    TComboBox *cbbLCh1_Mode;
    TEdit *edtLCh1_Result;
    TComboBox *cbbLCh2_Channel;
    TComboBox *cbbLCh2_Range;
    TComboBox *cbbLCh2_Mode;
    TEdit *edtLCh2_Result;
    TComboBox *cbbLCh3_Channel;
    TComboBox *cbbLCh3_Range;
    TComboBox *cbbLCh3_Mode;
    TEdit *edtLCh3_Result;
    TCSpinEdit *seLChCnt;
    TEdit *edtChFreq;
    TLabel *Label7;
    TLabel *Label8;
    TButton *btnFindAdcFreq;



    void __fastcall btnRefreshDevListClick(TObject *Sender);
    void __fastcall FormCreate(TObject *Sender);
    void __fastcall btnOpenClick(TObject *Sender);
    void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
    void __fastcall btnStartClick(TObject *Sender);
    void __fastcall btnStopClick(TObject *Sender);
    void __fastcall btnFindAdcFreqClick(TObject *Sender);
private:	// User declarations
    static const unsigned EXAMPLE_MAX_LCH_CNT = 3;
    TComboBox* ccbChannels[EXAMPLE_MAX_LCH_CNT];
    TComboBox* ccbRanges[EXAMPLE_MAX_LCH_CNT];
    TComboBox* ccbModes[EXAMPLE_MAX_LCH_CNT];
    TEdit*     edtChResults[EXAMPLE_MAX_LCH_CNT];

    DynamicArray<TLTR_MODULE_LOCATION> ltr11_list; //������ ��������� �������

    TLTR11 hltr11; // ��������� ������, � ������� ���� ������
    bool threadRunning; // �������, ������� �� ����� ����� ������
    TLTR11_ProcessThread* thread; //������ ������ ��� ���������� ����� ������

    void updateControls();
    void refreshDeviceList();
    void closeDevice();
    void __fastcall OnThreadTerminate(TObject *obj);

public:		// User declarations
    __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
