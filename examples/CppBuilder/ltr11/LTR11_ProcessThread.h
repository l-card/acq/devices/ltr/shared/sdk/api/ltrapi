//---------------------------------------------------------------------------

#ifndef LTR24_PROCESS_THREAD_H_
#define LTR24_PROCESS_THREAD_H_

#include "ltr/include/ltr11api.h"
#include <vcl.h>


class TLTR11_ProcessThread : public TThread {

protected:
    void __fastcall Execute();

public:
     //�������� ���������� ��� ����������� ����������� ���������
    TEdit* edtChAvg[LTR11_MAX_CHANNEL];

    TLTR11* phltr11; //��������� ������

    bool stop;  //������ �� ������� (��������������� �� ��������� ������)
    INT err;  //��� ������ ��� ���������� ������ �����

    __fastcall TLTR11_ProcessThread(bool CreateSuspended);

private:
    void __fastcall updateData() ;

     // ������� � ����� �� ������� ������
    double ChAvg [LTR11_MAX_CHANNEL];
    // �������, ��� ���� ����������� ������ �� ������� � ChAvg
    bool ChValidData[LTR11_MAX_CHANNEL];
};



//---------------------------------------------------------------------------
#endif