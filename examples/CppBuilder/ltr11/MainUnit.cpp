//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "MainUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TMainForm *MainForm;
//---------------------------------------------------------------------------
__fastcall TMainForm::TMainForm(TComponent* Owner)
    : TForm(Owner) {
    ccbChannels[0]  = cbbLCh1_Channel;
    ccbRanges[0]    = cbbLCh1_Range;
    ccbModes[0]     = cbbLCh1_Mode;
    edtChResults[0] = edtLCh1_Result;

    ccbChannels[1]  = cbbLCh2_Channel;
    ccbRanges[1]    = cbbLCh2_Range;
    ccbModes[1]     = cbbLCh2_Mode;
    edtChResults[1] = edtLCh2_Result;

    ccbChannels[2]  = cbbLCh3_Channel;
    ccbRanges[2]    = cbbLCh3_Range;
    ccbModes[2]     = cbbLCh3_Mode;
    edtChResults[2] = edtLCh3_Result;
}

void TMainForm::updateControls() {
    bool module_opened = (LTR11_IsOpened(&hltr11)==LTR_OK);
    bool devsel = (ltr11_list.Length > 0) && (cbbModulesList->ItemIndex >= 0);

    //���������� ������ ��������� � ����� ����� ������ ������ ���� �� ������� ���������� ����������
    btnRefreshDevList->Enabled = !module_opened;
    cbbModulesList->Enabled = !module_opened;

    //���������� ����� ����� ������ ���� ������� ����������
    btnOpen->Enabled = devsel;
    btnOpen->Caption =  module_opened ? "������� ����������" : "���������� ����������";

    btnStart->Enabled = module_opened && !threadRunning;
    btnStop->Enabled = module_opened && threadRunning;

    //��������� �������� �������� ������ ��� �������� ���������� � �� ���������� �����
    bool cfg_en = module_opened && !threadRunning;

    cbbAdcPrescaler->Enabled = cfg_en;
    seAdcDivider->Enabled = cfg_en;
    btnFindAdcFreq->Enabled = cfg_en;
    edtAdcFreq->Enabled = cfg_en;

    seLChCnt->Enabled = cfg_en;
    for (unsigned ch = 0; ch < EXAMPLE_MAX_LCH_CNT; ch++) {
        ccbChannels[ch]->Enabled = cfg_en;
        ccbRanges[ch]->Enabled = cfg_en;
        ccbModes[ch]->Enabled = cfg_en;
    }
}



void TMainForm::refreshDeviceList() {
    INT err = LTR_OK;
    int modules_cnt = 0;
    ltr11_list.Length = 0;
    cbbModulesList->Items->Clear();


    // ������������� ����� � ����������� ������� �������, ����� �������� ������ �������
    TLTR srv;
    LTR_Init(&srv);
    err=LTR_OpenSvcControl(&srv, LTRD_ADDR_DEFAULT, LTRD_PORT_DEFAULT);
    if (err!=LTR_OK) {
        MessageDlg("�� ������� ���������� ����� � ��������: " + String(LTR_GetErrorString(err)),
                    mtError, TMsgDlgButtons() << mbOK,NULL);
    } else {
        char serial_list[LTR_CRATES_MAX][LTR_CRATE_SERIAL_SIZE];
        //�������� ������ �������� ������� ���� ������������ �������
        err=LTR_GetCrates(&srv, (BYTE*)&serial_list[0][0]);
        //��������� ���������� ������ �� ����� - ����� �������
        LTR_Close(&srv);
        if (err!=LTR_OK) {
             MessageDlg("�� ������� �������� ������ �������: " + String(LTR_GetErrorString(err)),
                    mtError, TMsgDlgButtons() << mbOK,NULL);
        } else {
            for (int crate_ind=0 ; crate_ind < LTR_CRATES_MAX; crate_ind++) {
                //������� ������ ������������� �������� �������� �����
                if (serial_list[crate_ind][0]!='\0') {
                    // ������������� ����� � ������ �������, ����� �������� ������ �������
                    TLTR crate;
                    LTR_Init(&crate);
                    err =  LTR_OpenCrate(&crate, LTRD_ADDR_DEFAULT, LTRD_PORT_DEFAULT,
                                         LTR_CRATE_IFACE_UNKNOWN, serial_list[crate_ind]);
                    if (err==LTR_OK) {
                        WORD mids[LTR_MODULES_PER_CRATE_MAX];
                        err=LTR_GetCrateModules(&crate, mids);
                        if (err==LTR_OK) {
                            for (int module_ind=0; module_ind < LTR_MODULES_PER_CRATE_MAX; module_ind++) {
                                //���� ������ LTR210
                                if (mids[module_ind]==LTR_MID_LTR11) {
                                    // ��������� ���������� � ��������� ������, ����������� ���
                                    // ������������ ������������ ���������� � ���, � ������
                                    modules_cnt++;
                                    ltr11_list.Length=modules_cnt;
                                    ltr11_list[modules_cnt-1].csn = String(serial_list[crate_ind]);
                                    ltr11_list[modules_cnt-1].slot = module_ind+LTR_CC_CHNUM_MODULE1;
                                    // � ��������� � ComboBox ��� ����������� ������ �������
                                    cbbModulesList->AddItem("����� " + ltr11_list[modules_cnt-1].csn +
                                            ", ���� " + IntToStr(ltr11_list[modules_cnt-1].slot), NULL);
                                }
                            }
                        }        
                        //��������� ���������� � �������
                        LTR_Close(&crate);
                    }
                }
            }
        }
    }

    cbbModulesList->ItemIndex = 0;
    updateControls();
}


void TMainForm::closeDevice() {
    // ��������� ����� � �������� ���������� ������
    if (threadRunning) {
        thread->stop=true;
        thread->WaitFor();
    }

    LTR11_Close(&hltr11);
}


//�������, ���������� �� ���������� ������ ����� ������
//��������� ����� ������, ������������� threadRunning
void __fastcall TMainForm::OnThreadTerminate(TObject *obj) {
    if (thread->err != LTR_OK) {
        MessageDlg("���� ������ �������� � �������: " + String(LTR11_GetErrorString(thread->err)),
                    mtError, TMsgDlgButtons() << mbOK,NULL);
    }

    threadRunning = false;
    updateControls();
}



//---------------------------------------------------------------------------
void __fastcall TMainForm::btnRefreshDevListClick(TObject *Sender) {
    refreshDeviceList();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::FormCreate(TObject *Sender) {
    thread = 0;
    LTR11_Init(&hltr11);

    refreshDeviceList();
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::btnOpenClick(TObject *Sender) {
// ���� ���������� � ������� ������� - �� ��������� �����
    if (LTR11_IsOpened(&hltr11)!=LTR_OK) {
        INT res = LTR_OK;
        // ���������� � ������ � ����� ����� �� ������������ ������ �� �������
        // ������� ��������� ������
        TLTR_MODULE_LOCATION location = ltr11_list[ cbbModulesList->ItemIndex ];
        LTR11_Init(&hltr11);
        res=LTR11_Open(&hltr11, LTRD_ADDR_DEFAULT, LTRD_PORT_DEFAULT, location.csn.c_str(), location.slot);
        if (res!=LTR_OK) {
            MessageDlg("�� ������� ���������� ����� � �������: " +
                        String(LTR11_GetErrorString(res)), mtError, TMsgDlgButtons() << mbOK, 0);
        }

        if (res==LTR_OK) {
            // ������ ���������� �� Flash-������ (������� ������������� ������������)
            res=LTR11_GetConfig(&hltr11);
            if (res != LTR_OK) {
                MessageDlg("�� ������� ��������� ������������ �� ������: "
                           + String(LTR11_GetErrorString(res)), mtError, TMsgDlgButtons() << mbOK, 0);
            }
        }

        if (res==LTR_OK) {
            edtDevSerial->Text = String(hltr11.ModuleInfo.Serial);
            edtVerFirm->Text = IntToStr((hltr11.ModuleInfo.Ver >> 8) & 0xFF) + "." +
                                IntToStr(hltr11.ModuleInfo.Ver & 0xFF);
        } else {
            LTR11_Close(&hltr11);
        }
    } else {
        closeDevice();
    }
    updateControls();
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::FormClose(TObject *Sender, TCloseAction &Action) {
    closeDevice();
    delete thread;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::btnStartClick(TObject *Sender) {
    int res = LTR_OK;

    /* ��������� �������� �� ��������� ���������� � ���������������
       ���� ��������� ������. ��� �������� ����� �� �������� ���. ��������, ���
        ������� ������ ��������... */
    hltr11.ADCRate.prescaler = StrToInt(cbbAdcPrescaler->Text);
    hltr11.ADCRate.divider   = seAdcDivider->Value;
    hltr11.LChQnt = seLChCnt->Value;

    for (int ch=0; (ch < hltr11.LChQnt) && (res == LTR_OK); ch++) {
        int phy_ch = ccbChannels[ch]->ItemIndex;
        int range  = ccbRanges[ch]->ItemIndex;
        int mode   = ccbModes[ch]->ItemIndex;

        if ((phy_ch < 0) || (phy_ch >= 32) || (range < 0) || (mode < 0)) {
            res = LTR_ERROR_PARAMETERS;
        } else {
            hltr11.LChTbl[ch] = LTR11_CreateLChannel(phy_ch, mode, range);
        }
    }


    if (res == LTR_OK)
        res = LTR11_SetADC(&hltr11);

    if (res == LTR_OK) {
        if (thread) {
            delete thread;
            thread = 0;
        }

        edtChFreq->Text = FloatToStr(hltr11.ChRate*1000);
        edtAdcFreq->Text = FloatToStr(hltr11.ChRate*hltr11.LChQnt*1000);

        thread = new TLTR11_ProcessThread(true);
        /* ��� ��� ��������� ������ ���� ���� � �� ��, ��� ������������ �������,
            ��� � ������ ��������� ����, �� ��������� �������� �� ��� pointer */
        thread->phltr11 = &hltr11;
        /* ��������� �������� ����������, ������� ������ ���������� ��������������
           ������� � ����� ������ */
        for (unsigned ch=0; ch < EXAMPLE_MAX_LCH_CNT; ch++) {
            thread->edtChAvg[ch]=edtChResults[ch];
            edtChResults[ch]->Text = "";
        }
        /*  ������������� ������� �� ������� ���������� ������ (� ���������,
            ����� ���������, ���� ����� ���������� ��� ��-�� ������ ��� �����
            ������) */
        thread->OnTerminate = OnThreadTerminate;
        thread->Resume();

        threadRunning = true;
    }

    if (res != LTR_OK) {
        MessageDlg("�� ������� ���������� ���������: " +
                   String(LTR11_GetErrorString(res)), mtError, TMsgDlgButtons() << mbOK, 0);
    }

    updateControls();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::btnStopClick(TObject *Sender) {
    // ������������� ������ �� ���������� ������
    if (threadRunning) {
        thread->stop=true;
    }
    btnStop->Enabled = false;
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::btnFindAdcFreqClick(TObject *Sender)  {
    INT prescaler, divider;
    double freq;
    /* ������ �� ������� ��� �������� ��������� */

    freq = StrToFloat(edtAdcFreq->Text);
    LTR11_FindAdcFreqParams(freq, &hltr11.ADCRate.prescaler, &hltr11.ADCRate.divider, &freq);
    seAdcDivider->Value = hltr11.ADCRate.divider;
    cbbAdcPrescaler->Text = IntToStr(hltr11.ADCRate.prescaler);
    edtAdcFreq->Text = FloatToStr(freq);     
}
//---------------------------------------------------------------------------


