//---------------------------------------------------------------------------


#pragma hdrstop

#include "LTR25_ProcessThread.h"

// �����, �� ������� ����� ������������ ����������� �������� (� ��)
#define RECV_BLOCK_TIME          1000
// ��������������  ���������� ������� �� ����� ������ (� ��)
#define RECV_TOUT                4000


//---------------------------------------------------------------------------

#pragma package(smart_init)

 __fastcall TLTR25_ProcessThread::TLTR25_ProcessThread(bool CreateSuspended)
    : TThread(CreateSuspended), stop(false), err (LTR_OK) {

}

/* ���������� ����������� ����� ������������ ���������� ���������.
   ����� ������ ����������� ������ ����� Syncronize, ������� �����
   ��� ������� � ��������� VCL �� �� ��������� ������ */
void __fastcall TLTR25_ProcessThread::updateData() {
    for (int ch=0; ch < LTR25_CHANNEL_CNT; ch++) {
        if (ChValidData[ch]) {
            if (ChStatus[ch] == LTR25_CH_STATUS_OK) {
                edtChVal[ch]->Text = FloatToStrF(ChPP[ch], ffFixed, 8, 6);
            } else if (ChStatus[ch] == LTR25_CH_STATUS_SHORT) {
                edtChVal[ch]->Text = "��";
            } else if (ChStatus[ch] == LTR25_CH_STATUS_OPEN) {
                edtChVal[ch]->Text = "�����";
            }
        } else {
            edtChVal[ch]->Text = "";
        }
    }
}


void __fastcall TLTR25_ProcessThread::Execute() {
    DWORD *rcv_buf=0;
    double *data=0;
    INT err = LTR_OK;
    // ������ ����������� �������
    BYTE ch_nums[LTR25_CHANNEL_CNT];
    INT ch_cnt=0;  //���������� ����������� �������
    INT recv_wrd_cnt;  //���������� ����������� ����� ���� �� ���
    INT recv_data_cnt; //���������� ������������ ����, ������� ������ ������� �� ���

    //�������� ����������
    for (INT ch=0; ch < LTR25_CHANNEL_CNT; ch++)
        ChValidData[ch]=false;
    Synchronize(updateData);

    //���������, ������� � ����� ������ ���������
    for (INT ch=0; ch < LTR25_CHANNEL_CNT; ch++) {
        if (phltr25->Cfg.Ch[ch].Enabled) {
            ch_nums[ch_cnt] = ch;
            ch_cnt++;
        }
    }

    /* ����������, ������ �������������� ����� ���������� �� �������� �����
      => ����� ��������� ������ ������� ������ ������� */
    recv_data_cnt =  (INT)(phltr25->State.AdcFreq*RECV_BLOCK_TIME/1000) * ch_cnt;
    /* � 24-������ ������� ������� ������� ������������� ��� ����� �� ������,
                   � � 20-������ - ���� */
    recv_wrd_cnt = phltr25->Cfg.DataFmt == LTR25_FORMAT_32 ? 2*recv_data_cnt : recv_data_cnt;

    //�������� ������� ��� ������ ������
    rcv_buf = new DWORD[recv_wrd_cnt];
    data = new double[recv_data_cnt];

    err = LTR25_Start(phltr25);
    if (err==LTR_OK) {
        while (!stop && (err==LTR_OK)) {
            /* ��������� ������ */
            INT recv_size = LTR25_Recv(phltr25, rcv_buf, 0, recv_wrd_cnt, RECV_TOUT + RECV_BLOCK_TIME);
            //�������� ������ ���� ������������� ���� ������
            if (recv_size < 0) {
                err=recv_size;
            } else if (recv_size < recv_wrd_cnt) {
                err=LTR_ERROR_RECV_INSUFFICIENT_DATA;
            } else {
                double ch_max[LTR25_CHANNEL_CNT];
                double ch_min[LTR25_CHANNEL_CNT];
                DWORD  ch_cur_status[LTR25_CHANNEL_CNT];
                bool   ch_valid[LTR25_CHANNEL_CNT];

                err=LTR25_ProcessData(phltr25, rcv_buf, data, &recv_size,
										LTR25_PROC_FLAG_VOLT |
										LTR25_PROC_FLAG_PHASE_COR |
										LTR25_PROC_FLAG_SIGN_COR,
										ch_cur_status);
                if (err==LTR_OK) {
                    for (INT ch=0; ch < LTR25_CHANNEL_CNT; ch++) {
                        ch_valid[ch] = false;
                    }

                    for (INT ch=0; ch < ch_cnt; ch++) {
                        ChStatus[ch_nums[ch]] = ch_cur_status[ch];
                        ch_min[ch_nums[ch]]   = data[ch];
                        ch_max[ch_nums[ch]]   = data[ch];
                        ch_valid[ch_nums[ch]] = true;
                    }

                    // �������� ���-�� �������� �� �����
                    recv_size = (INT) (recv_size/ch_cnt);

                    // ������� ������������ � ������������� �������� ��� ���� �������,
                    // ������� ��������� � ��� ������� ������ = OK
                    for (INT i=0; i < recv_size; i++) {
                        for (INT ch=0; ch < ch_cnt; ch++) {
                            if (ch_cur_status[ch] == LTR25_CH_STATUS_OK) {
                                if (ch_min[ch_nums[ch]] > data[ch_cnt*i + ch])
                                    ch_min[ch_nums[ch]] = data[ch_cnt*i + ch];
                                if (ch_max[ch_nums[ch]] < data[ch_cnt*i + ch])
                                    ch_max[ch_nums[ch]] = data[ch_cnt*i + ch];
                            }
                        }
                    }

                    // ��������� ���-��� ������ ��� ������� ������������� � ������������
                    for (INT ch=0; ch < LTR25_CHANNEL_CNT; ch++) {
                        if (ch_valid[ch]) {
                            if (ChStatus[ch] == LTR25_CH_STATUS_OK)
                                ChPP[ch] = ch_max[ch] - ch_min[ch];
                        }
                        ChValidData[ch] = ch_valid[ch];
                    }
                    // ��������� �������� ��������� ����������
                    Synchronize(updateData);
                }
            }
        } // while (!stop && (err==LTR_OK))

        /* �� ������ �� ����� ������������� ���� ������.
            ����� �� �������� ��� ������ (���� ����� �� ������)
            ��������� �������� ��������� � ��������� ���������� */
        INT stoperr= LTR25_Stop(phltr25);
        if (err == LTR_OK)
            err= stoperr;
    }

    delete []rcv_buf;
    delete []data;
}
