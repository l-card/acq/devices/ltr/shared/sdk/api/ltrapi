//---------------------------------------------------------------------------

#ifndef MainUnitH
#define MainUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include "ltr/include/ltr25api.h"
#include "LTR25_ProcessThread.h"

/* ����������, ����������� ��� ������������ ���������� � ������� */
typedef struct  {
    AnsiString csn; //�������� ����� ������
    WORD slot; //����� �����
} TLTR_MODULE_LOCATION;


//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
    TButton *btnRefreshDevList;
    TButton *btnOpen;
    TButton *btnStart;
    TButton *btnStop;
    TComboBox *cbbModulesList;
    TGroupBox *grpDevInfo;
    TLabel *lblDevSerial;
    TLabel *lblVerPld;
    TEdit *edtDevSerial;
    TEdit *edtVerPld;
    TEdit *edtVerFPGA;
    TLabel *lblVerFPGA;
    TLabel *lbl1;
    TLabel *lbl2;
    TGroupBox *grpConfig;
    TLabel *lblAdcFreq;
    TLabel *lblDataFmt;
    TLabel *lblISrcVal;
    TComboBox *cbbAdcFreq;
    TComboBox *cbbDataFmt;
    TComboBox *cbbISrcValue;
    TCheckBox *chkCh1En;
    TEdit *edtCh1Val;
    TCheckBox *chkCh2En;
    TEdit *edtCh2Val;
    TCheckBox *chkCh3En;
    TEdit *edtCh3Val;
    TCheckBox *chkCh4En;
    TEdit *edtCh4Val;
    TCheckBox *chkCh5En;
    TEdit *edtCh5Val;
    TCheckBox *chkCh6En;
    TEdit *edtCh6Val;
    TCheckBox *chkCh7En;
    TEdit *edtCh7Val;
    TCheckBox *chkCh8En;
    TEdit *edtCh8Val;
    void __fastcall btnRefreshDevListClick(TObject *Sender);
    void __fastcall btnOpenClick(TObject *Sender);
    void __fastcall FormCreate(TObject *Sender);
    void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
    void __fastcall btnStartClick(TObject *Sender);
    void __fastcall btnStopClick(TObject *Sender);
private:	// User declarations
    DynamicArray<TLTR_MODULE_LOCATION> ltr25_list; //������ ��������� �������

    TLTR25 hltr25; // ��������� ������, � ������� ���� ������
    bool threadRunning; // �������, ������� �� ����� ����� ������
    TLTR25_ProcessThread* thread; //������ ������ ��� ���������� ����� ������

    void updateControls();
    void refreshDeviceList();
    void closeDevice();
    void __fastcall OnThreadTerminate(TObject *obj);    
public:		// User declarations
    __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
