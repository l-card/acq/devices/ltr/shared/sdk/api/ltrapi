object MainForm: TMainForm
  Left = 192
  Top = 124
  Width = 609
  Height = 446
  Caption = 'ltr25 example'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 136
    Top = 248
    Width = 89
    Height = 13
    Caption = #1079#1085#1072#1095#1077#1085#1080#1077' '#1087#1080#1082'-'#1087#1080#1082
  end
  object lbl2: TLabel
    Left = 392
    Top = 248
    Width = 89
    Height = 13
    Caption = #1079#1085#1072#1095#1077#1085#1080#1077' '#1087#1080#1082'-'#1087#1080#1082
  end
  object btnRefreshDevList: TButton
    Left = 32
    Top = 16
    Width = 185
    Height = 25
    Caption = #1054#1073#1085#1086#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082
    TabOrder = 0
    OnClick = btnRefreshDevListClick
  end
  object btnOpen: TButton
    Left = 32
    Top = 73
    Width = 185
    Height = 25
    Caption = #1054#1090#1082#1088#1099#1090#1100' '#1091#1089#1090#1088#1086#1081#1089#1090#1074#1086
    TabOrder = 1
    OnClick = btnOpenClick
  end
  object btnStart: TButton
    Left = 32
    Top = 104
    Width = 185
    Height = 25
    Caption = #1047#1072#1087#1091#1089#1082' '#1089#1073#1086#1088#1072' '#1076#1072#1085#1085#1099#1093
    TabOrder = 2
    OnClick = btnStartClick
  end
  object btnStop: TButton
    Left = 32
    Top = 136
    Width = 185
    Height = 25
    Caption = #1054#1089#1090#1072#1085#1086#1074' '#1089#1073#1086#1088#1072' '#1076#1072#1085#1085#1099#1093
    Enabled = False
    TabOrder = 3
    OnClick = btnStopClick
  end
  object cbbModulesList: TComboBox
    Left = 272
    Top = 16
    Width = 273
    Height = 21
    ItemHeight = 13
    TabOrder = 4
  end
  object grpDevInfo: TGroupBox
    Left = 272
    Top = 56
    Width = 273
    Height = 105
    Caption = #1048#1085#1092#1086#1088#1084#1072#1094#1080#1103' '#1086' '#1084#1086#1076#1091#1083#1077
    TabOrder = 5
    object lblDevSerial: TLabel
      Left = 16
      Top = 24
      Width = 86
      Height = 13
      Caption = #1057#1077#1088#1080#1081#1085#1099#1081' '#1085#1086#1084#1077#1088
    end
    object lblVerPld: TLabel
      Left = 16
      Top = 48
      Width = 61
      Height = 13
      Caption = #1042#1077#1088#1089#1080#1103' PLD'
    end
    object lblVerFPGA: TLabel
      Left = 16
      Top = 72
      Width = 71
      Height = 13
      Caption = #1042#1077#1088#1089#1080#1103' '#1055#1051#1048#1057
    end
    object edtDevSerial: TEdit
      Left = 112
      Top = 24
      Width = 145
      Height = 21
      ReadOnly = True
      TabOrder = 0
    end
    object edtVerPld: TEdit
      Left = 112
      Top = 48
      Width = 145
      Height = 21
      ReadOnly = True
      TabOrder = 1
    end
    object edtVerFPGA: TEdit
      Left = 112
      Top = 72
      Width = 145
      Height = 21
      ReadOnly = True
      TabOrder = 2
    end
  end
  object grpConfig: TGroupBox
    Left = 8
    Top = 176
    Width = 569
    Height = 57
    Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080
    TabOrder = 6
    object lblAdcFreq: TLabel
      Left = 8
      Top = 24
      Width = 89
      Height = 13
      Caption = #1063#1072#1089#1090#1086#1090#1072' '#1040#1062#1055' ('#1043#1094')'
    end
    object lblDataFmt: TLabel
      Left = 200
      Top = 24
      Width = 106
      Height = 13
      Caption = #1056#1072#1079#1088#1103#1076#1085#1086#1089#1090#1100' '#1076#1072#1085#1085#1099#1093
    end
    object lblISrcVal: TLabel
      Left = 400
      Top = 24
      Width = 74
      Height = 13
      Caption = #1048#1089#1090#1086#1095#1085#1080#1082' '#1090#1086#1082#1072
    end
    object cbbAdcFreq: TComboBox
      Left = 101
      Top = 20
      Width = 76
      Height = 21
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 0
      Text = '78125'
      Items.Strings = (
        '78125'
        '39063'
        '19531'
        '9766'
        '4883'
        '2441'
        '1221'
        '610')
    end
    object cbbDataFmt: TComboBox
      Left = 309
      Top = 20
      Width = 76
      Height = 21
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 1
      Text = '20 '#1073#1080#1090
      Items.Strings = (
        '20 '#1073#1080#1090
        '24 '#1073#1080#1090#1072)
    end
    object cbbISrcValue: TComboBox
      Left = 477
      Top = 20
      Width = 76
      Height = 21
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 2
      Text = '2.86 '#1084#1040
      Items.Strings = (
        '2.86 '#1084#1040
        '10 '#1084#1040)
    end
  end
  object chkCh1En: TCheckBox
    Left = 48
    Top = 272
    Width = 73
    Height = 17
    Caption = #1050#1072#1085#1072#1083' 1'
    Checked = True
    State = cbChecked
    TabOrder = 7
  end
  object edtCh1Val: TEdit
    Left = 128
    Top = 268
    Width = 105
    Height = 21
    ReadOnly = True
    TabOrder = 8
  end
  object chkCh2En: TCheckBox
    Left = 48
    Top = 296
    Width = 73
    Height = 17
    Caption = #1050#1072#1085#1072#1083' 2'
    Checked = True
    State = cbChecked
    TabOrder = 9
  end
  object edtCh2Val: TEdit
    Left = 128
    Top = 292
    Width = 105
    Height = 21
    ReadOnly = True
    TabOrder = 10
  end
  object chkCh3En: TCheckBox
    Left = 48
    Top = 320
    Width = 73
    Height = 17
    Caption = #1050#1072#1085#1072#1083' 3'
    Checked = True
    State = cbChecked
    TabOrder = 11
  end
  object edtCh3Val: TEdit
    Left = 128
    Top = 316
    Width = 105
    Height = 21
    ReadOnly = True
    TabOrder = 12
  end
  object chkCh4En: TCheckBox
    Left = 48
    Top = 344
    Width = 73
    Height = 17
    Caption = #1050#1072#1085#1072#1083' 4'
    Checked = True
    State = cbChecked
    TabOrder = 13
  end
  object edtCh4Val: TEdit
    Left = 128
    Top = 340
    Width = 105
    Height = 21
    ReadOnly = True
    TabOrder = 14
  end
  object chkCh5En: TCheckBox
    Left = 304
    Top = 272
    Width = 73
    Height = 17
    Caption = #1050#1072#1085#1072#1083' 5'
    Checked = True
    State = cbChecked
    TabOrder = 15
  end
  object edtCh5Val: TEdit
    Left = 384
    Top = 268
    Width = 105
    Height = 21
    ReadOnly = True
    TabOrder = 16
  end
  object chkCh6En: TCheckBox
    Left = 304
    Top = 296
    Width = 73
    Height = 17
    Caption = #1050#1072#1085#1072#1083' 6'
    Checked = True
    State = cbChecked
    TabOrder = 17
  end
  object edtCh6Val: TEdit
    Left = 384
    Top = 292
    Width = 105
    Height = 21
    ReadOnly = True
    TabOrder = 18
  end
  object chkCh7En: TCheckBox
    Left = 304
    Top = 320
    Width = 73
    Height = 17
    Caption = #1050#1072#1085#1072#1083' 7'
    TabOrder = 19
  end
  object edtCh7Val: TEdit
    Left = 384
    Top = 316
    Width = 105
    Height = 21
    ReadOnly = True
    TabOrder = 20
  end
  object chkCh8En: TCheckBox
    Left = 304
    Top = 344
    Width = 73
    Height = 17
    Caption = #1050#1072#1085#1072#1083' 8'
    TabOrder = 21
  end
  object edtCh8Val: TEdit
    Left = 384
    Top = 340
    Width = 105
    Height = 21
    ReadOnly = True
    TabOrder = 22
  end
end
