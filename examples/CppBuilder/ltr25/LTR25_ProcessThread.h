//---------------------------------------------------------------------------

#ifndef LTR24_PROCESS_THREAD_H_
#define LTR24_PROCESS_THREAD_H_

#include "ltr/include/ltr25api.h"
#include <vcl.h>


class TLTR25_ProcessThread : public TThread {

protected:
    void __fastcall Execute();

public:
     //�������� ���������� ��� ����������� ����������� ���������
    TEdit* edtChVal[LTR25_CHANNEL_CNT];

    TLTR25* phltr25; //��������� ������

    bool stop;  //������ �� ������� (��������������� �� ��������� ������)
    INT err;  //��� ������ ��� ���������� ������ �����

    __fastcall TLTR25_ProcessThread(bool CreateSuspended);

private:
    void __fastcall updateData() ;

    // �������� ���-��� �� ���������� ��������� ����� �� ������� ������
    double ChPP [LTR25_CHANNEL_CNT];
    // ��������� �������
    DWORD ChStatus[LTR25_CHANNEL_CNT];
    // �������, ��� ���� ����������� ������ �� ������� � ChAvg
    bool ChValidData[LTR25_CHANNEL_CNT];
};



//---------------------------------------------------------------------------
#endif