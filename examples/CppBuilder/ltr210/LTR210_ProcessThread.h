//---------------------------------------------------------------------------

#ifndef LTR210_PROCESS_THREAD_H_
#define LTR210_PROCESS_THREAD_H_

#include "ltr/include/ltr210api.h"
#include <vcl.h>


class LTR210_ProcessThread : public TThread {

protected:
    void __fastcall Execute();

public:
     //�������� ���������� ��� ����������� ����������� ���������
    TEdit* edtChAvg[LTR210_CHANNEL_CNT];
    TEdit* edtValidFrameCntr;
    TEdit* edtInvalidFrameCntr;
    TEdit* edtSyncSkipCntr;
    TEdit* edtOverlapCntr;
    TEdit* edtInvalidHistCntr;

    TLTR210* phltr210; //��������� ������

    bool stop;  //������ �� ������� (��������������� �� ��������� ������)
    INT err;  //��� ������ ��� ���������� ������ �����

    __fastcall LTR210_ProcessThread(bool CreateSuspended);

private:
    void __fastcall updateData() ;
    void __fastcall checkFrameStatusFlags(WORD status_flags);

     // ������� � ����� �� ������� ������
    double ChAvg [LTR210_CHANNEL_CNT];
    // �������, ��� ���� ����������� ������ �� ������� � ChAvg
    bool ChValidData[LTR210_CHANNEL_CNT];
    // ������� ��������� �������� ������
    int ValidFrameCntr;
    // ������� ������, �������� � �������
    int InvalidFrameCntr;
    // ������� ������ � ��������� ������� �������������
    int SyncSkipCntr;
    // ������� ������ ��� ��������� ������ ������� ��������� ������
    int OverlapCntr;
    // ������� ������ � �������� ������������
    int InvalidHistCntr;  
};



//---------------------------------------------------------------------------
#endif