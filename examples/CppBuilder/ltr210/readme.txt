������ ������ ������������� ������ � ������� LTR210 �� ����� "Borland C++ Builder".
������ �������� ���� ������� ��� ����� "Borland C++ Builder 6" (ltr210_cpp_builder.dpr) � ���
����� "Embarcadero RAD Studio" (ltr210_cpp_builder.cbproj)

��� ���� ����� ������� ������, ���������� �������� ���� � ������������ ������ �� ���� ��������� ��������� LTR
(������������ ����� ������ ������ � ������������� ltr/include ������������ ����������� ����).
���� ����� ������ ��������� �������: 
    Borland C++ Builder - "Project->Options->Directories/Conditionals->Include path", 
    RAD Studio          - "Project->Options->Directories and Conditionals->Include path"
����� ���� ���������� ������� �� ������� � �������� ������� � ���� ����� ����� ltr210api.lib � ����� ltrapi.lib
�� ���������� <LTR_INSTALL_DIR>/lib/borland

������ ������������� ���������� ����� � ���������� ������.
����� ������ ����������� � ��������� ������, ������������� � ������ TLTR210_ProcessThread.
�� ������ ���������� �������� �������� ������� � ��� ������� ��������� �������
�������� �� ������� ������.
