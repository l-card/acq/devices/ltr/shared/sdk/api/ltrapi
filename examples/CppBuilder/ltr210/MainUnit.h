//---------------------------------------------------------------------------

#ifndef MainUnitH
#define MainUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CSPIN.h"
#include <ComCtrls.hpp>



#include "ltr/include/ltr210api.h"
#include "LTR210_ProcessThread.h"


/* ����������, ����������� ��� ������������ ���������� � ������� */
typedef struct {
    AnsiString csn; //�������� ����� ������
    WORD slot; //����� �����
} TLTR_MODULE_LOCATION;

//---------------------------------------------------------------------------
class TMainForm : public TForm {
__published:	// IDE-managed Components
        TComboBox *cbbModulesList;
        TButton *btnRefreshDevList;
        TLabel *lblFpgaLoadProgr;
        TProgressBar *pbFpgaLoad;
        TButton *btnOpen;
        TButton *btnStart;
        TButton *btnStop;
        TButton *btnFrameStart;
        TGroupBox *grpDevInfo;
        TLabel *lblDevSerial;
        TLabel *lblVerPld;
        TLabel *lblVerFPGA;
        TEdit *edtDevSerial;
        TEdit *edtVerFPGA;
        TEdit *edtVerPld;
        TGroupBox *GroupBox1;
        TGroupBox *grp1;
        TCheckBox *chkChEn2;
        TComboBox *cbbRange2;
        TComboBox *cbbMode2;
        TEdit *edtSyncLevelL2;
        TEdit *edtSyncLevelH2;
        TComboBox *cbbDigBit2;
        TGroupBox *grpCfgCh1;
        TCheckBox *chkChEn1;
        TComboBox *cbbRange1;
        TComboBox *cbbMode1;
        TEdit *edtSyncLevelL1;
        TEdit *edtSyncLevelH1;
        TComboBox *cbbDigBit1;
        TLabel *lblRange1;
        TLabel *lblChMode1;
        TLabel *lblSyncLevelL1;
        TLabel *lblSyncLevelH1;
        TLabel *lblDigBit1;
        TLabel *lblFrameSize;
        TLabel *lblHistSize;
        TLabel *lblSyncMode;
        TComboBox *cbbSyncMode;
        TLabel *lblGroupMode;
        TComboBox *cbbGroupMode;
        TCheckBox *chkKeepaliveEn;
        TCheckBox *chkWriteAutoSusp;
        TGroupBox *grpResult;
        TLabel *lblCh1Avg;
        TLabel *lblCh2Avg;
        TEdit *edtCh1Avg;
        TEdit *edtCh2Avg;
        TGroupBox *grpFrameCntrs;
        TLabel *lblValidFrameCntr;
        TLabel *lblInvalidFrameCntr;
        TLabel *lblSyncSkip;
        TLabel *lblOverlapCntr;
        TLabel *lblInvalidHistCntr;
        TEdit *edtValidFrameCntr;
        TEdit *edtInvalidFrameCntr;
        TEdit *edtSyncSkipCntr;
        TEdit *edtOverlapCntr;
        TEdit *edtInvalidHistCntr;
        TCSpinEdit *seFrameSize;
        TCSpinEdit *seHistSize;
    TLabel *lblAdcFreqDiv;
    TEdit *edtAdcFreq;
    TEdit *edtFrameFreq;
    TLabel *lblAdcDcm;
    void __fastcall FormCreate(TObject *Sender);
    void __fastcall btnRefreshDevListClick(TObject *Sender);
    void __fastcall btnOpenClick(TObject *Sender);
    void __fastcall btnStartClick(TObject *Sender);
    void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
    void __fastcall btnStopClick(TObject *Sender);
    void __fastcall btnFrameStartClick(TObject *Sender);
private:    // User declarations
    DynamicArray<TLTR_MODULE_LOCATION> ltr210_list; //������ ��������� �������

    TLTR210 hltr210; // ��������� ������, � ������� ���� ������
    bool load_progr; // �������, ��� ������ ���� �������� ��������
    bool threadRunning; // �������, ������� �� ����� ����� ������
    LTR210_ProcessThread* thread; //������ ������ ��� ���������� ����� ������

    void updateControls();
    void refreshDeviceList();
    void closeDevice();
    void __fastcall OnThreadTerminate(TObject *obj);

public:     // User declarations
        __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
