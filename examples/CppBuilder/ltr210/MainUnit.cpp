//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "MainUnit.h"


//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"

TMainForm *MainForm;
//---------------------------------------------------------------------------

/* ����� ���������/��������� ������ �������� ���������� � ����������� ��
  �������� ��������� ��������� */
void TMainForm::updateControls()
{
    bool module_opened=LTR210_IsOpened(&hltr210)==LTR_OK;
    bool devsel = (ltr210_list.Length > 0) && (cbbModulesList->ItemIndex >= 0);


    //���������� ������ ��������� � ����� ����� ������ ������ ���� �� ������� ���������� ����������
    btnRefreshDevList->Enabled = !module_opened;
    cbbModulesList->Enabled = !module_opened;

    //���������� ����� ����� ������ ���� ������� ����������
    btnOpen->Enabled = devsel && !load_progr;
    if (module_opened)
        btnOpen->Caption = "������� ����������";
    else
        btnOpen->Caption = "���������� ����������";

    btnStart->Enabled = module_opened && !threadRunning && !load_progr;
    btnStop->Enabled = module_opened && threadRunning;
    //���������� ������ ����� ����� ����� ������ ��� SyncMode = LTR210_SYNC_MODE_INTERNAL
    btnFrameStart->Enabled = threadRunning && (hltr210.Cfg.SyncMode == LTR210_SYNC_MODE_INTERNAL);

    //��������� �������� �������� ������ ��� �������� ���������� � �� ���������� �����
    bool change_en = module_opened && !threadRunning && !load_progr;
    chkChEn1->Enabled = change_en;
    chkChEn2->Enabled = change_en;
    cbbRange1->Enabled = change_en;
    cbbRange2->Enabled = change_en;
    cbbMode1->Enabled = change_en;
    cbbMode2->Enabled = change_en;
    edtSyncLevelL1->Enabled = change_en;
    edtSyncLevelL2->Enabled = change_en;
    edtSyncLevelH1->Enabled = change_en;
    edtSyncLevelH2->Enabled = change_en;
    cbbDigBit1->Enabled = change_en;
    cbbDigBit2->Enabled = change_en;
    seFrameSize->Enabled = change_en;
    seHistSize->Enabled = change_en;
    edtAdcFreq->Enabled = change_en;
    edtFrameFreq->Enabled = change_en;
    cbbSyncMode->Enabled = change_en;
    cbbGroupMode->Enabled = change_en;
    chkKeepaliveEn->Enabled = change_en;
    chkWriteAutoSusp->Enabled = change_en;

}


void TMainForm::refreshDeviceList() {
    INT err = LTR_OK;
    int modules_cnt = 0;
    ltr210_list.Length = 0;
    cbbModulesList->Items->Clear();

    // ������������� ����� � ����������� ������� �������, ����� �������� ������ �������
    TLTR srv;
    LTR_Init(&srv);
    err=LTR_OpenSvcControl(&srv, LTRD_ADDR_DEFAULT, LTRD_PORT_DEFAULT);
    if (err!=LTR_OK) {
        MessageDlg("�� ������� ���������� ����� � ��������: " + String(LTR_GetErrorString(err)),
                    mtError, TMsgDlgButtons() << mbOK,NULL);
    } else {
        char serial_list[LTR_CRATES_MAX][LTR_CRATE_SERIAL_SIZE];
        //�������� ������ �������� ������� ���� ������������ �������
        err=LTR_GetCrates(&srv, &serial_list[0][0]);
        //��������� ���������� ������ �� ����� - ����� �������
        LTR_Close(&srv);
        if (err!=LTR_OK) {
             MessageDlg("�� ������� �������� ������ �������: " + String(LTR_GetErrorString(err)),
                    mtError, TMsgDlgButtons() << mbOK,NULL);
        } else {
            for (int crate_ind=0 ; crate_ind < LTR_CRATES_MAX; crate_ind++) {
                //������� ������ ������������� �������� �������� �����
                if (serial_list[crate_ind][0]!='\0') {
                    // ������������� ����� � ������ �������, ����� �������� ������ �������
                    TLTR crate;
                    LTR_Init(&crate);
                    err =  LTR_OpenCrate(&crate, LTRD_ADDR_DEFAULT, LTRD_PORT_DEFAULT,
                                         LTR_CRATE_IFACE_UNKNOWN, serial_list[crate_ind]);
                    if (err==LTR_OK) {
                        WORD mids[LTR_MODULES_PER_CRATE_MAX];
                        err=LTR_GetCrateModules(&crate, mids);
                        if (err==LTR_OK) {
                            for (int module_ind=0; module_ind < LTR_MODULES_PER_CRATE_MAX; module_ind++) {
                                //���� ������ LTR210
                                if (mids[module_ind]==LTR_MID_LTR210) {
                                    // ��������� ���������� � ��������� ������, ����������� ���
                                    // ������������ ������������ ���������� � ���, � ������
                                    modules_cnt++;
                                    ltr210_list.Length=modules_cnt;
                                    ltr210_list[modules_cnt-1].csn = String(serial_list[crate_ind]);
                                    ltr210_list[modules_cnt-1].slot = module_ind+LTR_CC_CHNUM_MODULE1;
                                    // � ��������� � ComboBox ��� ����������� ������ �������
                                    cbbModulesList->AddItem("����� " + ltr210_list[modules_cnt-1].csn +
                                            ", ���� " + IntToStr(ltr210_list[modules_cnt-1].slot), NULL);
                                }
                            }
                        }
                        //��������� ���������� � �������
                        LTR_Close(&crate);
                    }
                }
            }
        }
    }

    cbbModulesList->ItemIndex = 0;
    updateControls();    
}

void TMainForm::closeDevice() {
    // ��������� ����� � �������� ���������� ������
    if (threadRunning) {
        thread->stop=true;
        thread->WaitFor();
    }
    LTR210_Close(&hltr210);
}

//�������, ���������� �� ���������� ������ ����� ������
//��������� ����� ������, ������������� threadRunning
void __fastcall TMainForm::OnThreadTerminate(TObject *obj) {
    if (thread->err != LTR_OK) {
        MessageDlg("���� ������ �������� � �������: " + String(LTR210_GetErrorString(thread->err)),
                    mtError, TMsgDlgButtons() << mbOK,NULL);
    }

    threadRunning = false;
    updateControls();
}


void APIENTRY updateProg(void *cb_data, TLTR210 *hnd, DWORD done_size, DWORD full_size) {
    TProgressBar* bar = (TProgressBar*)cb_data;
    //��������� ProgressBar � ������������ � ��������� �������� ��������
    bar->Position = (int)(100.*done_size/full_size);
    //����� ��������� ������������ � ���������� ��������� ��������� ��������� ����������
    Application->ProcessMessages();
}



__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner) {
    refreshDeviceList();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::FormCreate(TObject *Sender) {
    thread = 0;
    LTR210_Init(&hltr210);

    refreshDeviceList();
}
//---------------------------------------------------------------------------



void __fastcall TMainForm::btnRefreshDevListClick(TObject *Sender) {
   refreshDeviceList();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::btnOpenClick(TObject *Sender) {
    // ���� ���������� � ������� ������� - �� ��������� �����
    if (LTR210_IsOpened(&hltr210)!=LTR_OK) {
        INT err;
        // ���������� � ������ � ����� ����� �� ������������ ������ �� �������
        // ������� ��������� ������
        TLTR_MODULE_LOCATION location = ltr210_list[ cbbModulesList->ItemIndex ];
        LTR210_Init(&hltr210);
        err=LTR210_Open(&hltr210, LTRD_ADDR_DEFAULT, LTRD_PORT_DEFAULT, location.csn.c_str(), location.slot);
        if (err!=LTR_OK) {
            MessageDlg("�� ������� ���������� ����� � �������: " + String(LTR210_GetErrorString(err)),
                    mtError, TMsgDlgButtons() << mbOK,NULL);
        } else {
            if (LTR210_FPGAIsLoaded(&hltr210) != LTR_OK) {
                load_progr = true;
                updateControls();
                /* ��������� ��������, ���������� � ����������. ��� ����������� ���������
                    ���������� ���������� ����������� �������, � ������� �������� ���������
                    �� ProgressBar, ������� ����� �������� */
                err=LTR210_LoadFPGA(&hltr210, NULL, updateProg, pbFpgaLoad);

                load_progr=false;
                updateControls();
                if (err!=LTR_OK) {
                    MessageDlg("�� ������� ��������� �������� ����: " + String(LTR210_GetErrorString(err)),
                                mtError, TMsgDlgButtons() << mbOK,NULL);
                }
            } else {
                //���� ���� �������� ����� ������������� ��������� �� 100%
                pbFpgaLoad->Position = 100;
            }
        }

        if (err==LTR_OK) {
            edtDevSerial->Text = String(hltr210.ModuleInfo.Serial);
            edtVerPld->Text = IntToStr(hltr210.ModuleInfo.VerPLD);
            edtVerFPGA->Text = IntToStr(hltr210.ModuleInfo.VerFPGA);
        } else {
            LTR210_Close(&hltr210);
        }
    } else {
        closeDevice();
    }

    updateControls();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::btnStartClick(TObject *Sender) {
    /* ��������� �������� �� ��������� ���������� � ���������������
        ���� ��������� ������. ��� �������� ����� �� �������� ���. ��������, ���
        ������� ������ ��������... */

    hltr210.Cfg.Ch[0].Enabled = chkChEn1->Checked;
    hltr210.Cfg.Ch[1].Enabled = chkChEn2->Checked;

    hltr210.Cfg.Ch[0].Range = cbbRange1->ItemIndex;
    hltr210.Cfg.Ch[1].Range = cbbRange2->ItemIndex;

    hltr210.Cfg.Ch[0].Mode = cbbMode1->ItemIndex;
    hltr210.Cfg.Ch[1].Mode = cbbMode2->ItemIndex;

    hltr210.Cfg.Ch[0].SyncLevelL = StrToFloat(edtSyncLevelL1->Text);
    hltr210.Cfg.Ch[1].SyncLevelL = StrToFloat(edtSyncLevelL2->Text);
    hltr210.Cfg.Ch[0].SyncLevelH = StrToFloat(edtSyncLevelH1->Text);
    hltr210.Cfg.Ch[1].SyncLevelH = StrToFloat(edtSyncLevelH2->Text);

    hltr210.Cfg.Ch[0].DigBitMode = cbbDigBit1->ItemIndex;
    hltr210.Cfg.Ch[1].DigBitMode = cbbDigBit2->ItemIndex;

    hltr210.Cfg.SyncMode  = cbbSyncMode->ItemIndex;
    hltr210.Cfg.GroupMode = cbbGroupMode->ItemIndex;
    hltr210.Cfg.FrameSize = seFrameSize->Value;
    hltr210.Cfg.HistSize  = seHistSize->Value;



    hltr210.Cfg.Flags = 0;
    if (chkKeepaliveEn->Checked)
        hltr210.Cfg.Flags |= LTR210_CFG_FLAGS_KEEPALIVE_EN;
    if (chkWriteAutoSusp->Checked)
        hltr210.Cfg.Flags |= LTR210_CFG_FLAGS_WRITE_AUTO_SUSP;

    INT err = LTR210_FillAdcFreq(  &hltr210.Cfg, StrToFloat(edtAdcFreq->Text), 0, NULL);
    if (err==LTR_OK)
        err = LTR210_FillFrameFreq(  &hltr210.Cfg, StrToFloat(edtFrameFreq->Text), NULL);
    if (err==LTR_OK)
        err = LTR210_SetADC(&hltr210);
    if (err!=LTR_OK) {
        MessageDlg("�� ������� ���������� ���������: " + String(LTR210_GetErrorString(err)),
                   mtError, TMsgDlgButtons() << mbOK,NULL);
    }

    /* �� ���������� ������ �������� ��������� ��������� ������������ ���� ��� ��� ��������� */
    if (err==LTR_OK){
        err = LTR210_MeasAdcZeroOffset(&hltr210, 0);
        if (err!=LTR_OK) {
            MessageDlg("�� ������� ��������� ��������� ������������ ����: " + String(LTR210_GetErrorString(err)),
                   mtError, TMsgDlgButtons() << mbOK,NULL);
        }
    }

    if (err==LTR_OK) {
        /*  ��������� �������� ������ �� ������� ������������� */
        edtAdcFreq->Text = FloatToStrF(hltr210.State.AdcFreq, ffFixed, 8, 2);
        if (hltr210.Cfg.SyncMode == LTR210_SYNC_MODE_PERIODIC)
            edtFrameFreq->Text = FloatToStrF(hltr210.State.FrameFreq, ffFixed, 8, 2);

        if (thread) {
            delete thread;
            thread = 0;
        }

        thread = new LTR210_ProcessThread(true);
        /* ��� ��� ��������� ������ ���� ���� � �� ��, ��� ������������ �������,
            ��� � ������ ��������� ����, �� ��������� �������� �� ��� ��������� */
        thread->phltr210 = &hltr210;
        /* ��������� �������� ����������, ������� ������ ���������� ��������������
            ������� � ����� ������ */
        thread->edtValidFrameCntr = edtValidFrameCntr;
        thread->edtInvalidFrameCntr = edtInvalidFrameCntr;
        thread->edtSyncSkipCntr = edtSyncSkipCntr;
        thread->edtOverlapCntr = edtOverlapCntr;
        thread->edtInvalidHistCntr = edtInvalidHistCntr;
        thread->edtChAvg[0]=edtCh1Avg;
        thread->edtChAvg[1]=edtCh2Avg;

         /* ������������� ������� �� ������� ���������� ������ (� ���������,
            ����� ���������, ���� ����� ���������� ��� ��-�� ������ ��� �����
            ������) */
        thread->OnTerminate = OnThreadTerminate;
        thread->Resume();
        threadRunning = true;

        updateControls();
    }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::FormClose(TObject *Sender, TCloseAction &Action) {
    closeDevice();
    delete thread;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::btnStopClick(TObject *Sender) {
    // ������������� ������ �� ���������� ������
    if (threadRunning)
        thread->stop=true;
    btnStop->Enabled = false;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::btnFrameStartClick(TObject *Sender) {
    INT err = LTR210_FrameStart(&hltr210);
    if (err!=LTR_OK) {
        MessageDlg("�� ������� ��������� ����������� ������ �����: " + String(LTR210_GetErrorString(err)),
                    mtError, TMsgDlgButtons() << mbOK,NULL);
    }
}
//---------------------------------------------------------------------------

