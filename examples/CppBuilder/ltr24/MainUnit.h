//---------------------------------------------------------------------------

#ifndef MainUnitH
#define MainUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>


#include "ltr/include/ltr24api.h"
#include "LTR24_ProcessThread.h"

/* ����������, ����������� ��� ������������ ���������� � ������� */
typedef struct  {
    AnsiString csn; //�������� ����� ������
    WORD slot; //����� �����
} TLTR_MODULE_LOCATION;


//---------------------------------------------------------------------------
class TMainForm : public TForm  {
__published:	// IDE-managed Components
    TComboBox *cbbModulesList;
    TButton *btnRefreshDevList;
    TButton *btnOpen;
    TButton *btnStart;
    TButton *btnStop;
    TGroupBox *grpDevInfo;
    TLabel *lblDevSerial;
    TLabel *lblVerPld;
    TLabel *lblVerFPGA;
    TEdit *edtDevSerial;
    TEdit *edtICPSupport;
    TEdit *edtVerPld;
    TGroupBox *grpConfig;
    TLabel *lblRange1;
    TLabel *lblChAc1;
    TLabel *lblDigBit1;
    TLabel *lblAdcFreq;
    TLabel *lblDataFmt;
    TLabel *lblISrcVal;
    TLabel *lblChAc2;
    TGroupBox *grpCfgCh1;
    TCheckBox *chkChEn1;
    TComboBox *cbbRange1;
    TComboBox *cbbAC1;
    TComboBox *cbbICPMode1;
    TComboBox *cbbAdcFreq;
    TCheckBox *chkTestModes;
    TComboBox *cbbDataFmt;
    TComboBox *cbbISrcValue;
    TGroupBox *grp1;
    TCheckBox *chkChEn2;
    TComboBox *cbbRange2;
    TComboBox *cbbAC2;
    TComboBox *cbbICPMode2;
    TGroupBox *grp2;
    TCheckBox *chkChEn3;
    TComboBox *cbbRange3;
    TComboBox *cbbAC3;
    TComboBox *cbbICPMode3;
    TGroupBox *grp3;
    TCheckBox *chkChEn4;
    TComboBox *cbbRange4;
    TComboBox *cbbAC4;
    TComboBox *cbbICPMode4;
    TGroupBox *grpResult;
    TEdit *edtCh1Avg;
    TEdit *edtCh2Avg;
    TEdit *edtCh3Avg;
    TEdit *edtCh4Avg;
    void __fastcall cfgChanged(TObject *Sender);
    void __fastcall btnRefreshDevListClick(TObject *Sender);
    void __fastcall FormCreate(TObject *Sender);
    void __fastcall btnOpenClick(TObject *Sender);
    void __fastcall btnStartClick(TObject *Sender);
    void __fastcall btnStopClick(TObject *Sender);
    void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
private:	// User declarations
    DynamicArray<TLTR_MODULE_LOCATION> ltr24_list; //������ ��������� �������

    TLTR24 hltr24; // ��������� ������, � ������� ���� ������
    bool threadRunning; // �������, ������� �� ����� ����� ������
    TLTR24_ProcessThread* thread; //������ ������ ��� ���������� ����� ������

    void updateControls();
    void refreshDeviceList();
    void closeDevice();
    void __fastcall OnThreadTerminate(TObject *obj);


    void assignComboList(TComboBox *comboBox, TStringList *list);
    void setRangeComboList(TComboBox *rangeBox, TComboBox *icpBox);
public:		// User declarations
    __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
