//---------------------------------------------------------------------------


#pragma hdrstop

#include "LTR24_ProcessThread.h"

// �����, �� ������� ����� ������������ ������� �������� (� ��)
#define RECV_BLOCK_TIME          500
// ��������������  ���������� ������� �� ����� ������ (� ��)
#define RECV_TOUT                4000


//---------------------------------------------------------------------------

#pragma package(smart_init)

 __fastcall TLTR24_ProcessThread::TLTR24_ProcessThread(bool CreateSuspended)
    : TThread(CreateSuspended), stop(false), err (LTR_OK) {

}

/* ���������� ����������� ����� ������������ ���������� ���������.
   ����� ������ ����������� ������ ����� Syncronize, ������� �����
   ��� ������� � ��������� VCL �� �� ��������� ������ */
void __fastcall TLTR24_ProcessThread::updateData() {
    for (int ch=0; ch < LTR24_CHANNEL_NUM; ch++) {
        if (ChValidData[ch]) {
            edtChAvg[ch]->Text = FloatToStrF(ChAvg[ch], ffFixed, 4, 8);
        } else {
            edtChAvg[ch]->Text = "";
        }
    }
}


void __fastcall TLTR24_ProcessThread::Execute() {
    DWORD *rcv_buf=0;
    double *data=0;
    INT err = LTR_OK;
    // ������ ����������� �������
    BYTE ch_nums  [LTR24_CHANNEL_NUM];
    INT ch_cnt=0;  //���������� ����������� �������
    INT recv_wrd_cnt;  //���������� ����������� ����� ���� �� ���
    INT recv_data_cnt; //���������� ������������ ����, ������� ������ ������� �� ���

    //�������� ����������
    for (INT ch=0; ch < LTR24_CHANNEL_NUM; ch++)
        ChValidData[ch]=false;
    Synchronize(updateData);

    //���������, ������� � ����� ������ ���������
    for (INT ch=0; ch < LTR24_CHANNEL_NUM; ch++) {
        if (phltr24->ChannelMode[ch].Enable) {
            ch_nums[ch_cnt] = ch;
            ch_cnt++;
        }
    }

    /* ����������, ������ �������������� ����� ���������� �� �������� �����
      => ����� ��������� ������ ������� ������ ������� */
    recv_data_cnt =  (INT)(phltr24->ADCFreq*RECV_BLOCK_TIME/1000) * ch_cnt;
    /* � 24-������ ������� ������� ������� ������������� ��� ����� �� ������,
                   � � 20-������ - ���� */
    recv_wrd_cnt = phltr24->DataFmt == LTR24_FORMAT_24 ? 2*recv_data_cnt : recv_data_cnt;

    //�������� ������� ��� ������ ������
    rcv_buf = new DWORD[recv_wrd_cnt];
    data = new double[recv_data_cnt];

    err = LTR24_Start(phltr24);
    if (err==LTR_OK) {
        while (!stop && (err==LTR_OK)) {
            /* ��������� ������ */
            INT recv_size = LTR24_Recv(phltr24, rcv_buf, 0, recv_wrd_cnt, RECV_TOUT + RECV_BLOCK_TIME);
            //�������� ������ ���� ������������� ���� ������
            if (recv_size < 0) {
                err=recv_size;
            } else if (recv_size < recv_wrd_cnt) {
                err=LTR_ERROR_RECV_INSUFFICIENT_DATA;
            } else {
                double ch_avg[LTR24_CHANNEL_NUM];
                bool   ch_valid[LTR24_CHANNEL_NUM];

                err=LTR24_ProcessData(phltr24, rcv_buf, data, &recv_size,
                                        LTR24_PROC_FLAG_CALIBR |
                                        LTR24_PROC_FLAG_VOLT |
										LTR24_PROC_FLAG_AFC_COR_EX |
										LTR24_PROC_FLAG_ICP_PHASE_COR, 0);
                if (err==LTR_OK) {
                    for (INT ch=0; ch < LTR24_CHANNEL_NUM; ch++) {
                        ch_avg[ch] =  0;
                        ch_valid[ch] = false;
                    }

                    // �������� ���-�� �������� �� �����
                    recv_size = (INT) (recv_size/ch_cnt);

                    // ������� ��������
                    for (INT i=0; i < recv_size; i++) {
                        for (INT ch=0; ch < ch_cnt; ch++) {
                            ch_avg[ch_nums[ch]] +=  data[ch_cnt*i + ch];
                            ch_valid[ch_nums[ch]] = true;
                        }
                    }

                    for (INT ch=0; ch < LTR24_CHANNEL_NUM; ch++) {
                        if (ch_valid[ch])
                            ChAvg[ch]=ch_avg[ch]/recv_size;
                        ChValidData[ch]= ch_valid[ch];
                    }
                    // ��������� �������� ��������� ����������
                    Synchronize(updateData);
                }
            }
        } // while (!stop && (err==LTR_OK))

        /* �� ������ �� ����� ������������� ���� ������.
            ����� �� �������� ��� ������ (���� ����� �� ������)
            ��������� �������� ��������� � ��������� ���������� */
        INT stoperr= LTR24_Stop(phltr24);
        if (err == LTR_OK)
            err= stoperr;
    }

    delete []rcv_buf;
    delete []data;
}
