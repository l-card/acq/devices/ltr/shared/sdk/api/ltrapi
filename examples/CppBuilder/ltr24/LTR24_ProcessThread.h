//---------------------------------------------------------------------------

#ifndef LTR24_PROCESS_THREAD_H_
#define LTR24_PROCESS_THREAD_H_

#include "ltr/include/ltr24api.h"
#include <vcl.h>


class TLTR24_ProcessThread : public TThread {

protected:
    void __fastcall Execute();

public:
     //�������� ���������� ��� ����������� ����������� ���������
    TEdit* edtChAvg[LTR24_CHANNEL_NUM];

    TLTR24* phltr24; //��������� ������

    bool stop;  //������ �� ������� (��������������� �� ��������� ������)
    INT err;  //��� ������ ��� ���������� ������ �����

    __fastcall TLTR24_ProcessThread(bool CreateSuspended);

private:
    void __fastcall updateData() ;

     // ������� � ����� �� ������� ������
    double ChAvg [LTR24_CHANNEL_NUM];
    // �������, ��� ���� ����������� ������ �� ������� � ChAvg
    bool ChValidData[LTR24_CHANNEL_NUM];
};



//---------------------------------------------------------------------------
#endif