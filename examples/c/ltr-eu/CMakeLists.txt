cmake_minimum_required(VERSION 2.8.12)

add_subdirectory(ltreu_sec_start)
add_subdirectory(ltreu_start_mark)
add_subdirectory(ltreu_irig)
add_subdirectory(ltreu_cfg_test)
add_subdirectory(ltreu_flash_test)
add_subdirectory(ltreu_custom_ctlreq)
