#include "ltr/include/ltrapi.h"
#include <stdio.h>
#include <string.h>

#ifdef _WIN32
#include <locale.h>
#endif


typedef struct {
    const char *serial;
    DWORD addr;
} t_open_param;

/* Разбор параметров командной строки. Если указано меньше, то используются
 * значения по умолчанию:
 * 1 параметр - серийный номер крейта
 * 2 параметр - ip-адрес сервера */
static int f_get_params(int argc, char** argv, t_open_param* par) {
    int err = 0;
    par->serial = "";
    par->addr = LTRD_ADDR_DEFAULT;

    if (argc > 1)
        par->serial = argv[1];
    if (argc > 2) {
        int a[4],i;
        if (sscanf(argv[2], "%d.%d.%d.%d", &a[0], &a[1], &a[2], &a[3])!=4) {
            fprintf(stderr, "Неверный формат IP-адреса!!\n");
            err = -1;
        }

        for (i=0; (i < 4) && !err; i++) {
            if ((a[i]<0) || (a[i] > 255)) {
                fprintf(stderr, "Недействительный IP-адрес!!\n");
                err = -1;
            }
        }

        if (!err) {
            par->addr = (a[0] << 24) | (a[1]<<16) | (a[2]<<8) | a[3];
        }
    }
    return err;
}


int main(int argc, char **argv) {
    INT err;
    t_open_param par;

#ifdef _WIN32
    /* для вывода русских букв в консоль для ОС Windows*/
    setlocale(LC_ALL, "");
#endif

    err = f_get_params(argc, argv, &par);

    if (err==LTR_OK) {
        TLTR hcrate;
        LTR_Init(&hcrate);
        err = LTR_OpenCrate(&hcrate, par.addr, LTRD_PORT_DEFAULT, LTR_CRATE_IFACE_UNKNOWN, par.serial);
        if (err!=LTR_OK) {
            fprintf(stderr, "Невозможно установить связь с крейтом! Ошибка %d: %s\n",
                    err, LTR_GetErrorString(err));
        } else {

            err = LTR_MakeStartMark(&hcrate, LTR_MARK_INTERNAL);
            if (err!=LTR_OK) {
                fprintf(stderr, "Не удалось сгенерировать метку СТАРТ! Ошибка %d: %s\n",
                        err, LTR_GetErrorString(err));
            }

            LTR_Close(&hcrate);
        }
    }
   return -err;
}
