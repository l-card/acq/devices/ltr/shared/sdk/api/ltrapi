#include "ltr/include/ltr030api.h"
#include <stdio.h>
#include <string.h>
#include <stddef.h>

#ifdef _WIN32
#include <locale.h>
#include <conio.h>
#else
#include <signal.h>
#include <unistd.h>
#endif









int main(int argc, char **argv) {
    INT err;
    TLTR030 hcrate;
    LTR030_Init(&hcrate);
    err = LTR030_Open(&hcrate, LTRD_ADDR_DEFAULT, LTRD_PORT_DEFAULT, LTR_CRATE_IFACE_USB, NULL);
    if (err == LTR_OK) {
        TLTR030_CONFIG cfg;
        BYTE fabric_mac[6];
        cfg.Size = sizeof(cfg);
        err = LTR030_GetConfig(&hcrate, &cfg);
        if (err == LTR_OK) {
            printf("LTR-EU config:\n");
            printf("  iface = %d\n", cfg.Interface);
            printf("  ip_addr = %d.%d.%d.%d\n",
                   (cfg.IpAddr >> 24) & 0xFF, (cfg.IpAddr >> 16) & 0xFF,
                   (cfg.IpAddr >> 8) & 0xFF, cfg.IpAddr & 0xFF);
            printf("  ip_mask = %d.%d.%d.%d\n",
                   (cfg.IpMask >> 24) & 0xFF, (cfg.IpMask >> 16) & 0xFF,
                   (cfg.IpMask >> 8) & 0xFF, cfg.IpMask & 0xFF);
            printf("  ip_gate = %d.%d.%d.%d\n",
                   (cfg.Gateway >> 24) & 0xFF, (cfg.Gateway >> 16) & 0xFF,
                   (cfg.Gateway >> 8) & 0xFF, cfg.Gateway & 0xFF);
            if (cfg.Size > offsetof(TLTR030_CONFIG, UserMac)) {
                printf("  user mac = %02X:%02X:%02X:%02X:%02X:%02X\n",
                       cfg.UserMac[0], cfg.UserMac[1], cfg.UserMac[2],
                       cfg.UserMac[3], cfg.UserMac[4], cfg.UserMac[5]);
            }
        }

        err = LTR030_GetFactoryMac(&hcrate, fabric_mac);
        if (err == LTR_OK) {
            printf("  fabric mac = %02X:%02X:%02X:%02X:%02X:%02X\n",
                   fabric_mac[0], fabric_mac[1], fabric_mac[2],
                   fabric_mac[3], fabric_mac[4], fabric_mac[5]);
        }


        if (err == LTR_OK) {
            cfg.UserMac[0] = 1;
            cfg.UserMac[1] = 2;
            cfg.UserMac[2] = 3;
            cfg.UserMac[3] = 4;
            cfg.UserMac[4] = 5;

            err = LTR030_SetConfig(&hcrate, &cfg);
        }


    }

    return -err;
}
