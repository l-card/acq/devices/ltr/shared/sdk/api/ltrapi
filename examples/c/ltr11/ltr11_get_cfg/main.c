
#include "ltr/include/ltr11api.h"
/* остальные заголовочные файлы */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#ifdef _WIN32
#include <locale.h>
#include <conio.h>
#else
#include <signal.h>
#include <unistd.h>
#endif



typedef struct {
    int slot;
    const char *serial;
    DWORD addr;
} t_open_param;


/* признак необходимости завершить сбор данных */
static int f_out = 0;

#ifndef _WIN32
/* Обработчик сигнала завершения для Linux */
static void f_abort_handler(int sig) {
    f_out = 1;
}
#endif


/* Разбор параметров командной строки. Если указано меньше, то используются
 * значения по умолчанию:
 * 1 параметр - номер слота (от 1 до 16)
 * 2 параметр - серийный номер крейта
 * 3 параметр - ip-адрес сервера */
static int f_get_params(int argc, char** argv, t_open_param* par) {
    int err = 0;
    par->slot = LTR_CC_CHNUM_MODULE1;
    par->serial = "";
    par->addr = LTRD_ADDR_DEFAULT;


    if (argc > 1)
        par->slot = atoi(argv[1]);
    if (argc > 2)
        par->serial = argv[2];
    if (argc > 3) {
        int a[4],i;
        if (sscanf(argv[3], "%d.%d.%d.%d", &a[0], &a[1], &a[2], &a[3])!=4) {
            fprintf(stderr, "Неверный формат IP-адреса!!\n");
            err = -1;
        }

        for (i=0; (i < 4) && !err; i++) {
            if ((a[i]<0) || (a[i] > 255)) {
                fprintf(stderr, "Недействительный IP-адрес!!\n");
                err = -1;
            }
        }

        if (!err) {
            par->addr = (a[0] << 24) | (a[1]<<16) | (a[2]<<8) | a[3];
        }
    }
    return err;
}



/*-----------------------------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
    TLTR11 hltr11;
    INT  err;
    t_open_param par;
#ifndef _WIN32
    struct sigaction sa;
    memset(&sa, 0, sizeof(sa));
    /* В ОС Linux устанавливаем свой обработчик на сигнал закрытия,
       чтобы завершить сбор корректно */
    sa.sa_handler = f_abort_handler;
    sigaction(SIGTERM, &sa, NULL);
    sigaction(SIGINT, &sa, NULL);
    sigaction(SIGABRT, &sa, NULL);
#endif
#ifdef _WIN32
    /* для вывода русских букв в консоль для ОС Windows*/
    setlocale(LC_ALL, "");
#endif
    err = f_get_params(argc, argv, &par);

    if (!err) {
        /* инициализация дескриптора модуля */
        LTR11_Init(&hltr11);
        /* открытие канала связи с модулем, установленным в заданный слот */
        err = LTR11_Open(&hltr11, par.addr, LTRD_PORT_DEFAULT, par.serial, par.slot);
    }
    if (err != LTR_OK) {
        fprintf(stderr, "Не удалось установить связь с модулем. Ошибка %d (%s)\n",
                err, LTR11_GetErrorString(err));
    } else {
        while ((err == LTR_OK) && !f_out) {
            /* получение информации о модуле из flash-памяти */
            err = LTR11_GetConfig(&hltr11);
            if (err != LTR_OK) {
                fprintf(stdout, "\n");
                fflush(stdout);
                fprintf(stderr, "Не удалось прочитать информацию о модуле. Ошибка %d: %s\n", err,
                                 LTR11_GetErrorString(err));
            } else {
                fprintf(stdout, ".");
                fflush(stdout);
            }
#ifdef _WIN32
            /* проверка нажатия клавиши для выхода */
            if (err==LTR_OK) {
                if (_kbhit())
                    f_out = 1;
            }
#endif
        }
    }

    if (LTR11_IsOpened(&hltr11) == LTR_OK) {
        /* закрытие канала связи с модулем */
        LTR11_Close(&hltr11);
    }
        
    return err;
}



