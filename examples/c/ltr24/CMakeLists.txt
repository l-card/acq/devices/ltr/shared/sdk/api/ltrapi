cmake_minimum_required(VERSION 2.8.12)

add_subdirectory(ltr24_recv)

if(LTRAPI_USE_KD_STORESLOTS)
    add_subdirectory(ltr24_recv_ex)
    add_subdirectory(ltr24_ex)
endif(LTRAPI_USE_KD_STORESLOTS)

add_subdirectory(ltr24_show_info)


if(LTRAPI_BUILD_MATH_EXAMPLES)
    add_subdirectory(ltr24_pha)
endif(LTRAPI_BUILD_MATH_EXAMPLES)
