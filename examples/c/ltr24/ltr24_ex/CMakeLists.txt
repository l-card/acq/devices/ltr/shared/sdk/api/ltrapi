cmake_minimum_required(VERSION 2.6)

set(PROJECT ltr24_ex)

project(${PROJECT} C)

set(SOURCES main.c)

set(GETOPT_SRC_DIR ${LTRAPI_LIB_DIR}/getopt)
include(${GETOPT_SRC_DIR}/getopt.cmake)

include_directories(${LTRAPI_INCLUDE_DIR})
link_directories(${LTRAPI_LIBRARIES_DIR})

add_executable(${PROJECT} ${HEADERS} ${SOURCES})

target_link_libraries(${PROJECT} ltr24api)

if(${CMAKE_SYSTEM_NAME} STREQUAL "QNX4")
    set_target_properties(${PROJECT} PROPERTIES LINK_FLAGS "op st=32k")
endif()


install(TARGETS ${PROJECT} DESTINATION ${LTRAPI_INSTALL_EXAMPLES})
