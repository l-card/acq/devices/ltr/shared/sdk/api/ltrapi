/*
    Пример находит все модули LTR24 и выводит о них информацию

    Сборка в VisualStudio:
    Для того чтобы собрать проект в Visual Studio, измените путь к заголовочным файлам
    (Проект (Project) -> Свойства (Properties) -> Свойства конфигурации (Configuration Properties)
    -> С/С++ -> Общие (General) -> Дополнительные каталоги включения (Additional Include Directories))
    на нужный в зависимаости от пути установки библиотек (ltr24api.h  и остальные заголовочные
    файлы должны находится в поддиректории ltr/include относительно указанного пути)
    и измените путь к .lib файлам на <путь установки библиотек>/lib/msvc
    (Проект (Project) -> Свойства (Properties) -> Свойства конфигурации (Configuration Properties) ->
    Компоновщик (Linker) -> Общие (General) -> Дополнительные каталоги библиотек (Additional Library Directories)).

    Внимание!: Если Вы собираете проект под Visual Studio и у Вас некорректно
    отображается вывод русских букв, то нужно изменить кодировку:
    выберите Файл (File) -> Дополнительные параметры сохранения (Advanced Save Options)...
    и в поле Кодировка (Encoding) выберите Юникод (UTF8, с сигнатурой)/Unicode (UTF-8 with signature)
    и сохраните изменения в файле.
*/

#include "ltr/include/ltrapi.h"
#include "ltr/include/ltr24api.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef _WIN32
#include <locale.h>
#endif




/* Отображение информации о модуле LTR24 по заданному серийному номеру крейта и слоту */
static int f_ltr24_show_info(const char* csn, int slot) {
    INT err = LTR_OK;
    TLTR24 hltr24;

    printf("Модуль LTR24 (Крейт %s, слот %2d):\n", csn, slot);

    LTR24_Init(&hltr24);
    /* Устанавливаем соединение с модулем */
    err = LTR24_Open(&hltr24, LTRD_ADDR_DEFAULT, LTRD_PORT_DEFAULT, csn, slot);
    if (err!=LTR_OK) {
        fprintf(stderr, "  Не удалось установить связь с модулем. Ошибка %d (%s)\n",
                err, LTR24_GetErrorString(err));
    } else {
        INT close_err;

        /* Читаем информацию о модуле, включая калибровки, из Flash-памяти
         * модуля. Без вызова этой функции нельзя будет получить правильные
         * откалиброванные значения */
        err = LTR24_GetConfig(&hltr24);
        if (err!=LTR_OK) {
            fprintf(stderr, "  Не удалось прочитать информацию о модуле из Flash-памяти. Ошибка %d (%s)\n",
                    err, LTR24_GetErrorString(err));
        } else {
            /* Выводим прочитанную информацию о модуле */
            printf("  Модуль открыт успешно! Информация о модуле: \n");
            printf("    Название модуля    = %s\n", hltr24.ModuleInfo.Name);
            printf("    Серийный номер     = %s\n", hltr24.ModuleInfo.Serial);
            printf("    Версия PLD         = %d\n", hltr24.ModuleInfo.VerPLD);
            printf("    Поддержка ICP      = %s\n", hltr24.ModuleInfo.SupportICP ? "Есть" : "Нет");
            fflush(stdout);
        }


        close_err = LTR24_Close(&hltr24);
        if (close_err!=LTR_OK) {
            fprintf(stderr, "  Не удалось закрыть связь с модулем. Ошибка %d (%s)\n",
                    close_err, LTR24_GetErrorString(close_err));
            if (err==LTR_OK)
                err = close_err;
        }
    }
    return err;
}

int main(int argc, char** argv) {
    INT err = LTR_OK;
    TLTR srv;

#ifdef _WIN32
    /* для вывода русских букв в консоль для ОС Windows*/
    setlocale(LC_ALL, "");
#endif

    LTR_Init(&srv);
    err=LTR_OpenSvcControl(&srv, LTRD_ADDR_DEFAULT, LTRD_PORT_DEFAULT);
    if (err!=LTR_OK) {
        fprintf(stderr, "Не удалось установить связь с LTR-сервером. Ошибка %d: %s!\n",
                err, LTR_GetErrorString(err));
    } else {
        char serial_list[LTR_CRATES_MAX][LTR_CRATE_SERIAL_SIZE];
        err=LTR_GetCrates(&srv, (BYTE*)&serial_list[0][0]);
        LTR_Close(&srv);
        if (err!=LTR_OK) {
            fprintf(stderr, "Не удалось получить список крейтов. Ошибка %d: %s!\n",
                    err, LTR_GetErrorString(err));
        } else {
            int crate_ind;
            for (crate_ind=0 ; crate_ind < LTR_CRATES_MAX; crate_ind++) {
                if (serial_list[crate_ind][0]!='\0') {
                    TLTR crate;
                    INT crate_err = LTR_OK;
                    LTR_Init(&crate);
                    crate_err =  LTR_OpenCrate(&crate, LTRD_ADDR_DEFAULT, LTRD_PORT_DEFAULT,
                                               LTR_CRATE_IFACE_UNKNOWN, serial_list[crate_ind]);
                    if (crate_err==LTR_OK) {
                        WORD mids[LTR_MODULES_PER_CRATE_MAX];
                        crate_err=LTR_GetCrateModules(&crate, mids);
                        if (crate_err==LTR_OK) {
                            int module_ind;
                            for (module_ind=0; module_ind < LTR_MODULES_PER_CRATE_MAX; module_ind++) {
                                if (mids[module_ind]==LTR_MID_LTR24) {
                                    f_ltr24_show_info(crate.csn, LTR_CC_CHNUM_MODULE1+module_ind);
                                }
                            }
                        }
                        LTR_Close(&crate);
                    }
                }
            }
        }
    }

    return err;
}
