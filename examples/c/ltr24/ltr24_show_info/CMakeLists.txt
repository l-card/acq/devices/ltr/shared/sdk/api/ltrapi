cmake_minimum_required(VERSION 2.8.12)

set(PROJECT ltr24_show_info)

project(${PROJECT} C)

set(SOURCES main.c)

include_directories(${LTRAPI_INCLUDE_DIR})
link_directories(${LTRAPI_LIBRARIES_DIR})

add_executable(${PROJECT} ${HEADERS} ${SOURCES})

target_link_libraries(${PROJECT} ltrapi ltr24api)

if(${CMAKE_SYSTEM_NAME} STREQUAL "QNX4")
    set_target_properties(${PROJECT} PROPERTIES LINK_FLAGS "op st=32k")
endif()


install(TARGETS ${PROJECT} DESTINATION ${LTRAPI_INSTALL_EXAMPLES})
