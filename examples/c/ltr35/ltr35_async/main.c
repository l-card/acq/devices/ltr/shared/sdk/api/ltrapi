#include "ltr/include/ltr35api.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef _WIN32
#include <locale.h>
#include <conio.h>
#else
#include <signal.h>
#include <unistd.h>
#endif






//#define DIGOUT_ENABLE




typedef struct {
    int slot;
    const char *serial;
    DWORD addr;
} t_open_param;


/* признак необходимости завершить передачу данных */
static int f_out = 0;

#ifndef _WIN32
/* Обработчик сигнала завершения для Linux */
static void f_abort_handler(int sig) {
    f_out = 1;
}
#endif

static int f_set_vals(TLTR35 *phltr35, double val) {
    static DWORD   wrds[LTR35_DAC_CHANNEL_CNT];
    static double  sig[LTR35_DAC_CHANNEL_CNT];
    DWORD i, snd_size, dac_size = 0;
    INT err;


    for (i = 0; i < phltr35->State.SDRAMChCnt; i++) {
        sig[dac_size++] = val;
    }

    err = LTR35_PrepareData(phltr35, sig,&dac_size, NULL, NULL,
                                   LTR35_PREP_FLAGS_VOLT, wrds, &snd_size);
    if (err == LTR_OK) {
        INT cur_sent = LTR35_Send(phltr35, wrds, snd_size, 2000);
        if (cur_sent < 0) {
            err = cur_sent;
            fprintf(stderr, "Ошибка передачи данных (%d) : %s!\n", err, LTR35_GetErrorString(err));
        } else if ((DWORD)cur_sent != snd_size) {
            err = LTR_ERROR_SEND_INSUFFICIENT_DATA;
            fprintf(stderr, "Передано недостаточно данных. Передавали %d, передано %d\n", snd_size, cur_sent);
        }
    }
    return  err;
}


/* Разбор параметров командной строки. Если указано меньше, то используются
 * значения по умолчанию:
 * 1 параметр - номер слота (от 1 до 16)
 * 2 параметр - серийный номер крейта
 * 3 параметр - ip-адрес сервера */
static int f_get_params(int argc, char** argv, t_open_param* par) {
    int err = 0;
    par->slot = LTR_CC_CHNUM_MODULE1;
    par->serial = "";
    par->addr = LTRD_ADDR_DEFAULT;


    if (argc > 1)
        par->slot = atoi(argv[1]);
    if (argc > 2)
        par->serial = argv[2];
    if (argc > 3) {
        int a[4],i;
        if (sscanf(argv[3], "%d.%d.%d.%d", &a[0], &a[1], &a[2], &a[3])!=4) {
            fprintf(stderr, "Неверный формат IP-адреса!!\n");
            err = -1;
        }

        for (i=0; (i < 4) && !err; i++) {
            if ((a[i]<0) || (a[i] > 255)) {
                fprintf(stderr, "Недействительный IP-адрес!!\n");
                err = -1;
            }
        }

        if (!err) {
            par->addr = (a[0] << 24) | (a[1]<<16) | (a[2]<<8) | a[3];
        }
    }
    return err;
}


int main(int argc, char** argv) {
    t_open_param par;
    INT err = LTR_OK;

#ifndef _WIN32
    struct sigaction sa;
    memset(&sa, 0, sizeof(sa));
    /* В ОС Linux устанавливаем свой обработчик на сигнал закрытия,
       чтобы завершить сбор корректно */
    sa.sa_handler = f_abort_handler;
    sigaction(SIGTERM, &sa, NULL);
    sigaction(SIGINT, &sa, NULL);
    sigaction(SIGABRT, &sa, NULL);
#endif

#ifdef _WIN32
    /* для вывода русских букв в консоле... */
    setlocale(LC_ALL, "");
#endif


    err = f_get_params(argc, argv, &par);

    if (!err) {
        TLTR35 hltr35;
        LTR35_Init(&hltr35);
        /* Устанавливаем соединение с модулем */
        err = LTR35_Open(&hltr35, par.addr, LTRD_PORT_DEFAULT, par.serial, par.slot);
        if (err!=LTR_OK) {
            fprintf(stderr, "Не удалось установить связь с модулем. Ошибка %d (%s)\n",
                    err, LTR35_GetErrorString(err));
        } else {
            double fnd_freq;
            double snd_val = 1;

            printf("Модуль открыт успешно!\n");
            printf("  Название модуля    = %s\n", hltr35.ModuleInfo.Name);
            printf("  Серийный номер     = %s\n", hltr35.ModuleInfo.Serial);
            printf("  Модификация        = LTR35-%d\n", hltr35.ModuleInfo.Modification);
            printf("  Версия PLD         = %d\n", hltr35.ModuleInfo.VerPLD);
            printf("  Версия ПЛИС        = %d\n", hltr35.ModuleInfo.VerFPGA);
            printf("  Выходов ЦАП        = %d\n", hltr35.ModuleInfo.DacChCnt);
            printf("  Цифровых выходов   = %d\n", hltr35.ModuleInfo.DoutLineCnt);

            fflush(stdout);

            /* выбираем частоту ЦАП - 192 КГц */
            err = LTR35_FillOutFreq(&hltr35.Cfg, 192*1000, &fnd_freq);
            if (err==LTR_OK) {
                hltr35.ModuleInfo.CbrAfcCoef.Valid = TRUE;

                hltr35.Cfg.OutDataFmt = LTR35_OUTDATA_FORMAT_20;
                hltr35.Cfg.OutMode = LTR35_OUT_MODE_STREAM;
                /* первый канал разрешаем потоковым */
                hltr35.Cfg.Ch[0].Enabled = TRUE;
                hltr35.Cfg.Ch[0].Source = LTR35_CH_SRC_SDRAM;

                /* настраиваем параметры модуля */
                err = LTR35_Configure(&hltr35);
                if (err!=LTR_OK) {
                    fprintf(stderr, "Не удалось настроить модуль. Ошибка %d (%s)\n",
                            err, LTR35_GetErrorString(err));
                }
            }

            /* Чтобы данные уже были при старте, подкачиваем один блок данных */
            if (err==LTR_OK) {                
                err = f_set_vals(&hltr35, 0);
            }

            if (err==LTR_OK) {
                err = LTR35_StreamStart(&hltr35, 0);
                if (err!=LTR_OK) {
                    fprintf(stderr, "Ошибка запуска потокового режима (%d) : %s!\n",
                            err, LTR35_GetErrorString(err));
                } else {
                    printf("Запущен потоковый вывод!\n"); fflush(stdout);
                }
            }

            while ((err==LTR_OK) && !f_out) {
                err = f_set_vals(&hltr35, snd_val);
                snd_val += 0.5;
                if (snd_val > 10)
                    snd_val = -0.5;
            }

            /* останавливаем выдачу данных, если она была запущена */
            if (hltr35.State.Run) {
                //INT stop_err = LTR35_StopWithTout(&hltr35, 100);
                INT stop_err = LTR35_StopWithTout(&hltr35, 0, 15000);
                if (stop_err!=LTR_OK) {
                    fprintf(stderr, "Не удалось остановить потоковый вывод. Ошибка %d:%s\n",
                            stop_err, LTR35_GetErrorString(stop_err));
                    if (err==LTR_OK)
                        err = stop_err;
                } else {
                    printf("Потоковый вывод остановлен успешно.\n");
                }
            }
        }

        if (LTR35_IsOpened(&hltr35) == LTR_OK) {
            /* закрываем связь с модулем */
            INT close_err = LTR35_Close(&hltr35);
            if (close_err!=LTR_OK) {
                fprintf(stderr, "Не удалось закрыть связь с модулем. Ошибка %d:%s\n",
                        close_err, LTR35_GetErrorString(close_err));
                if (err==LTR_OK)
                    err = close_err;
            } else {
                printf("Связь с модулем успешно закрыта.\n");
            }
        }
    }

    return err;
}
