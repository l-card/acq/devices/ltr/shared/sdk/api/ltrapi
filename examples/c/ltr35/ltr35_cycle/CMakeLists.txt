cmake_minimum_required(VERSION 2.8.12)

set(PROJECT ltr35_cycle)

project(${PROJECT} C)


set(SOURCES main.c)


if(CMAKE_COMPILER_IS_GNUCC)
    set(WARNOPTS
        -Werror=implicit-int -Werror=implicit-function-declaration -Werror=strict-prototypes -Werror=return-type
        -Wall -Wextra
        -Wformat-security -Winit-self -Wstrict-aliasing -Wfloat-equal
        -Wundef -Wshadow -Wunsafe-loop-optimizations -Wpointer-arith -Wcast-align
        -Wwrite-strings  -Wsign-compare -Wlogical-op -Waggregate-return -Winline
        -Wno-unused-parameter -Wno-unused-variable -Wno-aggregate-return)
    add_definitions(${WARNOPTS})
endif(CMAKE_COMPILER_IS_GNUCC)

if(MSVC)
    add_definitions(-D_CRT_SECURE_NO_WARNINGS)

    if( CMAKE_SIZEOF_VOID_P EQUAL 4 )
        SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} /SUBSYSTEM:WINDOWS,5.01")
    endif( CMAKE_SIZEOF_VOID_P EQUAL 4 )
    foreach(flag_var
            CMAKE_C_FLAGS CMAKE_C_FLAGS_DEBUG CMAKE_C_FLAGS_RELEASE
            CMAKE_C_FLAGS_MINSIZEREL CMAKE_C_FLAGS_RELWITHDEBINFO)
       if(${flag_var} MATCHES "/MD")
          string(REGEX REPLACE "/MD" "/MT" ${flag_var} "${${flag_var}}")
       endif(${flag_var} MATCHES "/MD")
    endforeach(flag_var)
endif(MSVC)

include_directories(${LTRAPI_INCLUDE_DIR})
link_directories(${LTRAPI_LIBRARIES_DIR})

set(GETOPT_SRC_DIR ${LTRAPI_LIB_DIR}/getopt)
include(${GETOPT_SRC_DIR}/getopt.cmake)

add_executable(${PROJECT} ${HEADERS} ${SOURCES})

target_link_libraries(${PROJECT} ltr35api)
if(UNIX)
    target_link_libraries(${PROJECT} m)
endif(UNIX)

install(TARGETS ${PROJECT} DESTINATION ${LTRAPI_INSTALL_EXAMPLES})
