cmake_minimum_required(VERSION 2.8.12)

if(LTRAPI_USE_KD_STORESLOTS)
    add_subdirectory(ltr27_ex)
endif(LTRAPI_USE_KD_STORESLOTS)
add_subdirectory(ltr27_recv)
add_subdirectory(ltr27_get_descr)
