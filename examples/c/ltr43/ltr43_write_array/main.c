#include "ltr/include/ltr43api.h"
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <string.h>
#ifdef _WIN32
#include <locale.h>
#include <conio.h>
#else
#include <signal.h>
#include <unistd.h>
#endif


#define WRITE_ARRAY_SIZE 28

typedef struct {
    int slot;
    const char *serial;
    DWORD addr;
} t_open_param;

/* признак необходимости завершить сбор данных */
static int f_out = 0;

static const DWORD wr_buf[] = {0xC0000000,  0x70000000,
                               0x80000000, 0x10000000,
                              0x80000000, 0x10000000,
                              0x80000000, 0x10000000,
                              0x80000000, 0x10000000,
                              0x80000000, 0x10000000,
                              0x80000000, 0x10000000,
                              0x80000000, 0x10000000,
                              0x80000000, 0x10000000,
                              0x80000000, 0x10000000,
                              0x80000000, 0x10000000,
                              0x80000000, 0x10000000,
                              0x80000000, 0x10000000,
                              0x80000000, 0xF0000000};



#ifndef _WIN32
/* Обработчик сигнала завершения для Linux */
static void f_abort_handler(int sig) {
    f_out = 1;
}
#endif

/* Разбор параметров командной строки. Если указано меньше, то используются
 * значения по умолчанию:
 * 1 параметр - номер слота (от 1 до 16)
 * 2 параметр - серийный номер крейта
 * 3 параметр - ip-адрес сервера */
static int f_get_params(int argc, char** argv, t_open_param* par) {
    int err = 0;
    par->slot = LTR_CC_CHNUM_MODULE1;
    par->serial = "";
    par->addr = LTRD_ADDR_DEFAULT;


    if (argc > 1)
        par->slot = atoi(argv[1]);
    if (argc > 2)
        par->serial = argv[2];
    if (argc > 3) {
        int a[4],i;
        if (sscanf(argv[3], "%d.%d.%d.%d", &a[0], &a[1], &a[2], &a[3])!=4) {
            fprintf(stderr, "Неверный формат IP-адреса!!\n");
            err = -1;
        }

        for (i=0; (i < 4) && !err; i++) {
            if ((a[i]<0) || (a[i] > 255)) {
                fprintf(stderr, "Недействительный IP-адрес!!\n");
                err = -1;
            }
        }

        if (!err) {
            par->addr = (a[0] << 24) | (a[1]<<16) | (a[2]<<8) | a[3];
        }
    }
    return err;
}



int main(int argc, char** argv) {
    INT err = LTR_OK;
    t_open_param par;
    TLTR43 hltr43;
#ifndef _WIN32
    struct sigaction sa;
    memset(&sa, 0, sizeof(sa));
    /* В ОС Linux устанавливаем свой обработчик на сигнал закрытия,
       чтобы завершить сбор корректно */
    sa.sa_handler = f_abort_handler;
    sigaction(SIGTERM, &sa, NULL);
    sigaction(SIGINT, &sa, NULL);
    sigaction(SIGABRT, &sa, NULL);
#endif
#ifdef _WIN32
    setlocale(LC_ALL, "");
#endif
    err = f_get_params(argc, argv, &par);
    if (err == LTR_OK) {
        LTR43_Init(&hltr43);
        err = LTR43_Open(&hltr43, par.addr, LTRD_PORT_DEFAULT, par.serial, par.slot);
    }

    if (err != LTR_OK)  {
        fprintf(stderr, "Не удалось установить соединение с модулем. Ошибка %d: %s\n",
                err, LTR43_GetErrorString(err));
    } else {
        printf("Модуль открыт успешно. Информация о модуле:\n  Название %s\n  Серийный %s\n  Версия прошивки %s\n",
               hltr43.ModuleInfo.Name, hltr43.ModuleInfo.Serial, hltr43.ModuleInfo.FirmwareVersion);
        fflush(stdout);

        if (err==LTR_OK) {
            hltr43.IO_Ports.Port1 = LTR43_PORT_DIR_OUT;
            hltr43.IO_Ports.Port2 = LTR43_PORT_DIR_OUT;
            hltr43.IO_Ports.Port3 = LTR43_PORT_DIR_OUT;
            hltr43.IO_Ports.Port4 = LTR43_PORT_DIR_OUT;

            hltr43.Marks.StartMark_Mode = 0;
            hltr43.Marks.SecondMark_Mode = 0;


            hltr43.RS485.FrameSize = 8;
            hltr43.RS485.Baud = 115200;
            hltr43.RS485.Parity = LTR43_RS485_PARITY_NONE;
            hltr43.RS485.StopBit = 0;
            hltr43.RS485.ReceiveTimeoutMultiplier = 0;

            err = LTR43_Config(&hltr43);
            if (err!=LTR_OK) {
                fprintf(stderr, "Не удалось сконфигурировать модуль. Ошибка %d:%s\n",
                        err, LTR43_GetErrorString(err));
            }
        }


        if(err==LTR_OK)  {
            if (err==LTR_OK) {
                printf("Запущен вывод данных. Для останова нажмите %s\n",
    #ifdef _WIN32
                       "любую клавишу"
    #else
                       "CTRL+C"
    #endif
                       );
                fflush(stdout);
            }


            while (!f_out && (err == LTR_OK)) {
                DWORD wr_words[WRITE_ARRAY_SIZE];
                DWORD wrd_idx;
                for (wrd_idx = 0; wrd_idx < WRITE_ARRAY_SIZE; wrd_idx++) {
                    wr_words[wrd_idx] = wr_buf[wrd_idx];
                }

#if 0
                for (wrd_idx = 0; wrd_idx < WRITE_ARRAY_SIZE; wrd_idx++) {
                    err = LTR43_WritePort(&hltr43, wr_words[wrd_idx]);
                }
#endif
                err = LTR43_WriteArray(&hltr43, wr_words, WRITE_ARRAY_SIZE);
                if (err != LTR_OK) {
                    fprintf(stderr, "Не удалось осуществить вывод массива данных. Ошибка %d:%s\n",
                            err, LTR43_GetErrorString(err));
                }

#ifdef _WIN32
                /* проверка нажатия клавиши для выхода */
                if (err==LTR_OK) {
                    if (_kbhit())
                        f_out = 1;
                }
#endif
            }
        }

        if (err == LTR_OK) {
            err = LTR43_Config(&hltr43);
            if (err!=LTR_OK) {
                fprintf(stderr, "Не удалось сконфигурировать модуль. Ошибка %d:%s\n",
                        err, LTR43_GetErrorString(err));
            }
        }


        LTR43_Close(&hltr43);
    }
    return err;
}
