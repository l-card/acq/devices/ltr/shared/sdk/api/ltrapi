cmake_minimum_required(VERSION 2.8.12)

set(PROJECT ltr43_read_write)

project(${PROJECT} C)

set(SOURCES main.c)

include_directories(${LTRAPI_INCLUDE_DIR})
link_directories(${LTRAPI_LIBRARIES_DIR})


add_executable(${PROJECT} ${HEADERS} ${SOURCES})

target_link_libraries(${PROJECT} ltr43api)

install(TARGETS ${PROJECT} DESTINATION ${LTRAPI_INSTALL_EXAMPLES})
