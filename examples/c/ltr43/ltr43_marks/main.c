/*
    Данный пример демонстрирует управление метками с помощью LTR43.

    ltr43_marks  slot crate_serial srvip
    где:
        slot         - номер слота (от 1 до 16)
        crate_serial - серийный номер крейта (если крейтов больше одного)
        srvip        - ip-адрес программы ltrd/LtrServer (если он запущен на другой машине)
    Параметры опциональны: можно указать только слот, слот и серийный номер крейта или все

    Пример запускает генерацию меток СЕКУНДА, после чего ждет нажатия любой кнопки
    (или CTRL+C на Linux), после чего выполняет генерацию метки старт и ждет
    следующего нажатия, после которого останавливает генерацию меток СЕКУНДА
    и завершает работу.

    Проверить наличие меток можно в статистике крейта и/или модуля в программе
    LtrManager/LtrServer или принимая поток данных от какого-либо модуля.

    Сборка в VisualStudio:
    Для того чтобы собрать проект в Visual Studio, измените путь к заголовочным файлам
    (Проект (Project) -> Свойства (Properties) -> Свойства конфигурации (Configuration Properties)
    -> С/С++ -> Общие (General) -> Дополнительные каталоги включения (Additional Include Directories))
    на нужный в зависимаости от пути установки библиотек (ltr43api.h  и остальные заголовочные
    файлы должны находится в поддиректории ltr/include относительно указанного пути)
    и измените путь к .lib файлам на <путь установки библиотек>/lib/msvc
    (Проект (Project) -> Свойства (Properties) -> Свойства конфигурации (Configuration Properties) ->
    Компоновщик (Linker) -> Общие (General) -> Дополнительные каталоги библиотек (Additional Library Directories)).

    Внимание!: Если Вы собираете проект под Visual Studio и у Вас некорректно
    отображается вывод русских букв, то нужно изменить кодировку:
    выберите Файл (File) -> Дополнительные параметры сохранения (Advanced Save Options)...
    и в поле Кодировка (Encoding) выберите Юникод (UTF8, с сигнатурой)/Unicode (UTF-8 with signature)
    и сохраните изменения в файле. А также следует убедится, что в настройках
    консоли стоит шрифт с поддержкой русского языка (например Consolas).
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef _WIN32
#include <locale.h>
#include <conio.h>
#else
#include <signal.h>
#include <unistd.h>
#endif

#include "ltr/include/ltr43api.h"

#ifndef _WIN32
/* признак нажатия CTRL+C */
static int f_out = 0;

/* Обработчик сигнала завершения для Linux */
static void f_abort_handler(int sig) {
    f_out = 1;
}
#endif

typedef struct {
    int slot;
    const char *serial;
    DWORD addr;
} t_open_param;

/* Разбор параметров командной строки. Если указано меньше, то используются
 * значения по умолчанию:
 * 1 параметр - номер слота (от 1 до 16)
 * 2 параметр - серийный номер крейта
 * 3 параметр - ip-адрес сервера */
static int f_get_params(int argc, char** argv, t_open_param* par) {
    int err = 0;
    par->slot = LTR_CC_CHNUM_MODULE1;
    par->serial = "";
    par->addr = LTRD_ADDR_DEFAULT;

    if (argc > 1)
        par->slot = atoi(argv[1]);
    if (argc > 2)
        par->serial = argv[2];
    if (argc > 3) {
        int a[4],i;
        if (sscanf(argv[3], "%d.%d.%d.%d", &a[0], &a[1], &a[2], &a[3])!=4) {
            fprintf(stderr, "Неверный формат IP-адреса!!\n");
            err = -1;
        }

        for (i=0; (i < 4) && !err; i++) {
            if ((a[i]<0) || (a[i] > 255)) {
                fprintf(stderr, "Недействительный IP-адрес!!\n");
                err = -1;
            }
        }

        if (!err) {
            par->addr = (a[0] << 24) | (a[1]<<16) | (a[2]<<8) | a[3];
        }
    }
    return err;
}
/*------------------------------------------------------------------------------------------------*/
int main(int argc, char *argv[]) {
    INT err = LTR_OK;
    TLTR43 hltr43;
    t_open_param par;
#ifndef _WIN32
    struct sigaction sa;
    memset(&sa, 0, sizeof(sa));
    /* В ОС Linux устанавливаем свой обработчик на сигнал закрытия,
       чтобы завершить сбор корректно */
    sa.sa_handler = f_abort_handler;
    sigaction(SIGTERM, &sa, NULL);
    sigaction(SIGINT, &sa, NULL);
    sigaction(SIGABRT, &sa, NULL);
#endif
#ifdef _WIN32
    /* для вывода русских букв в консоль для ОС Windows*/
    setlocale(LC_ALL, "");
#endif

    err = f_get_params(argc, argv, &par);
    if (err == LTR_OK) {
        LTR43_Init(&hltr43);
        /* установление соединения с модулем */
        err = LTR43_Open(&hltr43, par.addr, LTRD_PORT_DEFAULT, par.serial, par.slot);
        if (err != LTR_OK) {
            fprintf(stderr, "Не удалось установить связь с модулем. Ошибка %d (%s)\n",
                    err, LTR43_GetErrorString(err));
        } else {
            /* вывод информации о модуле */
            printf("Модуль открыт успешно:\n");
            printf("  Название:        %s\n", hltr43.ModuleInfo.Name);
            printf("  Серийный номер:  %s\n", hltr43.ModuleInfo.Serial);
            printf("  Версия прошивки: %s\n", hltr43.ModuleInfo.FirmwareVersion);
            printf("  Дата прошивки:   %s\n", hltr43.ModuleInfo.FirmwareDate);


            /* конфигурация модуля  */
            hltr43.Marks.SecondMark_Mode = LTR43_MARK_MODE_INTERNAL;
            hltr43.Marks.StartMark_Mode = LTR43_MARK_MODE_INTERNAL;
            err = LTR43_Config(&hltr43);
            if (err != LTR_OK) {
                fprintf(stderr, "Не удалось сконфигурировать модуль. Ошибка %d (%s)\n",
                        err, LTR43_GetErrorString(err));
            }
        }

        if (err == LTR_OK) {
            err = LTR43_StartSecondMark(&hltr43);
            if (err != LTR_OK) {
                fprintf(stderr, "Не удалось запустить генерацию секундных меток. Ошибка %d (%s)\n",
                        err, LTR43_GetErrorString(err));
            }
        }

        if (err == LTR_OK) {
            INT stop_err;
            printf("Запущена генерация секундных меток.\n Для генерации метки СТАРТ нажмите %s\n",
#ifdef _WIN32
                   "любую клавишу"
#else
                   "CTRL+C"
#endif
                   );
            fflush(stdout);
#ifdef _WIN32
            _getch();
#else
            f_out = 0;
            while (f_out == 0) {
                pause();
            }
#endif
            err = LTR43_MakeStartMark(&hltr43);
            if (err != LTR_OK) {
                fprintf(stderr, "Не удалось сгенерировать метку СТАРТ. Ошибка %d (%s)\n",
                        err, LTR43_GetErrorString(err));
            } else {
                printf("Сгенерирована метка СТАРТ.\n Для останова секундных меток и выхода нажмите %s\n",
#ifdef _WIN32
                       "любую клавишу"
#else
                       "CTRL+C"
#endif
                       );
                fflush(stdout);
#ifdef _WIN32
                _getch();
#else
                f_out = 0;
                while (f_out == 0) {
                    pause();
                }
#endif
            }

            stop_err = LTR43_StopSecondMark(&hltr43);
            if (stop_err != LTR_OK) {
                fprintf(stderr, "Не удалось остановить генерацию секундных меток. Ошибка %d (%s)\n",
                        err, LTR43_GetErrorString(err));
                if (err == LTR_OK)
                    err = stop_err;
            } else {
                printf("Генерация секундных меток остановлена\n");
            }
        }

        if (LTR43_IsOpened(&hltr43) == LTR_OK)
            LTR43_Close(&hltr43);
    }


    return err;
}
