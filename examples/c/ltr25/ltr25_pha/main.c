/*
    Данный пример демонстрирует работу с модулем LTR25.
    По умолчанию идет работа с первым слотом первого крейта, но это можно изменить
        через параметры командной строки:
    ltr25_recv  slot crate_serial srvip
    где:
        slot         - номер слота (от 1 до 16)
        crate_serial - серийный номер крейта (если крейтов больше одного)
        srvip        - ip-адрес программы ltrd или LtrServer (если он запущен на другой машине)
    Параметры опциональны: можно указать только слот, слот и серийный номер крейта или все

    Пример принимает данные от первых трех каналов на максимальной частоте сбора.
    На экране отображается значение пик-пик (максимум - минимум) по принятому блоку
    по каждому каналу.

    Пользователь может изменить настройки на свои, поменяв заполнение полей
    структуры перед запуском сбора.

    Сбор идет до нажатия любой клавиши на Windows или  CTRL+C на Linux

    Сборка в VisualStudio:
    Для того чтобы собрать проект в Visual Studio, измените путь к заголовочным файлам
    (Проект (Project) -> Свойства (Properties) -> Свойства конфигурации (Configuration Properties)
    -> С/С++ -> Общие (General) -> Дополнительные каталоги включения (Additional Include Directories))
    на нужный в зависимаости от пути установки библиотек (ltr25api.h  и остальные заголовочные
    файлы должны находится в поддиректории ltr/include относительно указанного пути)
    и измените путь к .lib файлам на <путь установки библиотек>/lib/msvc
    (Проект (Project) -> Свойства (Properties) -> Свойства конфигурации (Configuration Properties) ->
    Компоновщик (Linker) -> Общие (General) -> Дополнительные каталоги библиотек (Additional Library Directories)).

    Внимание!: Если Вы собираете проект под Visual Studio и у Вас некорректно
    отображается вывод русских букв, то нужно изменить кодировку:
    выберите Файл (File) -> Дополнительные параметры сохранения (Advanced Save Options)...
    и в поле Кодировка (Encoding) выберите Юникод (UTF8, с сигнатурой)/Unicode (UTF-8 with signature)
    и сохраните изменения в файле.
*/

#include "ltr/include/ltr25api.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef _WIN32
#include <locale.h>
#include <conio.h>
#else
#include <signal.h>
#include <unistd.h>
#endif

#include "lmath.h"

/* количество отсчетов на канал, принмаемых за раз */
#define RECV_BLOCK_CH_SIZE  4096*8
/* таймаут на ожидание данных при приеме */
#define RECV_TOUT  4000

#define EN_CHANNELS_CNT 4

static const int en_channels_nums[EN_CHANNELS_CNT] = {3,4,7,1};

typedef struct {
    int slot;
    const char *serial;
    DWORD addr;
} t_open_param;

typedef struct {
    double freq;
    double pow;
    double amp;
    double pha;
} t_ch_reuslt;


/* признак необходимости завершить сбор данных */
static int f_out = 0;

#ifndef _WIN32
/* Обработчик сигнала завершения для Linux */
static void f_abort_handler(int sig) {
    f_out = 1;
}
#endif


static void f_goertzel_amp_pha(const double *sig, int sig_size, double dt, double f, double *amp, double *pha) {
    t_lmath_goertzel_ctx ctx = lmath_goertzel_ctx_create(sig_size, dt, f);
    if (ctx) {
        lmath_goertzel_process(ctx, sig, sig_size);
        lmath_goertzel_result_amp_pha(ctx, LMATH_PHASE_MODE_SIN, amp, pha);
        lmath_goertzel_ctx_destroy(ctx);
    }
}

void process_ch_data(const double *data, int size, double fd, t_ch_reuslt *results) {
    t_lmath_wintype wintype = LMATH_WINTYPE_BH_4TERM;
    double *winsig = malloc(size * sizeof(winsig[0]));
    t_lmath_window_ctx winctx = lmath_window_ctx_create(wintype, 0, size);
    lmath_window_apply_scaled(data, winctx, winsig);
    lmath_find_peak_freq(data, size, 1./fd, -1, wintype, &results->freq, &results->pow);
    f_goertzel_amp_pha(winsig, size, 1./fd, results->freq, &results->amp, &results->pha);

    //f_calc_params_fft(winsig, points_cnt, 1./fd, winctx, ch_result);
    lmath_window_ctx_destroy(winctx);
}

void print_ch_result(t_ch_reuslt *results, const t_ch_reuslt *ref_ch) {
    printf("%f, %f, %f", results->freq, results->amp, lmath_phase_conv_degree(results->pha));
    if (ref_ch) {
        printf(" (%f)", lmath_phase_conv_degree(lmath_phase_normalize(results->pha - ref_ch->pha)));
    }
}



/* Разбор параметров командной строки. Если указано меньше, то используются
 * значения по умолчанию:
 * 1 параметр - номер слота (от 1 до 16)
 * 2 параметр - серийный номер крейта
 * 3 параметр - ip-адрес сервера */
static int f_get_params(int argc, char** argv, t_open_param* par) {
    int err = 0;
    par->slot = LTR_CC_CHNUM_MODULE1;
    par->serial = "";
    par->addr = LTRD_ADDR_DEFAULT;


    if (argc > 1)
        par->slot = atoi(argv[1]);
    if (argc > 2)
        par->serial = argv[2];
    if (argc > 3) {
        int a[4],i;
        if (sscanf(argv[3], "%d.%d.%d.%d", &a[0], &a[1], &a[2], &a[3])!=4) {
            fprintf(stderr, "Неверный формат IP-адреса!!\n");
            err = -1;
        }

        for (i=0; (i < 4) && !err; i++) {
            if ((a[i]<0) || (a[i] > 255)) {
                fprintf(stderr, "Недействительный IP-адрес!!\n");
                err = -1;
            }
        }

        if (!err) {
            par->addr = (a[0] << 24) | (a[1]<<16) | (a[2]<<8) | a[3];
        }
    }
    return err;
}

int main(int argc, char** argv) {
    INT err = LTR_OK;
    TLTR25 hltr25;
    t_open_param par;
#ifndef _WIN32
    struct sigaction sa;
    memset(&sa, 0, sizeof(sa));
    /* В ОС Linux устанавливаем свой обработчик на сигнал закрытия,
       чтобы завершить сбор корректно */
    sa.sa_handler = f_abort_handler;
    sigaction(SIGTERM, &sa, NULL);
    sigaction(SIGINT, &sa, NULL);
    sigaction(SIGABRT, &sa, NULL);
#endif

#ifdef _WIN32
    /* для вывода русских букв в консоль для ОС Windows*/
    setlocale(LC_ALL, "");
#endif

    err = f_get_params(argc, argv, &par);

    if (err==LTR_OK) {
        LTR25_Init(&hltr25);
        /* Устанавливаем соединение с модулем */
        err = LTR25_Open(&hltr25, par.addr, LTRD_PORT_DEFAULT, par.serial, par.slot);
        if (err!=LTR_OK) {
            fprintf(stderr, "Не удалось установить связь с модулем. Ошибка %d (%s)\n",
                    err, LTR25_GetErrorString(err));
        } else {
            INT close_err;
            INT ch_idx;

            /* Выводим прочитанную информацию о модуле */
            printf("Модуль открыт успешно! Информация о модуле: \n");
            printf("  Название модуля    = %s\n", hltr25.ModuleInfo.Name);
            printf("  Серийный номер     = %s\n", hltr25.ModuleInfo.Serial);
            printf("  Версия PLD         = %d\n", hltr25.ModuleInfo.VerPLD);
            printf("  Версия ПЛИС        = %d\n", hltr25.ModuleInfo.VerFPGA);
            printf("  Ревизия платы      = %d\n", hltr25.ModuleInfo.BoardRev);
            printf("  Темп. диапазон     = %s\n", hltr25.ModuleInfo.Industrial ? "Индустриальный" : "Коммерческий");
            fflush(stdout);

            /* Настройка модуля */
            /* Формат - 24 или 20 битный */
            hltr25.Cfg.DataFmt = LTR25_FORMAT_20;
            /* Устанавливаем частоту с помощью одной из констант */
            hltr25.Cfg.FreqCode = LTR25_FREQ_39K;
            /* Источник тока 2.86 или 10 мА */
            hltr25.Cfg.ISrcValue = LTR25_I_SRC_VALUE_2_86;

            /* Настройка режимов каналов */
            for (ch_idx = 0; ch_idx < EN_CHANNELS_CNT; ch_idx++) {
                int ch_num = en_channels_nums[ch_idx];
                hltr25.Cfg.Ch[ch_num].Enabled = TRUE;
            }

            err = LTR25_SetADC(&hltr25);
            if (err!=LTR_OK) {
                fprintf(stderr, "Не удалось установить настройки АЦП: Ошибка %d (%s)\n",
                        err, LTR25_GetErrorString(err));
            } else {
                /* после SetADC() обновляется поле AdcFreq. Становится равной действительной
                 * установленной частоте */
                printf("Настройки АЦП установленны успешно. Частота = %.2f Гц, Кол-во каналов = %d\n",
                        hltr25.State.AdcFreq, hltr25.State.EnabledChCnt);
            }

            if (err==LTR_OK) {
                DWORD recvd_blocks=0;
                INT recv_data_cnt = RECV_BLOCK_CH_SIZE*hltr25.State.EnabledChCnt;
                /* В 24-битном формате каждому отсчету соответствует два слова от модуля,
                   а в 20-битном - одно */
                INT   recv_wrd_cnt = recv_data_cnt*(hltr25.Cfg.DataFmt==LTR25_FORMAT_20 ? 1 : 2);
                DWORD  *rbuf = (DWORD*)malloc(recv_wrd_cnt*sizeof(rbuf[0]));
                double *data = (double *)malloc(recv_data_cnt*sizeof(data[0]));
                double *ch_data = (double *)malloc(RECV_BLOCK_CH_SIZE * sizeof(ch_data[0]));
                DWORD  *ch_status = (DWORD*)malloc(hltr25.State.EnabledChCnt*sizeof(ch_status[0]));

                if ((rbuf==NULL) || (data==NULL) || (ch_status==NULL) || (ch_data == NULL)) {
                    fprintf(stderr, "Ошибка выделения памяти!\n");
                    err = LTR_ERROR_MEMORY_ALLOC;
                }

                if (err==LTR_OK) {
                    /* Запуск сбора данных */
                    err=LTR25_Start(&hltr25);
                    if (err!=LTR_OK) {
                        fprintf(stderr, "Не удалось запустить сбор данных! Ошибка %d (%s)\n",
                                err, LTR25_GetErrorString(err));
                    }
                }

                if (err==LTR_OK) {
                    printf("Сбор данных запущен. Для останова нажмите %s\n",
#ifdef _WIN32
                           "любую клавишу"
#else
                           "CTRL+C"
#endif
                           );
                    fflush(stdout);
                }


                /* ведем сбор данных до возникновения ошибки или до
                 * запроса на завершение */
                while (!f_out && (err==LTR_OK)) {
                    INT recvd;
                    /* В таймауте учитываем время сбора запрашиваемого числа отсчетов */
                    DWORD tout = RECV_TOUT + (DWORD)(1000.*RECV_BLOCK_CH_SIZE/hltr25.State.AdcFreq + 1);

                    /* Прием данных от модуля.  */
                    recvd = LTR25_Recv(&hltr25, rbuf, NULL, recv_wrd_cnt, tout);

                    /* Значение меньше нуля соответствуют коду ошибки */
                    if (recvd<0) {
                        err = recvd;
                        fprintf(stderr, "Ошибка приема данных. Ошибка %d:%s\n",
                                err, LTR25_GetErrorString(err));
                    } else if (recvd!=recv_wrd_cnt) {
                        fprintf(stderr, "Принято недостаточно данных. Запрашивали %d, приняли %d\n",
                                recv_wrd_cnt, recvd);
                        err = LTR_ERROR_RECV_INSUFFICIENT_DATA;
                    } else {
                        err = LTR25_ProcessData(&hltr25, rbuf, data, &recvd,
                                                LTR25_PROC_FLAG_VOLT | LTR25_PROC_FLAG_SIGN_COR, ch_status);
                        if (err!=LTR_OK) {
                            fprintf(stderr, "Ошибка обработки данных. Ошибка %d:%s\n",
                                    err, LTR25_GetErrorString(err));
                        } else {
                            t_ch_reuslt ch_results[EN_CHANNELS_CNT];

                            recvd_blocks++;
                            /* выводим по первому слову на канал */
                            printf("Блок %4d:\n", recvd_blocks);


                            for (ch_idx=0; ch_idx < hltr25.State.EnabledChCnt; ch_idx++) {
                                printf("  Канал %d: ", ch_idx);
                                if (ch_status[ch_idx]==LTR25_CH_STATUS_OPEN) {
                                    printf("%10s", "обрыв");
                                } else if (ch_status[ch_idx]==LTR25_CH_STATUS_SHORT) {
                                    printf("%10s", "кз");
                                } else if (ch_status[ch_idx]==LTR25_CH_STATUS_OK) {
                                    INT i;
                                    DWORD ch_data_size = (DWORD)recvd / hltr25.State.EnabledChCnt;

                                    for (i=1; i <  recvd / hltr25.State.EnabledChCnt; i++) {
                                        double val = data[i*hltr25.State.EnabledChCnt+ch_idx];
                                        ch_data[i] = val;
                                    }
                                    process_ch_data(ch_data, (int)ch_data_size, hltr25.State.AdcFreq,
                                                    &ch_results[ch_idx]);
                                    print_ch_result(&ch_results[ch_idx], ch_idx == 0 ? NULL : &ch_results[0]);
                                } else {
                                    printf("незвест. сост.!");
                                }

                                printf("\n");
                            }
                            printf("\n");
                            fflush(stdout);

                        }
                    }

#ifdef _WIN32
                    /* проверка нажатия клавиши для выхода */
                    if (err==LTR_OK) {
                        if (_kbhit())
                            f_out = 1;
                    }
#endif
                } //while (!f_out && (err==LTR_OK))




                /* по завершению останавливаем сбор, если был запущен */
                if (hltr25.State.Run) {
                    INT stop_err = LTR25_Stop(&hltr25);
                    if (stop_err!=LTR_OK) {
                        fprintf(stderr, "Не удалось остановить сбор данных. Ошибка %d:%s\n",
                                stop_err, LTR25_GetErrorString(stop_err));
                        if (err==LTR_OK)
                            err = stop_err;
                    } else {
                        printf("Сбор остановлен успешно.\n");
                    }
                }


                free(rbuf);
                free(data);
                free(ch_status);
            }

            /* закрываем связь с модулем */
            close_err = LTR25_Close(&hltr25);
            if (close_err!=LTR_OK) {
                fprintf(stderr, "Не удалось закрыть связь с модулем. Ошибка %d:%s\n",
                        close_err, LTR25_GetErrorString(close_err));
                if (err==LTR_OK)
                    err = close_err;
            } else {
                printf("Связь с модулем успешно закрыта.\n");
            }
        }
    }

    return err;
}
