cmake_minimum_required(VERSION 2.8.12)

add_subdirectory(ltr25_recv)

if(LTRAPI_USE_KD_STORESLOTS)
    add_subdirectory(ltr25_ex)
endif(LTRAPI_USE_KD_STORESLOTS)

add_subdirectory(ltr25_teds)
if(WIN32)
    add_subdirectory(ltr25_rcv_not_full_test)
endif(WIN32)
if(LTRAPI_BUILD_MATH_EXAMPLES)
    add_subdirectory(ltr25_pha)
endif(LTRAPI_BUILD_MATH_EXAMPLES)
