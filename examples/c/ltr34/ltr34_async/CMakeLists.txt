cmake_minimum_required(VERSION 2.8.12)

set(PROJECT ltr34_async)

project(${PROJECT} C)

set(SOURCES main.c)

include_directories(${LTRAPI_INCLUDE_DIR})
link_directories(${LTRAPI_LIBRARIES_DIR})

set(LTIMER_DIR       ${LTRAPI_LIB_DIR}/ltimer)
set(LCSPEC_DIR       ${LTRAPI_LIB_DIR}/lcspec)
include(${LTIMER_DIR}/ltimer.cmake)
set(SOURCES ${SOURCES} ${LTIMER_SOURCES} ${LTIMER_HEADERS})


if(MSVC)
    if( CMAKE_SIZEOF_VOID_P EQUAL 4 )
        SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} /SUBSYSTEM:WINDOWS,5.01")
    endif( CMAKE_SIZEOF_VOID_P EQUAL 4 )
    foreach(flag_var
            CMAKE_C_FLAGS CMAKE_C_FLAGS_DEBUG CMAKE_C_FLAGS_RELEASE
            CMAKE_C_FLAGS_MINSIZEREL CMAKE_C_FLAGS_RELWITHDEBINFO)
       if(${flag_var} MATCHES "/MD")
          string(REGEX REPLACE "/MD" "/MT" ${flag_var} "${${flag_var}}")
       endif(${flag_var} MATCHES "/MD")
    endforeach(flag_var)
endif(MSVC)
add_executable(${PROJECT} ${HEADERS} ${SOURCES})

target_link_libraries(${PROJECT} ltr34api)
target_link_libraries(${PROJECT} ${LTIMER_LIBS})
install(TARGETS ${PROJECT} DESTINATION ${LTRAPI_INSTALL_EXAMPLES})

