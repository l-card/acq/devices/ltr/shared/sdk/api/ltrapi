cmake_minimum_required(VERSION 2.8.12)

if(LTRAPI_BUILD_MATH_EXAMPLES)
    add_subdirectory(lpw25_ltr25_test)
    add_subdirectory(lpw25_ltr24_test)    
endif(LTRAPI_BUILD_MATH_EXAMPLES)

add_subdirectory(lpw25_teds_write)
