#include "ltr/include/ltravrapi.h"
/* остальные заголовочные файлы */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>



typedef struct {
    int slot;
    const char *serial;
    DWORD addr;
} t_open_param;

/* Разбор параметров командной строки. Если указано меньше, то используются
 * значения по умолчанию:
 * 1 параметр - номер слота (от 1 до 16)
 * 2 параметр - серийный номер крейта
 * 3 параметр - ip-адрес сервера */
static int f_get_params(int argc, char** argv, t_open_param* par) {
    int err = 0;
    par->slot = LTR_CC_CHNUM_MODULE1;
    par->serial = "";
    par->addr = LTRD_ADDR_DEFAULT;


    if (argc > 1)
        par->slot = atoi(argv[1]);
    if (argc > 2)
        par->serial = argv[2];
    if (argc > 3) {
        int a[4],i;
        if (sscanf(argv[3], "%d.%d.%d.%d", &a[0], &a[1], &a[2], &a[3])!=4) {
            fprintf(stderr, "Неверный формат IP-адреса!!\n");
            err = -1;
        }

        for (i=0; (i < 4) && !err; i++) {
            if ((a[i]<0) || (a[i] > 255)) {
                fprintf(stderr, "Недействительный IP-адрес!!\n");
                err = -1;
            }
        }

        if (!err) {
            par->addr = (a[0] << 24) | (a[1]<<16) | (a[2]<<8) | a[3];
        }
    }
    return err;
}



/*-----------------------------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
    TLTRAVR hltr11;
    INT  err;
    t_open_param par;

    err = f_get_params(argc, argv, &par);

    if (err == LTR_OK) {
        /* инициализация дескриптора модуля */
        LTRAVR_Init(&hltr11);
        /* открытие канала связи с модулем, установленным в заданный слот */
        err = LTRAVR_Open(&hltr11, par.addr, LTRD_PORT_DEFAULT, par.serial, par.slot);
    }
    if (err != LTR_OK) {
        fprintf(stderr, "Не удалось установить связь с модулем. Ошибка %d (%s)\n",
                err, LTRAVR_GetErrorString(err));
    } else {
        /* получение информации о модуле из flash-памяти */
        err = LTRAVR_SetSpeedFlag(&hltr11, TRUE);
        if (err != LTR_OK) {
            fprintf(stderr, "Не удалось перевести крейт в режим низкой скорости обмена. Ошибка %d: %s\n", err,
                    LTRAVR_GetErrorString(err));
        }




        if (err == LTR_OK) {
            WORD fuses = 0xDFC0;
            err = LTRAVR_WriteFuseBits(&hltr11, (BYTE*)&fuses);
            if (err != LTR_OK) {
                fprintf(stderr, "Не удалось выполнить запись FUSE. Ошибка %d: %s\n", err,
                        LTRAVR_GetErrorString(err));
            }
        }

        err = LTRAVR_SetSpeedFlag(&hltr11, FALSE);
    }

    LTRAVR_Close(&hltr11);

    return err;
}


