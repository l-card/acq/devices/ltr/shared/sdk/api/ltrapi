cmake_minimum_required(VERSION 2.8.12)

add_subdirectory(ltr42_loop_test)
add_subdirectory(ltr42_get_descr_test)
if(LTRAPI_USE_KD_STORESLOTS)
    add_subdirectory(ltr42_ex)
endif(LTRAPI_USE_KD_STORESLOTS)
