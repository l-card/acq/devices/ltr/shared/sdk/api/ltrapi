#include "ltr/include/ltedsapi.h"
#include <stdio.h>
#include <stdlib.h>

//http://www.futek.com/files/pdf/Product%20Drawings/FSH03189.pdf
static const BYTE teds_data_fsh[] = {
    0x97, 0x37, 0xC0, 0xAA, 0x0E, 0x19, 0x40, 0xE2, 0x01, 0x84, 0x3C, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x20, 0x41, 0x02, 0x00, 0x00, 0x00, 0xBC, 0x49, 0x0C, 0xEC, 0x28, 0xDA, 0xC0, 0x36, 0x66,
    0x7E, 0xCC, 0x98, 0x2E, 0x0A, 0x31, 0x56, 0x5B, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};


static INT lteds_read_file_raw(TLTEDS_DECODE_CONTEXT *ctx, const char *filename, int skip) {
    INT err = LTEDS_OK;
    FILE *f = fopen(filename, "rb");
    if (f) {
        int fsize;
        BYTE *data;

        fseek(f, 0, SEEK_END);
        fsize = ftell(f);
        fseek(f, 0, SEEK_SET);
        data = malloc(fsize);
        if (data == NULL) {
            fprintf(stderr, "memory alloc error!\n");
            err = -1;
        } else if ((int)fread(data, 1, fsize, f) != fsize) {
            fprintf(stderr, "read file error!\n");
            err = -2;
        } else if (fsize > skip) {
            err = LTEDS_DecodeInit(ctx, &data[skip], fsize - skip, 0);
        }
        free(data);
        fclose(f);
    } else {
       fprintf(stderr, "open file error!\n");
       err = -3;
    }

    return err;
}


/*-----------------------------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
    INT err = LTEDS_OK;

    TLTEDS_DECODE_CONTEXT dec;

    //err = LTEDS_DecodeInit(&dec, teds_data_fsh, sizeof(teds_data_fsh), LTEDS_DATA_FLAG_HAS_CHECKSUM);
    err = lteds_read_file_raw(&dec, "/home/user/PRJ/LTR/ltr25/teds/tedsdata.bin", 32);
    if (err == LTEDS_OK) {
        TLTEDS_INFO_BASIC basic;

        err = LTEDS_GetBasicInfo(&dec, &basic);
        if (err == LTEDS_OK) {
            BOOLEAN end = FALSE;
            printf("TEDS info:\n  Manufacturer ID = %d\n  Model Number = %d\n  Version Letter = %c\n  Version Number = %d\n  Serial Number = %d\n",
                   (int)basic.ManufacturerID, (int)basic.ModelNumber, (char)basic.VersionLetter,
                   (int)basic.VersionNumber, (int)basic.SerialNumber);
            while (!end && (err == LTEDS_OK)) {
                BYTE selector;
                err = LTEDS_GetSelector(&dec, &selector);
                if (err == LTEDS_OK) {
                    if (selector == LTEDS_SEL_STANDARD) {
                        BYTE template_id;
                        err = LTEDS_GetStdTemplateID(&dec, &template_id);
                        if (err == LTEDS_OK) {
                            if (template_id == LTEDS_STD_TEMPLATE_ID_BRIDGE_SENSOR) {
                                TLTEDS_INFO_TMPLT_BRIDGE bridge_tmpl;
                                err = LTEDS_GetTmpltInfoBridge(&dec, &bridge_tmpl);
                                if (err == LTEDS_OK) {
                                    printf("Bridget template\n");
                                    printf("  Physical units: [%f-%f] %d\n", bridge_tmpl.Meas.MinValue,
                                                bridge_tmpl.Meas.MaxValue, bridge_tmpl.Meas.Measurand);
                                    printf("  Electrical units [%f-%f] %d\n", bridge_tmpl.ElecSigOut.Range.MinValue,
                                           bridge_tmpl.ElecSigOut.Range.MaxValue, bridge_tmpl.ElecSigOut.Range.PrecType);

                                    printf("  Bridge type %d, impedance %f, response time %f\n",
                                           bridge_tmpl.ElecSigOut.BridgeType,
                                           bridge_tmpl.ElecSigOut.BridgeImpedance,
                                           bridge_tmpl.ElecSigOut.ResponseTime);
                                    printf("  Excitation: Nominal %f, Min %f, Max %f\n",
                                           bridge_tmpl.ExcitationLevels.NominalValue,
                                           bridge_tmpl.ExcitationLevels.MinValue,
                                           bridge_tmpl.ExcitationLevels.MaxValue);

                                    printf("  Calibration: data %02d.%02d.%04d, initials %s, period %d days\n",
                                           bridge_tmpl.CalInfo.Date.Day,
                                           bridge_tmpl.CalInfo.Date.Month,
                                           bridge_tmpl.CalInfo.Date.Year,
                                           bridge_tmpl.CalInfo.Initials,
                                           bridge_tmpl.CalInfo.PeriodDays);
                                    printf("  Location ID: %d\n", bridge_tmpl.MeasLocationID);
                                }
                            } else if (template_id == LTEDS_STD_TEMPLATE_ID_ACCFORCE) {
                                TLTEDS_INFO_TMPLT_ACCFORCE acc_tmpl;
                                err = LTEDS_GetTmpltInfoAccForce(&dec, &acc_tmpl);
                                if (err == LTEDS_OK) {
                                    printf("Accelerometer-force template\n");

                                }
                            }
                        }
                    } else if (selector == LTEDS_SEL_MANUFACTURER) {
                        printf("Найден специфичный блок данных производителя с id = %d. Нет данных как интерпретировать!\n",
                               basic.ManufacturerID);
                        err = LTEDS_ERROR_UNSUPPORTED_FORMAT;
                    } else if (selector == LTEDS_SEL_OTHER_MANUFACTURER) {
                        WORD TemplateManufacturerID;
                        err = LTEDS_GetManufacturerID(&dec, &TemplateManufacturerID);
                        if (err == LTEDS_OK) {
                            printf("Найден специфичный блок данных производителя с id = %d. Нет данных как интерпретировать!\n",
                                   TemplateManufacturerID);
                        }
                        err = LTEDS_ERROR_UNSUPPORTED_FORMAT;
                    } else if (selector == LTEDS_SEL_END) {
                        BYTE extsel;
                        err = LTEDS_GetEndExtendedSelector(&dec, &extsel);
                        if (err == LTEDS_OK) {
                            printf("Конец информации описываемой шаблонами. Оставшиеся данные представлены ");
                            if (extsel == LTEDS_EXTSEL_FREE_FORM) {
                                printf("в свободной форме\n");
                            } else if (extsel == LTEDS_EXTSEL_ASCII) {
                                printf("ASCII строкой\n");
                            }
                            end = 1;
                        }
                    }
                }
            }
        }
    }


    return err;
}



