#include "ltr/include/ltedsapi.h"
#include <stdio.h>


static BYTE teds_data[8*1024];


/*-----------------------------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
    INT err = LTEDS_OK;
    const DWORD test_word = 0x3a55;
    const double test_val = 78.567;
    const char *test_str  = "Q@_ .A";
    const char test_str_sym_cnt = 6;
    const char *test_str2 = "Q@";
    const char test_str2_sym_cnt = 6;

    TLTEDS_DECODE_CONTEXT dec;
    TLTEDS_ENCODE_CONTEXT enc;


    err = LTEDS_EncodeInit(&enc, teds_data, sizeof(teds_data), 0);
    if (err == LTEDS_OK) {

        err = LTEDS_PutUnInt(&enc, 14, test_word);
        if (err == LTEDS_OK) {
            err = LTEDS_PutSingle(&enc, test_val);
        }
        if (err == LTEDS_OK) {
            err = LTEDS_PutConRes(&enc, 20, 78, 0.001, test_val);
        }
        if (err == LTEDS_OK) {
            err = LTEDS_PutConRelRes(&enc, 20, 70, 0.001, test_val);
        }

        if (err == LTEDS_OK) {
            TLTEDS_INFO_DATE date;
            date.Year = 2018;
            date.Month = 12;
            date.Day = 31;
            err = LTEDS_PutDate(&enc, 16, &date);
        }
        if (err == LTEDS_OK) {
            err = LTEDS_PutChr5(&enc, test_str_sym_cnt*5, test_str);
        }
        if (err == LTEDS_OK) {
            err = LTEDS_PutChr5(&enc, test_str2_sym_cnt*5, test_str2);
        }
        if (err == LTEDS_OK) {
            err = LTEDS_PutASCII(&enc, test_str_sym_cnt*7, test_str);
        }
        if (err == LTEDS_OK) {
            err = LTEDS_PutASCII(&enc, test_str2_sym_cnt*7, test_str2);
        }

    }

    if (err == LTEDS_OK) {
        err = LTEDS_DecodeInit(&dec, teds_data, sizeof(teds_data), 0);
        if (err == LTEDS_OK) {
            DWORD word;
            err = LTEDS_GetUnInt(&dec, 14, &word);
            if (err == LTEDS_OK)
                printf("get unint = 0x%08X\n", word);
        }

        if (err == LTEDS_OK) {
            double val;
            err = LTEDS_GetSingle(&dec, &val);
            if (err == LTEDS_OK)
                printf("get single = %f\n", val);
        }

        if (err == LTEDS_OK) {
            double val;
            err = LTEDS_GetConRes(&dec, 20, 78, 0.001, &val);
            if (err == LTEDS_OK)
                printf("get conres = %f\n", val);
        }
        if (err == LTEDS_OK) {
            double val;
            err = LTEDS_GetConRelRes(&dec, 20,  70, 0.001, &val);
            if (err == LTEDS_OK)
                printf("get conrelres = %f\n", val);
        }

        if (err == LTEDS_OK) {
            TLTEDS_INFO_DATE date;
            err = LTEDS_GetDate(&dec, 16, &date);
            if (err == LTEDS_OK)
                printf("date = %d.%d.%d\n", date.Day, date.Month, date.Year);
        }
        if (err == LTEDS_OK) {
            char str[7];
            err = LTEDS_GetChr5(&dec, test_str_sym_cnt*5, str);
            if (err == LTEDS_OK)
                printf("chr5  = '%s'\n", str);
        }
        if (err == LTEDS_OK) {
            char str[7];
            err = LTEDS_GetChr5(&dec, test_str2_sym_cnt*5, str);
            if (err == LTEDS_OK)
                printf("chr5  = '%s'\n", str);
        }
        if (err == LTEDS_OK) {
            char str[7];
            err = LTEDS_GetASCII(&dec, test_str_sym_cnt*7, str);
            if (err == LTEDS_OK)
                printf("ascii = '%s'\n", str);
        }
        if (err == LTEDS_OK) {
            char str[7];
            err = LTEDS_GetASCII(&dec, test_str2_sym_cnt*7, str);
            if (err == LTEDS_OK)
                printf("ascii = '%s'\n", str);
        }
    }

    return err;
}



