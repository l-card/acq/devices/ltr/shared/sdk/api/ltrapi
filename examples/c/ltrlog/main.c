#include "ltr/include/ltrlogapi.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#ifdef _WIN32
#include <locale.h>
#include <conio.h>
#else
#include <signal.h>
#include <unistd.h>
#endif



/* признак необходимости завершить сбор данных */
static int f_out = 0;

#ifndef _WIN32
/* Обработчик сигнала завершения для Linux */
static void f_abort_handler(int sig)
{
    f_out = 1;
}
#endif


typedef struct {
    DWORD addr;
} t_open_param;

static int f_get_params(int argc, char** argv, t_open_param* par)
{
    int err = 0;
    par->addr = LTRD_ADDR_DEFAULT;

    if (argc > 1)
    {
        int a[4],i;
        if (sscanf(argv[1], "%d.%d.%d.%d", &a[0], &a[1], &a[2], &a[3])!=4)
        {
            fprintf(stderr, "Invalid ip address format!!\n");
            err = -1;
        }

        for (i=0; (i < 4) && !err; i++)
        {
            if ((a[i]<0) || (a[i] > 255))
            {
                fprintf(stderr, "invalid ip address!!\n");
                err = -1;
            }
        }

        if (!err)
        {
            par->addr = (a[0] << 24) | (a[1]<<16) | (a[2]<<8) | a[3];
        }
    }
    return err;
}



/*-----------------------------------------------------------------------------------------------*/
int main(int argc, char* argv[])
{
    TLTRLOG hlog;
    INT  err;
    t_open_param par;
#ifndef _WIN32
    struct sigaction sa;
    memset(&sa, 0, sizeof(sa));
    /* В ОС Linux устанавливаем свой обработчик на сигнал закрытия,
       чтобы завершить сбор корректно */
    sa.sa_handler = f_abort_handler;
    sigaction(SIGTERM, &sa, NULL);
    sigaction(SIGINT, &sa, NULL);
    sigaction(SIGABRT, &sa, NULL);
#endif

#ifdef _WIN32
    /* для вывода русских букв в консоль для ОС Windows*/
    setlocale(LC_ALL, "");
#endif

    err = f_get_params(argc, argv, &par);
    if (!err) {
        LTRLOG_Init(&hlog);
        err = LTRLOG_Open(&hlog, par.addr, LTRD_PORT_DEFAULT);
        if (err!=LTR_OK) {
            fprintf(stderr, "Не удалось установить связь с сервером. Ошибка %d: %s\n",
                    err, LTR_GetErrorString(err));
        } else {
            printf("Связь с сервером установлена. Для завершения останова нажмите %s\n",
#ifdef _WIN32
                          "любую клавишу"
#else
                          "CTRL+C"
#endif
                   );
            fflush(stdout);

            while (!f_out && (err==LTR_OK)) {
                TLTRLOG_MSG *msg;
                err = LTRLOG_GetNextMsg(&hlog, &msg, 1000);
                if (err==LTR_OK) {
                    if (msg!=NULL) {
                        if (msg->Sign==LTRLOG_MSG_SIGN) {
                            struct tm* loc_time;
                            time_t time;

                            time = (time_t)(msg->Time.QuadPart);
                            loc_time = localtime(&time);
                            printf("%02d:%02d:%02d, %02d.%02d.%04d. ",
                                   loc_time->tm_hour, loc_time->tm_min, loc_time->tm_sec,
                                   loc_time->tm_mday, loc_time->tm_mon+1, loc_time->tm_year+1900);

                            printf("Сообщение: уровень = %d, код = %d, текст = %s\n",
                                    msg->Lvl, msg->Err, msg->Msg);
                            LTRLOG_FreeMsg(msg);
                            fflush(stdout);
                        }
                    }
                } else {
                    fprintf(stderr, "Ошибка при приеме сообщения. Ошибка %d: %s\n",
                            err, LTR_GetErrorString(err));
                }


#ifdef _WIN32
                /* проверка нажатия клавиши для выхода */
                if (err==LTR_OK) {
                    if (_kbhit())
                        f_out = 1;
                }
#endif
            }

            LTRLOG_Close(&hlog);
            printf("Связь с сервером разорвана\n");
        }


    }

    return err;

}



