/* Пример, демострирующий, как установить режим генерации синхрометок для
   LTR-U-1-4 */

#include "ltr/include/ltr021api.h"
#include <stdio.h>

#ifdef _WIN32
    #include <locale.h>
#endif

int main(int argc, char** argv)
{
   INT err;
   TLTR021 hltr021;
   const char* csn="";

#ifdef _WIN32
    setlocale(LC_ALL, "");
#endif

   /* если передан параметр, то считаем, что это строка с серийным номером
    * крейта */
   if (argc > 1)
       csn = argv[1];

   LTR021_Init(&hltr021);
   err = LTR021_Open(&hltr021, LTRD_ADDR_DEFAULT, LTRD_PORT_DEFAULT, csn);
   if (err!=LTR_OK)
   {
       fprintf(stderr, "Невозможно установить связь с крейтом! Ошибка %d: %s\n",
              err, LTR021_GetErrorString(err));
   }
   else
   {
       printf("Соединение с крейтом успешно установлено!\n");

       /* устанавливаем режим генерации синхрометок. В данном случае
        * по спаду сигнала на входе SYNC генерируется метка СЕКУНДА */
       err = LTR021_SetCrateSyncType(&hltr021, LTR021_Falling_Sync);
       if (err!=LTR_OK)
       {
           fprintf(stderr, "Невозможно установить режим генерации меток! Ошибка %d: %s\n",
                  err, LTR021_GetErrorString(err));
       }
       else
       {
           printf("Режим генерации меток установлен успешно!\n");
       }

       LTR021_Close(&hltr021);
   }


   return err;
}
