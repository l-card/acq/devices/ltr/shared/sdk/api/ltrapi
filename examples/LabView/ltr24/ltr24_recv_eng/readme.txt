This example demonstrates how to work with LTR24 module from LabView.

In this example we use .Net assembly ltrModulesNet.dll to get access to module functions from LabView  
(this assemply is part of ltr libraries http://www.lcard.ru/download/ltrdll.exe),
therefore it is necessary to have Microsoft Net Framework 2.0 installed. (preinstalled in Windows started from Vista).
Also it is necessary to have installed ltrlibraries (http://www.lcard.ru/download/ltrdll.exe) in system and installed 
ltrd service (http://www.lcard.ru/download/ltrd-setup.exe).

This example can be used in all LabView versions started from 8.0. Use example from subdirectory 10.0 for LabView 10.0 
or higher and from subdirectory 8.0 for previous LabView version.