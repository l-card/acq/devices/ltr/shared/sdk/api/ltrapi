This example demonstrates how to calculate and apply user coefficient to convert Volt values from LTR24 module
to user defined physical units with taking to account full channel (include sensors) conversion coefficients.

Calculation of this coefficient is named as tare, these user coefficient are named as tare coefficient and
converted to user unit values are named tared values in this example.

Example can calculate only coefficient for linear conversion. It uses 2 coefficient for each channel: scale and offset 
(like calibration coefficient in module), or only scale for AC mode (see below). 
User tared value (Y) is calculated by the input Volt value (X) as 
Y = (X + offset) * scale.

Example can calculate tare coefficient based on DC (constant component of signal) or AC (RMS of variable component of signal)
value. Example uses LabView "AC & DC Estimator VI" (http://zone.ni.com/reference/en-XX/help/371361P-01/lvanls/ac_and_dc_estimator/)
This VI must be included in your labview installed version or user can 
implement his own function to calculate single result based on sampled points for block time.
For this purpose created separated vi called ltr24_point_calc.vi.

Example always calculates and show on plots both original values in Volts and converted values after apply user tare coefficients.

User can enter tare coefficient manually for each channel in "Tare Coefficients" array or can use automatic coefficient calculation
based on two tare points with two known reference input impact or based on single point if offset has no matter typically for AC).
In the latter case user can leave zero value for input an result second tare point.

For automatic coefficient calculation you must:
1. Set first reference signal with known value on system input.
2. Enter known signal value in user units in "Tare Input Value 1" control field
3. When LTR24 Measured values are stable press "Save Tare Point 1 Meas Results". 
   Current volt measured values would be saved in indicators on the left hand of this button.
4. For single point tare coefficient calculation (typically for AC)  leave in "Tare Input Value 2" and
    point 2 results zero values and go to pt.8. For two point calculation go to pt.5. 
5. Set second (maximum) reference signal with known value on system input.
6. Enter known signal value in user units in "Tare Input Value 2" control field
7. When LTR24 Measured values are stable press "Save Tare Point 2 Meas Results". 
   Current volt measured values would be saved in indicators on the left hand of this button.
8. Press "Calc Tare Coefficients" button to calculate tare coefficients based on completed measurements.
   Example calculate for each channel linear conversion coefficient in such a way that measured 
   Volts values (X) wold correspond to entered by user Tare Input Values (Y) in two points as:
   Scale = (Y2 - Y1)/(X2 - X1)
   Offset = Y1/Scale - X1

   Values in "Tare Coefficients" control panel would be changed to calculated scale and offset values.
8. New tare values would be applied automatically after change on next LTR24 block processing.
    

This example doesn't save calculated coefficient to some persistent storage. User must implement 
this function if required.


In this example we use .Net assembly ltrModulesNet.dll to get access to module functions from LabView  
(this assemply is part of ltr libraries http://www.lcard.ru/download/ltrdll.exe),
therefore it is necessary to have Microsoft Net Framework 2.0 installed. (preinstalled in Windows started from Vista).
Also it is necessary to have installed ltrlibraries (http://www.lcard.ru/download/ltrdll.exe) in system and installed 
ltrd service (http://www.lcard.ru/download/ltrd-setup.exe).

This example can be used in all LabView versions started from 8.0. Use example from subdirectory 10.0 for LabView 10.0 
or higher and from subdirectory 8.0 for previous LabView version.
