#ifndef LTR_4X_CFG_H_
#define LTR_4X_CFG_H_

#include "ltr42api.h"

#define MODULE_ID               LTR_MID_LTR42
#define MODULE_DEFAULT_NAME     "LTR42"

typedef TLTR42_MODULE_INFO TLTR4X_MODULE_INFO;

// Коды команд AVR
#define CONFIG                                        (LTR010CMD_INSTR | 0x01)
#define START_SECOND_MARK                             (LTR010CMD_INSTR | 0x02)
#define STOP_SECOND_MARK                              (LTR010CMD_INSTR | 0x03)
#define MAKE_START_MARK                               (LTR010CMD_INSTR | 0x04)

#define WRITE_EEPROM                                  (LTR010CMD_INSTR | 0x05)
#define READ_EEPROM                                   (LTR010CMD_INSTR | 0x06)
#define READ_CONF_RECORD                              (LTR010CMD_INSTR | 0x07)
#define CMD_CONFIG_PAR_EX                             (LTR010CMD_INSTR | 0x13)

#define INIT                                          (LTR010CMD_INSTR | 0x0F)

#define DATA_OUTPUT_CONFIRM                           (LTR010CMD_INSTR | 0x1D)
#define PARITY_ERROR                                  (LTR010CMD_INSTR | 0x1E)
#define DATA_ERROR                                    (LTR010CMD_INSTR | 0x1F)


/* Коды ошибок */
#define LTR4X_ERR_DATA_TRANSMISSON_ERROR  LTR42_ERR_DATA_TRANSMISSON_ERROR


#define F_OSC  (15000000) // Тактовая частота микроконтроллера AVR

#define LTR4X_INIT_TIMEOUT      200 /* таймаут на ожидание после STOP-RESET-STOP */

#define LTR4X_EEPROM_SIZE       LTR42_EEPROM_SIZE

#endif
