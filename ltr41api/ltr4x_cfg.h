#ifndef LTR_4X_CFG_H_
#define LTR_4X_CFG_H_

#include "ltr41api.h"

#define MODULE_ID               LTR_MID_LTR41
#define MODULE_DEFAULT_NAME     "LTR41"

typedef TLTR41_MODULE_INFO TLTR4X_MODULE_INFO;

// Коды команд AVR
#define READ_WORD                                     (LTR010CMD_INSTR | 0x01)
#define CONFIG                                        (LTR010CMD_INSTR | 0x02)
#define START_SECOND_MARK                             (LTR010CMD_INSTR | 0x03)
#define STOP_SECOND_MARK                              (LTR010CMD_INSTR | 0x04)
#define MAKE_START_MARK                               (LTR010CMD_INSTR | 0x05)

#define WRITE_EEPROM                                  (LTR010CMD_INSTR | 0x06)
#define READ_EEPROM                                   (LTR010CMD_INSTR | 0x07)
#define READ_CONF_RECORD                              (LTR010CMD_INSTR | 0x08)
#define START_STREAM_READ                             (LTR010CMD_INSTR | 0x09)
#define STOP_STREAM_READ                              (LTR010CMD_INSTR | 0x0A)
#define CONFIG_READ_RATE                              (LTR010CMD_INSTR | 0x0B)
#define CMD_CONFIG_PAR_EX                             (LTR010CMD_INSTR | 0x13)

#define INIT                                          (LTR010CMD_INSTR | 0x0F)

#define PARITY_ERROR                                  (LTR010CMD_INSTR | 0x1D)


#define F_OSC  (15000000) // Тактовая частота микроконтроллера AVR

#define LTR4X_INIT_TIMEOUT      500 /* таймаут на ожидание после STOP-RESET-STOP */

#define LTR4X_EEPROM_SIZE       LTR41_EEPROM_SIZE

#endif
