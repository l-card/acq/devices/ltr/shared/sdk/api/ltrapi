cmake_minimum_required(VERSION 2.8.12)

set(LTRAPI_MODULE_NAME                  LPW25)
set(LTRAPI_MODULE_NO_STD_FUNC           ON)
set(LTRAPI_MODULE_NO_LTRAPI             ON)
set(LTRAPI_MODULE_USE_PHASE_FILTER      ON)
set(LTRAPI_MODULE_USE_MATH              ON)
set(LTRAPI_MODULE_USE_DOXYGEN_DOC       ON)
set(LTRAPI_MODULE_REQ_MODULES           ltedsapi)

set(LTRAPI_MODULE_DOC_ABOUT_FILE  doc/about.md.in)
set(LTRAPI_MODULE_DOC_INPUT_FILES doc/gen_descr.md)

include(${LTRAPI_MODULE_CMAKE_TEMPLATE})
