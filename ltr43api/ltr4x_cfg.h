#ifndef LTR_4X_CFG_H_
#define LTR_4X_CFG_H_

#include "ltr43api.h"

#define MODULE_ID               LTR_MID_LTR43
#define MODULE_DEFAULT_NAME     "LTR43"

typedef TLTR43_MODULE_INFO TLTR4X_MODULE_INFO;

// Коды команд AVR
// Коды команд AVR
#define READ_WORD                                     (LTR010CMD_INSTR | 0x01)
#define CONFIG                                        (LTR010CMD_INSTR | 0x02)
#define START_SECOND_MARK                             (LTR010CMD_INSTR | 0x03)
#define STOP_SECOND_MARK                              (LTR010CMD_INSTR | 0x04)
#define MAKE_START_MARK                               (LTR010CMD_INSTR | 0x05)
#define RS485_CONFIG                                  (LTR010CMD_INSTR | 0x06)
#define RS485_SEND_BYTE                               (LTR010CMD_INSTR | 0x07)

#define WRITE_EEPROM                                  (LTR010CMD_INSTR | 0x08)
#define READ_EEPROM                                   (LTR010CMD_INSTR | 0x09)
#define READ_CONF_RECORD                              (LTR010CMD_INSTR | 0x0A)
#define RS485_RECEIVE                                 (LTR010CMD_INSTR | 0x0B)
#define RS485_STOP_RECEIVE                            (LTR010CMD_INSTR | 0x0C)
#define START_STREAM_READ                             (LTR010CMD_INSTR | 0x0D)
#define STOP_STREAM_READ                              (LTR010CMD_INSTR | 0x0E)
#define CONFIG_READ_RATE                              (LTR010CMD_INSTR | 0x10)
#define CMD_RS485_FRAME_CFG                           (LTR010CMD_INSTR | 0x11)
#define CMD_RS485_START_EXCHANGE                      (LTR010CMD_INSTR | 0x12)
#define CMD_CONFIG_PAR_EX                             (LTR010CMD_INSTR | 0x13)


#define INIT                                          (LTR010CMD_INSTR | 0x0F)

#define DATA_OUTPUT_CONFIRM                           (LTR010CMD_INSTR | 0x16)
#define PARITY_ERROR                                  (LTR010CMD_INSTR | 0x17)
#define RS485_ERROR_CONFIRM_TIMEOUT                   (LTR010CMD_INSTR | 0x18)
#define RS485_ERROR_SEND_TIMEOUT                      (LTR010CMD_INSTR | 0x19)
#define DATA_ERROR                                    (LTR010CMD_INSTR | 0x1A)
#define ACK_RS485_RESPONSE_L                          (LTR010CMD_INSTR | 0x1B)
#define ACK_RS485_RESPONSE_H                          (LTR010CMD_INSTR | 0x1C)
#define ACK_RS485_DATA_L                              (LTR010CMD_INSTR | 0x1D)
#define ACK_RS485_DATA_H                              (LTR010CMD_INSTR | 0x1E)


/* Коды ошибок */
#define LTR4X_ERR_DATA_TRANSMISSON_ERROR  LTR43_ERR_DATA_TRANSMISSON_ERROR
#define LTR4X_ERR_RS485_CONFIRM_TIMEOUT   LTR43_ERR_RS485_CONFIRM_TIMEOUT
#define LTR4X_ERR_RS485_SEND_TIMEOUT      LTR43_ERR_RS485_SEND_TIMEOUT

#define F_OSC  (15000000) // Тактовая частота микроконтроллера AVR

#define LTR4X_INIT_TIMEOUT      500 /* таймаут на ожидание после STOP-RESET-STOP */

#define LTR4X_EEPROM_SIZE       LTR43_EEPROM_SIZE

#endif
