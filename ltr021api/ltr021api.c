#include "ltr021api.h"
#include "ltrapitypes.h"
#include "ltrapidefine.h"
#include "ltrd_protocol_defs.h"

#include <stdlib.h>
#include <string.h>


static const TLTR_ERROR_STRING_DEF ErrorStrings[] = {
{ LTR021_ERROR_GET_ARRAY,           "Ошибка выполнения команды GET_ARRAY."},
{ LTR021_ERROR_PUT_ARRAY,           "Ошибка выполнения команды PUT_ARRAY."},
{ LTR021_ERROR_GET_MODULE_NAME,     "Ошибка выполнения команды GET_MODULE_NAME."},
{ LTR021_ERROR_GET_MODULE_GESCR,    "Ошибка выполнения команды GET_MODULE_DESCRIPTOR."},
{ LTR021_ERROR_CRATE_TYPE,          "Неверный тип крейта."},
{ LTR021_ERROR_TIMEOUT,             "Превышение таймаута"}
};



#ifdef _WIN32
BOOL APIENTRY DllMain( HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved) {
    switch (ul_reason_for_call) {
    case DLL_PROCESS_ATTACH:
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}
#endif


//-----------------------------------------------------------------------------
LTR021API_DllExport (INT) LTR021_Init(TLTR021 *module) {
     return module == NULL ? LTR_ERROR_INVALID_MODULE_DESCR : LTR_Init(&module->ltr);
}
//-----------------------------------------------------------------------------
LTR021API_DllExport (INT) LTR021_Open(TLTR021 *module, DWORD saddr, WORD sport, const CHAR *csn) {
    INT res = module == NULL ? LTR_ERROR_INVALID_MODULE_DESCR : LTR_OK;
    if (res==LTR_OK)
        res = LTR021_Close(module);

    if (res == LTR_OK) {
        res = LTR_OpenCrate(&module->ltr, saddr, sport, LTR_CRATE_IFACE_UNKNOWN, csn);
        if (res==LTR_OK) {
            TLTR_CRATE_INFO CrateInfo;
            res = LTR_GetCrateInfo(&module->ltr, &CrateInfo);
            if ((res==LTR_OK) && (CrateInfo.CrateType!=LTR_CRATE_TYPE_LTR021)) {
                res = LTR021_ERROR_CRATE_TYPE;
                LTR021_Close(module);
            }
        }
    }
    return res;
}
//-----------------------------------------------------------------------------
LTR021API_DllExport (INT) LTR021_Close(TLTR021 *module) {
    return module == NULL ? LTR_ERROR_INVALID_MODULE_DESCR : LTR_Close(&module->ltr);
}
//-----------------------------------------------------------------------------
LTR021API_DllExport (INT) LTR021_GetArray(TLTR021 *module, BYTE *buf, DWORD size, DWORD address) {
    INT res = module == NULL ? LTR_ERROR_INVALID_MODULE_DESCR :
                               (buf==NULL) ? LTR_ERROR_PARAMETERS : LTR_OK;
    if (res==LTR_OK) {
        res = LTR_CrateGetArray(&module->ltr, address, buf, size);
    }
    return res;
}
//-----------------------------------------------------------------------------
LTR021API_DllExport (INT) LTR021_PutArray(TLTR021 *module, BYTE *buf, DWORD size, DWORD address) {
    INT res = module == NULL ? LTR_ERROR_INVALID_MODULE_DESCR :
                               (buf==NULL) ? LTR_ERROR_PARAMETERS : LTR_OK;
    if (res==LTR_OK) {
        res = LTR_CratePutArray(&module->ltr, address, buf, size);
    }
    return res;
}
//-----------------------------------------------------------------------------
LTR021API_DllExport (INT) LTR021_GetDescription(TLTR021 *module, TDESCRIPTION_LTR021 *description) {

    INT res = module == NULL ? LTR_ERROR_INVALID_MODULE_DESCR :
                               (description==NULL) ? LTR_ERROR_PARAMETERS : LTR_OK;
    if (res==LTR_OK) {
        DWORD sbuf[] = {CONTROL_COMMAND_START, CONTROL_COMMAND_GET_DESCRIPTION};
        res = LTR__GenericCtlFunc(&module->ltr,
                sbuf, sizeof(sbuf), description, sizeof(TDESCRIPTION_LTR021),
                LTR021_ERROR_GET_MODULE_GESCR, LTR021_ERROR_TIMEOUT);
    }
    return res;
}
//-----------------------------------------------------------------------------
LTR021API_DllExport (INT) LTR021_SetCrateSyncType(TLTR021 *module, DWORD SyncType) {
    INT res= module == NULL ? LTR_ERROR_INVALID_MODULE_DESCR : LTR_IsOpened(&module->ltr);
    if (res==LTR_OK) {
        res=LTR021_PutArray(module, (BYTE*)&SyncType, sizeof(SyncType), SEL_AVR_DM|0x800A);
    }
    return res;
}
//-----------------------------------------------------------------------------
LTR021API_DllExport (LPCSTR) LTR021_GetErrorString(INT error) {
    size_t i;
    for (i = 0; i < sizeof(ErrorStrings) / sizeof(ErrorStrings[0]); i++) {
        if (ErrorStrings[i].code == error)
            return ErrorStrings[i].message;
    }
    return LTR_GetErrorString(error);
}
//----------------------------------------------------------------------------- 
