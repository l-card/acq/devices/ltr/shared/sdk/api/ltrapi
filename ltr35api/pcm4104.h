#ifndef LPW100_PCM4104_H__
#define LPW100_PCM4104_H__

#define PCM4104_CTRL_WR_BYTE(addr)         (0x20 | ((addr) & 0xF))
#define PCM4104_CTRL_RD_BYTE(addr)         (0x80 | 0x20 | ((addr) & 0xF))

#define PCM4104_REGADDR_ZERO             0
#define PCM4104_REGADDR_ATTENUATION_CH1  1
#define PCM4104_REGADDR_ATTENUATION_CH2  2
#define PCM4104_REGADDR_ATTENUATION_CH3  3
#define PCM4104_REGADDR_ATTENUATION_CH4  4
#define PCM4104_REGADDR_FCR              5 //funcion control register
#define PCM4104_REGADDR_SCR              6 //system control register
#define PCM4104_REGADDR_ASPC             7 //audio serial port control register

#define PCM4104_REGADDR_ATTENUATION_CH(ch) (1+ch)

//********************* биты регистра  PCM4104_REGADDR_FCR ****************//
#define PCM4104_FCR_DEM_Pos             0
#define PCM4104_FCR_DEM_Msk             (0x3UL << PCM4104_FCR_DEM_Pos)
#define PCM4104_FCR_DEM_Dis             (0 << PCM4104_FCR_DEM_Pos)
#define PCM4104_FCR_DEM_FS48            (1 << PCM4104_FCR_DEM_Pos)
#define PCM4104_FCR_DEM_FS44            (2 << PCM4104_FCR_DEM_Pos)
#define PCM4104_FCR_DEM_FS32            (3 << PCM4104_FCR_DEM_Pos)


#define PCM4104_FCR_PHASE_Pos       2
#define PCM4104_FCR_PHASE_Msk       (0x1UL << PCM4104_FCR_PHASE_Pos)
#define PCM4104_FCR_PHASE           PCM4104_FCR_PHASE_Msk

#define PCM4104_FCR_ZDM_Pos         3
#define PCM4104_FCR_ZDM_Msk        (0x1UL << PCM4104_FCR_ZDM_Pos)
#define PCM4104_FCR_ZDM             PCM4104_FCR_ZDM_Msk

#define PCM4104_FCR_MUT1_Pos        4
#define PCM4104_FCR_MUT1_Msk        (0x1UL << PCM4104_FCR_MUT1_Pos)
#define PCM4104_FCR_MUT1            PCM4104_FCR_MUT1_Msk

#define PCM4104_FCR_MUT2_Pos        5
#define PCM4104_FCR_MUT2_Msk        (0x1UL << PCM4104_FCR_MUT2_Pos)
#define PCM4104_FCR_MUT2            PCM4104_FCR_MUT2_Msk

#define PCM4104_FCR_MUT3_Pos        6
#define PCM4104_FCR_MUT3_Msk        (0x1UL << PCM4104_FCR_MUT3_Pos)
#define PCM4104_FCR_MUT3            PCM4104_FCR_MUT3_Msk

#define PCM4104_FCR_MUT4_Pos        7
#define PCM4104_FCR_MUT4_Msk        (0x1UL << PCM4104_FCR_MUT4_Pos)
#define PCM4104_FCR_MUT4            PCM4104_FCR_MUT4_Msk

//********************* биты регистра  PCM4104_REGADDR_SCR ****************//
//sampling mode
#define PCM4104_SCR_FS_Pos             0
#define PCM4104_SCR_FS_Msk             (0x3UL << PCM4104_SCR_FS_Pos)
#define PCM4104_SCR_FS_SINGLE_RATE     (0 << PCM4104_SCR_FS_Pos)
#define PCM4104_SCR_FS_DOUBLE_RATE     (1 << PCM4104_SCR_FS_Pos)
#define PCM4104_SCR_FS_QUAD_RATE       (2 << PCM4104_SCR_FS_Pos)

//power down ch 1 and 2
#define PCM4104_SCR_PDN12_Pos          2
#define PCM4104_SCR_PDN12_Msk          (0x1UL << PCM4104_SCR_PDN12_Pos)
#define PCM4104_SCR_PDN12              PCM4104_SCR_PDN12_Msk

//power down ch 3 and 4
#define PCM4104_SCR_PDN34_Pos          3
#define PCM4104_SCR_PDN34_Msk          (0x1UL << PCM4104_SCR_PDN34_Pos)
#define PCM4104_SCR_PDN34              PCM4104_SCR_PDN34_Msk

//software reset
#define PCM4104_SCR_RST_Pos            7
#define PCM4104_SCR_RST_Msk            (0x1UL << PCM4104_SCR_RST_Pos)
#define PCM4104_SCR_RST                PCM4104_SCR_RST_Msk


//********************* биты регистра  PCM4104_REGADDR_ASPC ****************//
//Audio Data Format
#define PCM4104_ASPC_FMT_Pos           0
#define PCM4104_ASPC_FMT_Msk           (0x7UL << PCM4104_ASPC_FMT_Pos)
#define PCM4104_ASPC_FMT_24_LJ         (0 << PCM4104_ASPC_FMT_Pos)
#define PCM4104_ASPC_FMT_24_I2S        (1 << PCM4104_ASPC_FMT_Pos)
#define PCM4104_ASPC_FMT_TDM           (2 << PCM4104_ASPC_FMT_Pos)
#define PCM4104_ASPC_FMT_TDM_BCK       (3 << PCM4104_ASPC_FMT_Pos)
#define PCM4104_ASPC_FMT_24_RJ         (4 << PCM4104_ASPC_FMT_Pos)
#define PCM4104_ASPC_FMT_20_RJ         (5 << PCM4104_ASPC_FMT_Pos)
#define PCM4104_ASPC_FMT_18_RJ         (6 << PCM4104_ASPC_FMT_Pos)
#define PCM4104_ASPC_FMT_16_RJ         (7 << PCM4104_ASPC_FMT_Pos)

//LRCK Polarity (0 = Normal, 1 = Inverted).
#define PCM4104_ASPC_LRCKP_Pos         (4)
#define PCM4104_ASPC_LRCKP_Msk         (0x1UL << PCM4104_ASPC_LRCKP_Pos)
#define PCM4104_ASPC_LRCKP             PCM4104_ASPC_LRCKP_Msk

//BCK Sampling Edge (0 = Rising Edge, 1 = Falling Edge)
#define PCM4104_ASPC_BCKE_Pos          (5)
#define PCM4104_ASPC_BCKE_Msk          (0x1UL << PCM4104_ASPC_BCKE_Pos)
#define PCM4104_ASPC_BCKE               PCM4104_ASPC_BCKE_Msk


#endif
