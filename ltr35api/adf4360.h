#ifndef LPW100_ADF4360_H___
#define LPW100_ADF4360_H___


#define ADF4360_LATCH_CONTROL     (0x0)
#define ADF4360_LATCH_NCOUNTER    (0x2)
#define ADF4360_LATCH_RCOUNTER    (0x1)


//***************************   поля LATCH CONTROL   ********************************
//power core level
#define ADF4360_LATCHC_PC_Pos      (2)
#define ADF4360_LATCHC_PC_Msk      (0x3UL << ADF4360_LATCHC_PC_Pos)

#define ADF4360_LATCHC_PC_2_5mA      (0 << ADF4360_LATCHC_PC_Pos)
#define ADF4360_LATCHC_PC_5_0mA      (1 << ADF4360_LATCHC_PC_Pos)
#define ADF4360_LATCHC_PC_7_5mA      (2 << ADF4360_LATCHC_PC_Pos)
#define ADF4360_LATCHC_PC_10_0mA     (3 << ADF4360_LATCHC_PC_Pos)

//counter reset
#define ADF4360_LATCHC_CR_Pos         (4)
#define ADF4360_LATCHC_CR_Msk         (0x1UL << ADF4360_LATCHC_CR_Pos)
#define ADF4360_LATCHC_CR              ADF4360_LATCHC_CR_Msk

//divout control
#define ADF4360_LATCHC_DIVOUT_Pos     (5)
#define ADF4360_LATCHC_DIVOUT_Msk     (0x7UL << ADF4360_LATCHC_DIVOUT_Pos)

#define ADF4360_LATCHC_DIVOUT_VDD      (0 << ADF4360_LATCHC_DIVOUT_Pos)
#define ADF4360_LATCHC_DIVOUT_LOCK_DET (1 << ADF4360_LATCHC_DIVOUT_Pos)
#define ADF4360_LATCHC_DIVOUT_N        (2 << ADF4360_LATCHC_DIVOUT_Pos)
#define ADF4360_LATCHC_DIVOUT_R        (4 << ADF4360_LATCHC_DIVOUT_Pos)
#define ADF4360_LATCHC_DIVOUT_A2       (5 << ADF4360_LATCHC_DIVOUT_Pos)
#define ADF4360_LATCHC_DIVOUT_A        (6 << ADF4360_LATCHC_DIVOUT_Pos)
#define ADF4360_LATCHC_DIVOUT_DGND     (7 << ADF4360_LATCHC_DIVOUT_Pos)

//phase detector polarity
#define ADF4360_LATCHC_PDP_Pos         (8)
#define ADF4360_LATCHC_PDP_Msk         (0x1UL << ADF4360_LATCHC_PDP_Pos)
#define ADF4360_LATCHC_PDP              ADF4360_LATCHC_PDP_Msk

//CP three-state
#define ADF4360_LATCHC_CP_Pos          (9)
#define ADF4360_LATCHC_CP_Msk          (0x1UL << ADF4360_LATCHC_CP_Pos)
#define ADF4360_LATCHC_CP               ADF4360_LATCHC_CP_Msk

//CP Gain
#define ADF4360_LATCHC_CPG_Pos         (10)
#define ADF4360_LATCHC_CPG_Msk         (0x1UL << ADF4360_LATCHC_CPG_Pos)
#define ADF4360_LATCHC_CPG              ADF4360_LATCHC_CPG_Msk

//mute-tilld
#define ADF4360_LATCHC_MTLD_Pos        (11)
#define ADF4360_LATCHC_MTLD_Msk        (0x1UL << ADF4360_LATCHC_MTLD_Pos)
#define ADF4360_LATCHC_MTLD             ADF4360_LATCHC_MTLD_Msk

//output power lewel 
#define ADF4360_LATCHC_PL_Pos          (12)
#define ADF4360_LATCHC_PL_Msk          (0x3UL << ADF4360_LATCHC_PL_Pos)

#define ADF4360_LATCHC_PL_3_5mA        (0 << ADF4360_LATCHC_PL_Pos)
#define ADF4360_LATCHC_PL_5_0mA        (1 << ADF4360_LATCHC_PL_Pos)
#define ADF4360_LATCHC_PL_7_5mA        (2 << ADF4360_LATCHC_PL_Pos)
#define ADF4360_LATCHC_PL_11_0mA       (3 << ADF4360_LATCHC_PL_Pos)

//current settings 1
#define ADF4360_LATCHC_CPI1_Pos        (14)
#define ADF4360_LATCHC_CPI1_Msk        (0x7UL << ADF4360_LATCHC_CPI1_Pos)

#define ADF4360_LATCHC_CPI2_Pos        (17)
#define ADF4360_LATCHC_CPI2_Msk        (0x7UL << ADF4360_LATCHC_CPI2_Pos)

//power down
#define ADF4360_LATCHC_PD_Pos          (20)
#define ADF4360_LATCHC_PD_Msk          (0x3UL << ADF4360_LATCHC_PD_Pos)

#define ADF4360_LATCHC_PD_NORMAL_OP    (0 << ADF4360_LATCHC_PD_Pos)
#define ADF4360_LATCHC_PD_ASYNC        (1 << ADF4360_LATCHC_PD_Pos)
#define ADF4360_LATCHC_PD_SYNC         (3 << ADF4360_LATCHC_PD_Pos)


//**************************   поля LATCH N COUNTER   *********************************
#define ADF4360_LATCHN_A_Pos       (2)
#define ADF4360_LATCHN_A_Msk       (0x1FUL << ADF4360_LATCHN_A_Pos)

#define ADF4360_LATCHN_B_Pos       (8)
#define ADF4360_LATCHN_B_Msk       (0x1FFFUL << ADF4360_LATCHN_B_Pos)


#define ADF4360_LATCHN_CPG_Pos     (21)
#define ADF4360_LATCHN_CPG_Msk     (1UL << ADF4360_LATCHN_CPG_Pos)
#define ADF4360_LATCHN_CPG          ADF4360_LATCHN_CPG_Msk

//**************************   поля LATCH R COUNTER   ********************************
#define ADF4360_LATCHR_R_Pos      (0x2)
#define ADF4360_LATCHR_R_Msk      (0x3FFFUL << ADF4360_LATCHR_R_Pos)

//anti-backlash pulse width
#define ADF4360_LATCHR_ABP_Pos    (16)
#define ADF4360_LATCHR_ABP_Msk    (0x3UL << ADF4360_LATCHR_ABP_Pos)

#define ADF4360_LATCHR_ABP_3_0    (0 << ADF4360_LATCHR_ABP_Pos)
#define ADF4360_LATCHR_ABP_1_3    (1 << ADF4360_LATCHR_ABP_Pos)
#define ADF4360_LATCHR_ABP_6_0    (2 << ADF4360_LATCHR_ABP_Pos)

//lock detect precision
#define ADF4360_LATCHR_LDP_Pos    (18)
#define ADF4360_LATCHR_LDP_Msk    (0x3UL << ADF4360_LATCHR_LDP_Pos)
#define ADF4360_LATCHR_LDP        ADF4360_LATCHR_LDP_Msk

//band select clock
#define ADF4360_LATCHR_BSC_Pos    (20)
#define ADF4360_LATCHR_BSC_Msk    (0x3FUL << ADF4360_LATCHR_BSC_Pos)

#define ADF4360_LATCHR_BSC_1      (0 << ADF4360_LATCHR_BSC_Pos)
#define ADF4360_LATCHR_BSC_2      (1 << ADF4360_LATCHR_BSC_Pos)
#define ADF4360_LATCHR_BSC_4      (2 << ADF4360_LATCHR_BSC_Pos)
#define ADF4360_LATCHR_BSC_8      (3 << ADF4360_LATCHR_BSC_Pos)





#endif
