cmake_minimum_required(VERSION 2.8.12)

set(LTRAPI_MODULE_NAME      LTR51)
set(LTRAPI_MODULE_USE_TIMER ON)
set(LTRAPI_MODULE_USE_CRC   ON)


#для включения прошивки в библиотеку под Windows
if(WIN32)
    set(LTRAPI_MODULE_RC_STRINGS        "LTR51_TTF   RCDATA  ${CMAKE_CURRENT_SOURCE_DIR}/ltr51.ttf")
    set(LTRAPI_MODULE_BUILD_DEPENDENCIES ${CMAKE_CURRENT_SOURCE_DIR}/ltr51.ttf)
endif(WIN32)

set(LTRAPI_MODULE_INSTALL_DATA_FILES "ltr51.ttf")

include(${LTRAPI_MODULE_CMAKE_TEMPLATE})
