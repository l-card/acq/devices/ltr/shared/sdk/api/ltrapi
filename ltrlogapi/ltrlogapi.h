#ifndef __LTRLOGAPI_H_
#define __LTRLOGAPI_H_

#include "ltrapi.h"
#include <stddef.h>


#ifdef _WIN32
    #ifdef LTRLOGAPI_EXPORTS
        #define LTRLOGAPI_DllExport(type)    __declspec(dllexport) type APIENTRY
    #else
        #define LTRLOGAPI_DllExport(type)    __declspec(dllimport) type APIENTRY
    #endif
#elif defined __GNUC__
    #define LTRLOGAPI_DllExport(type) __attribute__ ((visibility("default"))) type
#else
    #define LTRLOGAPI_DllExport(type) type
#endif


#define LTRLOG_MSG_SIGN   0xA54C



#pragma pack (4)
typedef struct {
    INT    Size;
    TLTR   Channel;
    PVOID  Internal;
} TLTRLOG;


/** Структура, описывающая переданное сообщение журнала */
typedef struct {
    WORD Sign; /**< Признак сообщения. Всегда равен #LTRLOG_MSG_SIGN */
    WORD Size; /**< Размер всего сообщения с заголовком */
    INT  Err;  /**< Код ошибки (имеет значение только для уровня ошибок) */
    LARGE_INTEGER Time; /**< Время возникновения сообщения в сервере */
    BYTE Lvl; /**< Уровень сообщения */
    BYTE Reserved[15]; /**< Резерв */
    char Msg[1]; /**< Текст сообщения (переменной длины) */
} TLTRLOG_MSG;

/** Размер заголовка сообщения (фиксированной части) */
#define LTRLOG_MSG_HDR_SIZE (offsetof(TLTRLOG_MSG, Msg))

#pragma pack ()

#ifdef __cplusplus
extern "C" {
#endif
  
  
LTRLOGAPI_DllExport(INT) LTRLOG_Init(TLTRLOG *hnd);
LTRLOGAPI_DllExport(INT) LTRLOG_Open(TLTRLOG *hnd, DWORD net_addr, WORD net_port);
LTRLOGAPI_DllExport(INT) LTRLOG_Close(TLTRLOG *hnd);
LTRLOGAPI_DllExport(INT) LTRLOG_IsOpened(TLTRLOG *hnd);
LTRLOGAPI_DllExport(INT) LTRLOG_Shutdown(TLTRLOG *hnd);

/***************************************************************************//**
 * Прием сообщения от сервера. Возвращает управление как только будет принято
 * одно полное сообщение от сервера или истечет таймаут.
 *
 * В случае приема целового сообщения возвращает в msg указатель на созданную
 * структуру сообщения, которую затем следует освободить с помощью LTRLOG_FreeMsg().
 * Если за заданный таймаут не принято ни одного полного сообщения, то в msg
 * возвращается нулевой указатель.
 *
 * @param [in]  hnd      Описатель соединения
 * @param [out] msg      В данной переменной возвращается указатель на созданную
 *                       структуру с принятым сообщением или NULL, если сообщение
 *                       не было принято.
 * @param [in]  tout     Таймаут на время приема сообщения в мс
 * @return               Код ошибки
 ******************************************************************************/
LTRLOGAPI_DllExport(INT) LTRLOG_GetNextMsg(TLTRLOG *hnd, TLTRLOG_MSG **msg, DWORD tout);

/***************************************************************************//**
 * Освобождение памяти структуры сообщения, возвращанной LTRLOG_GetNextMsg().
 * Должна вызываться всегда при получении нового сообщения после его обработки.
 *
 * @param[in]  msg      Указатель на созданную структуру сообщения
 * @return              Код ошибки
 ******************************************************************************/
LTRLOGAPI_DllExport(INT) LTRLOG_FreeMsg(TLTRLOG_MSG *msg);

#ifdef __cplusplus
}
#endif


#endif
