cmake_minimum_required(VERSION 2.8.12)

set(LTRAPI_MODULE_PROJECT           ltrlogapi)
set(LTRAPI_MODULE_USE_TIMER         ON)
set(LTRAPI_MODULE_EXPORT_SYMBOL     "LTRLOGAPI_EXPORTS")
set(LTRAPI_MODULE_DESCR_ENG         "Library for getting ltrd log")
set(LTRAPI_MODULE_DESCR_RUS         "Библиотека для получения журнала ltrd")



include(${LTRAPI_MODULE_CMAKE_TEMPLATE})
