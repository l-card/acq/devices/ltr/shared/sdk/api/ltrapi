#ifndef LTRAPIDEFINE_H_
#define LTRAPIDEFINE_H_


/***************************************************************************//**
  @addtogroup const_list
  @{
  *****************************************************************************/

/** IP-адрес для полключения к службе ltrd, соответствующий случаю, когда
   служба запущена на локальной машине (той же, откуда устанавливается соединение) */
#define LTRD_ADDR_LOCAL   (0x7F000001l)
/** IP-адрес по умолчанию для подключения к службе ltrd */
#define LTRD_ADDR_DEFAULT (LTRD_ADDR_LOCAL)
/** TCP-порт, использующийся по умолчанию, для подключения к службе ltrd */
#define LTRD_PORT_DEFAULT (11111)


/** Максимальное количество крейтов, которое можно получить с помощью
    функции LTR_GetCrates(). В случае, если крейтов может быть больше, то
    можно воспользоваться функцией LTR_GetCratesEx(), которая не имеет
    ограничения на количество крейтов */
#define LTR_CRATES_MAX             16l
/** Максимальное количество модулей в одном крейте */
#define LTR_MODULES_PER_CRATE_MAX  16l


/** Если данная строка используется вместо серийного номера крейта при установлении
    подключения, то будет установлено управляющее соединение с ltrd, не связанное
    ни с одним крейтом */
#define LTR_CSN_SERVER_CONTROL              "#SERVER_CONTROL"



/** Номера каналов для соединения со службой ltrd */
enum en_LTR_CC_ChNum {
    LTR_CC_CHNUM_CONTROL                = 0,  /**< Канал для передачи управляющих запросов крейту или службе ltrd */
    LTR_CC_CHNUM_MODULE1                = 1,  /**< Канал для работы c модулем в слоте 1 */
    LTR_CC_CHNUM_MODULE2                = 2,  /**< Канал для работы c модулем в слоте 2 */
    LTR_CC_CHNUM_MODULE3                = 3,  /**< Канал для работы c модулем в слоте 3 */
    LTR_CC_CHNUM_MODULE4                = 4,  /**< Канал для работы c модулем в слоте 4 */
    LTR_CC_CHNUM_MODULE5                = 5,  /**< Канал для работы c модулем в слоте 5 */
    LTR_CC_CHNUM_MODULE6                = 6,  /**< Канал для работы c модулем в слоте 6 */
    LTR_CC_CHNUM_MODULE7                = 7,  /**< Канал для работы c модулем в слоте 7 */
    LTR_CC_CHNUM_MODULE8                = 8,  /**< Канал для работы c модулем в слоте 8 */
    LTR_CC_CHNUM_MODULE9                = 9,  /**< Канал для работы c модулем в слоте 9 */
    LTR_CC_CHNUM_MODULE10               = 10, /**< Канал для работы c модулем в слоте 10 */
    LTR_CC_CHNUM_MODULE11               = 11, /**< Канал для работы c модулем в слоте 11 */
    LTR_CC_CHNUM_MODULE12               = 12, /**< Канал для работы c модулем в слоте 12 */
    LTR_CC_CHNUM_MODULE13               = 13, /**< Канал для работы c модулем в слоте 13 */
    LTR_CC_CHNUM_MODULE14               = 14, /**< Канал для работы c модулем в слоте 14 */
    LTR_CC_CHNUM_MODULE15               = 15, /**< Канал для работы c модулем в слоте 15 */
    LTR_CC_CHNUM_MODULE16               = 16, /**< Канал для работы c модулем в слоте 16 */
    /** @cond not_supported */
    LTR_CC_CHNUM_USERDATA               = 18  /**< Псевдоканал для передачи пользовательских
                                                данных от крейта. Может использоваться только
                                                в специализированных пользовательских прошивках
                                               */
    /** @endcond */
};

/** Фалаги канала связи ltrd для явного задания интерфейса крейта */
enum en_LTR_CC_Iface {
    LTR_CC_IFACE_USB                = 0x0100, /**< Явное указание, что соединение должно быть
                                                  с крейтом, подключенным по USB-интерфейсу */
    LTR_CC_IFACE_ETH                = 0x0200, /**< Явное указание, что соединение должно быть
                                                  с крейтом, подключенным по Ethernet (TCP/IP) */
};

/** Дополнительные флаги канала связи с ltrd */
enum en_LTR_CC_Flags {
    /** @cond not_supported */
    LTR_CC_FLAG_RAW_DATA            = 0x4000 /**< флаг отладки --- ltrd передает клиенту
                                                  все данные, которые приходят от крейта,
                                                  без разбивки по модулям */
    /** @endcond */
};


/** @cond internal */
#define LTR_CC_CHNUM(cc)        ((cc) & 0x00FF)
#define LTR_CC_IFACE(cc)        ((cc) & 0x0F00)
#define LTR_CC_FLAGS(cc)        ((cc) & 0xF000)
/** @endcond */


/** Флаги состояния соединения */
enum en_LTR_ChStateFlags {
    LTR_FLAG_RBUF_OVF            = (1u<<0),    /**< Флаг переполнения буфера. Указывает что
                                                    из-за того, что клиент не считывал данные,
                                                    в ltrd произошло переполнение очереди клиента.
                                                    соответственно в принятых данных есть разрыв */
    LTR_FLAG_RFULL_DATA          = (1u<<1)     /**< Флаг получения данных в полном формате
                                                    в функции LTR_GetCrateRawData() */
};

/** Макрос задания идентификатора модуля с указанным номером */
#define LTR_MID_MODULE(x)             (((x) & 0xFF) | (((x) & 0xFF) << 8))

/** Идентификаторы модулей */
enum en_LTR_MIDs {
    LTR_MID_EMPTY               = 0,                    /**< Пустой слот */
    LTR_MID_IDENTIFYING         = 0xFFFF,               /**< Модуль в процессе определения типа */
    LTR_MID_INVALID             = 0xFFFE,               /**< Модуль с недействительным индикатором */
    LTR_MID_LTR01               = LTR_MID_MODULE(1),    /**< Идентификатор модуля LTR01 */
    LTR_MID_LTR11               = LTR_MID_MODULE(11),   /**< Идентификатор модуля LTR11 */
    LTR_MID_LTR12               = LTR_MID_MODULE(12),   /**< Идентификатор модуля LTR11 */
    LTR_MID_LTR22               = LTR_MID_MODULE(22),   /**< Идентификатор модуля LTR22 */
    LTR_MID_LTR24               = LTR_MID_MODULE(24),   /**< Идентификатор модуля LTR24 */
    LTR_MID_LTR25               = LTR_MID_MODULE(25),   /**< Идентификатор модуля LTR25 */
    LTR_MID_LTR27               = LTR_MID_MODULE(27),   /**< Идентификатор модуля LTR27 */
    LTR_MID_LTR34               = LTR_MID_MODULE(34),   /**< Идентификатор модуля LTR34 */
    LTR_MID_LTR35               = LTR_MID_MODULE(35),   /**< Идентификатор модуля LTR35 */
    LTR_MID_LTR41               = LTR_MID_MODULE(41),   /**< Идентификатор модуля LTR41 */
    LTR_MID_LTR42               = LTR_MID_MODULE(42),   /**< Идентификатор модуля LTR42 */
    LTR_MID_LTR43               = LTR_MID_MODULE(43),   /**< Идентификатор модуля LTR43 */
    LTR_MID_LTR51               = LTR_MID_MODULE(51),   /**< Идентификатор модуля LTR51 */
    LTR_MID_LTR114              = LTR_MID_MODULE(114),  /**< Идентификатор модуля LTR114 */
    LTR_MID_LTR210              = LTR_MID_MODULE(210),  /**< Идентификатор модуля LTR210 */
    LTR_MID_LTR212              = LTR_MID_MODULE(212),  /**< Идентификатор модуля LTR212 */
    LTR_MID_LTR216              = LTR_MID_MODULE(216)   /**< Идентификатор модуля LTR216 */
};

/** Типы крейтов */
enum en_LTR_CrateTypes {
    LTR_CRATE_TYPE_UNKNOWN                      = 0, /**< Неизвестный тип крейта */
    LTR_CRATE_TYPE_LTR010                       = 10, /**< Крейт LTR-U-8 или LTR-U-16 */
    LTR_CRATE_TYPE_LTR021                       = 21, /**< Крейт LTR-U-1 */
    LTR_CRATE_TYPE_LTR030                       = 30, /**< Крейт LTR-EU-8 или LTR-EU-16 */
    LTR_CRATE_TYPE_LTR031                       = 31, /**< Крейт LTR-EU-2 */
    /** @cond kd_extension */
    LTR_CRATE_TYPE_LTR032                       = 32, /**< Крейт LTR-E-7 или LTR-E-15 */
    /** @endcond */
    LTR_CRATE_TYPE_LTR_CU_1                     = 40, /**< Крейт LTR-CU-1 */
    LTR_CRATE_TYPE_LTR_CEU_1                    = 41, /**< Крейт LTR-CEU-1 */
    LTR_CRATE_TYPE_BOOTLOADER                   = 99  /**< Крейт в режиме загрузчика
                                                           (если в этом режиме нельзя узнать тип) */
};

/** Интерфейс подключения крейта */
enum en_LTR_CrateIface {
    LTR_CRATE_IFACE_UNKNOWN             = 0, /**< Неизвествный код интерфейса крейта.
                                                          При передаче в функции данное значение
                                                          может указывать, что интерфейс подклучения
                                                          крейта не имеет значения */
    LTR_CRATE_IFACE_USB                 = 1, /**< Крейт подключен по интерфейсу USB */
    LTR_CRATE_IFACE_TCPIP               = 2  /**< Крейт подключен по Ethernet (TCP/IP) */
};


/** Состояние соединения с крейтом, соответствующим записи с IP-адресом */
enum en_LTR_CrateIpStatus {
    LTR_CRATE_IP_STATUS_OFFLINE                = 0, /**< Крейт не подключен */
    LTR_CRATE_IP_STATUS_CONNECTING             = 1, /**< Идет процесс установки соединения с крейтом
                                                         (операция запущена, но еще не завершена) */
    LTR_CRATE_IP_STATUS_ONLINE                 = 2, /**< Крейт подключен */
    LTR_CRATE_IP_STATUS_ERROR                  = 3  /**< Ошибка подключения крейта.
                                                         Установить соединение не удалось. */
};

/** Флаги, соответствующие записи с IP-адресом крейта */
enum en_LTR_CrateIpFlags {
    /** Флаг указывает, что служба ltrd должна при старте или при обнаружении
        новой сети должна выполнить подключение к крейту с IP-адресом,
        соответствующим данной записи */
    LTR_CRATE_IP_FLAG_AUTOCONNECT              =  0x1,
    /** Флаг указывает, что при ошибке установки соединения с крейтом
        или при разрыве связи по ошибке с работающим крейтом служба ltrd должна
        выполнить новую попытку подключения. Если флаг не установлен, то в этих
        случаях запись переходит в состояние #LTR_CRATE_IP_STATUS_ERROR и никаких
        действий дальше не выполняется. Если флаг установлен, то через заданный
        интервал времени будет осуществлена новая попытка соединения и
        эти попытки будут повторяться до тех пор, пока соединение не будет
        установлено,  либо пока не будет выполнена явная команда разрыва соединения,
        снят этот флаг или удалена соответствующая запись с IP-адресом.

        Данная возможность доступна начиная с версии ltrd 2.1.5.0 и ltrapi 1.31.1. */
    LTR_CRATE_IP_FLAG_RECONNECT                =  0x2
};




/** Размер строки с названием модуля в описании модуля */
#define LTR_MODULE_NAME_SIZE                16
/** Размер строки с названием`устройства в описании крейта */
#define LTR_CRATE_DEVNAME_SIZE              32
/** Размер строки с серийным номером крейта */
#define LTR_CRATE_SERIAL_SIZE               16
/** Размер строки с версией прошивки крейта в его описании */
#define LTR_CRATE_SOFTVER_SIZE              32
/** Размер строки с ревизией крейта в его описании */
#define LTR_CRATE_REVISION_SIZE             16
/** Размер строки с описанием опций платы в описании крейта */
#define LTR_CRATE_BOARD_OPTIONS_SIZE       16
/** Размер строки с версией загрузчика в описании крейта */
#define LTR_CRATE_BOOTVER_SIZE              16
/** Размер строки с описанием процессора в описании крейта */
#define LTR_CRATE_CPUTYPE_SIZE              16
/** Размер строки с описанием типа крейта */
#define LTR_CRATE_TYPE_NAME                 16
/** Размер дополнительной информации о крейте */
#define LTR_CRATE_SPECINFO_SIZE             48
/** Размер строки с описанием типа FPGA в описании крейта */
#define LTR_CRATE_FPGA_NAME_SIZE            32
/** Размер строки с версией прошивки FPGA в описании крейта */
#define LTR_CRATE_FPGA_VERSION_SIZE         32

/** Максимальное кол-во термометров в крейте, показания которых отображаются в статистике */
#define LTR_CRATE_THERM_MAX_CNT              8


/** Флаги из описания модуля */
enum en_LTR_ModuleDescrFlags {
    LTR_MODULE_FLAGS_HIGH_BAUD          = 0x0001, /**< Признак, что модуль использует
                                                       высокую скорость интерфейса
                                                       передачи слов в модуль */
    LTR_MODULE_FLAGS_USE_HARD_SEND_FIFO = 0x0100, /**< Признак, что модуль использует статистику
                                                       внутреннего аппаратного FIFO на передачу
                                                       данных */
    LTR_MODULE_FLAGS_USE_SYNC_MARK      = 0x0200  /**< Признак, что модуль поддерживает
                                                       генерирование синхрометок */
};

/** @cond kd_extension */
#define LTR_CARD_START_OFF (0)
#define LTR_CARD_START_RUN (1)
/** @endcond */


/** Таймаут по умолчанию в мс на выполнение запросов к ltrd */
#define LTR_DEFAULT_SEND_RECV_TIMEOUT   10000UL


/** Режим работы крейта */
enum en_LTR_CrateMode {
    LTR_CRATE_MODE_BOOTLOADER = 1, /**< Крейт находится в состоянии загрузчика */
    LTR_CRATE_MODE_WORK       = 2, /**< Крейт находится в рабочем состоянии */
    LTR_CRATE_MODE_CONTROL    = 3  /**< Крейт находится в состоянии, когда принимает
                                     только управляющие запросы (например, если
                                     крейт подключен не по тому интерфейсу, на который
                                     нестроен) */
};


/** @brief Состояние ПЛИС

    Константы, определяющие текущее состояние ПЛИС модуля. Данные константы
    используются в функциях модуля и внесены в ltrapi, т.к. являются общими для
    гурппы модулей, но они явно не используются функциями данной библиотеки */
typedef enum {
    LTR_FPGA_STATE_NO_POWER       = 0x0, /**< Нет сигнала питания ПЛИС */
    LTR_FPGA_STATE_NSTATUS_TOUT   = 0x1, /**< Истекло время ожидания готовности
                                                ПЛИС к загрузке */
    LTR_FPGA_STATE_CONF_DONE_TOUT = 0x2, /**< Истекло время ожидания завершения
                                                загрузки ПЛИС (обычно означает,
                                                что во Flash нет действительной
                                                прошивки) */
    LTR_FPGA_STATE_LOAD_PROGRESS  = 0x3, /**< Идет загрузка ПЛИС */
    LTR_FPGA_STATE_POWER_ON       = 0x4, /**< Состояние после POWER_ON. Свидетельсвует
                                              о том, что было неожиданное снятие питания */
    LTR_FPGA_STATE_LOAD_DONE      = 0x7, /**< Загрузка ПЛИС завершена,
                                                но работа ПЛИС еще не разрешена */
    LTR_FPGA_STATE_WORK           = 0xF  /**< Нормальное рабочее состояние ПЛИС */
} e_LTR_FPGA_STATE;
/*================================================================================================*/

/** @} */

#endif /*#ifndef LTRAPIDEFINE_H_*/

