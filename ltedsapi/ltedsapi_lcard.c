#include "ltedsapi_lcard.h"



LTEDSAPI_DllExport(BOOL) LTEDS_IsLCardManufacturerID(WORD manid) {
    return manid == LTEDS_LCARD_MANUFACTURER_ID;
}


LTEDSAPI_DllExport(WORD) LTEDS_GetLCardManufacturerID(void) {
    return LTEDS_LCARD_MANUFACTURER_ID;
}

LTEDSAPI_DllExport(INT)  LTEDS_GetLCardTmpltInfoCurImpTable(TLTEDS_DECODE_CONTEXT *ctx, TLTEDS_LCARD_INFO_TMPLT_CUR_IMP *info) {
    DWORD val;
    INT err;
    err = LTEDS_GetSelectCase(ctx, 3, &val);
    if (err == LTEDS_OK) {
        BYTE fmt = val & 0x7;
        if (info != NULL)
            info->PrecFormat = fmt;

        if (fmt == 0) {
            err =  LTEDS_GetStructArraySize(ctx, 7, &val);
            if (err == LTEDS_OK) {
                BYTE pt_idx, pt_cnt;
                pt_cnt = val & 0x7F;
                if (info != NULL)
                    info->DataSetCount = pt_cnt;
                for (pt_idx = 0; (pt_idx < pt_cnt) && (err == LTEDS_OK); pt_idx++) {
                    TLTEDS_LCARD_INFO_CUR_IMP_DATASET dset;
                    err = LTEDS_GetConRes(ctx, 15, 0, 1e-6, &dset.SourceCurrent);
                    if (err == LTEDS_OK)
                        err = LTEDS_GetConRes(ctx, 18, 0, 0.1, &dset.OutputImpedance);
                    if ((err == LTEDS_OK) && info)
                        info->DataSets[pt_idx] = dset;
                }
            }
        } else {
            err = LTEDS_ERROR_UNKNOWN_SEL_CASE;
        }
    }
    return err;
}


LTEDSAPI_DllExport(INT) LTEDS_PutLCardTmpltInfoCurImpTable(TLTEDS_ENCODE_CONTEXT *ctx,
                                                           const TLTEDS_LCARD_INFO_TMPLT_CUR_IMP *info) {
    INT err;
    err = LTEDS_PutSelectCase(ctx, 3, info->PrecFormat);
    if (err == LTEDS_OK) {
        if (info->PrecFormat == 0) {
            if ((info->DataSetCount == 0) || (info->DataSetCount >= LTEDS_LCARD_INFO_CUR_IMP_DATASET_MAX)) {
                err = LTEDS_ERROR_ENCODE_VALUE;
            } else {
                err =  LTEDS_PutStructArraySize(ctx, 7, info->DataSetCount);
                if (err == LTEDS_OK) {
                    BYTE pt_idx;
                    for (pt_idx = 0; (pt_idx < info->DataSetCount) && (err == LTEDS_OK); pt_idx++) {
                        err = LTEDS_PutConRes(ctx, 15, 0, 1e-6, info->DataSets[pt_idx].SourceCurrent);
                        if (err == LTEDS_OK)
                            err = LTEDS_PutConRes(ctx, 18, 0, 0.1, info->DataSets[pt_idx].OutputImpedance);
                    }
                }
            }
        } else {
            err = LTEDS_ERROR_UNKNOWN_SEL_CASE;
        }
    }
    return err;
}


LTEDSAPI_DllExport(INT) LTEDS_GetLCardTmpltInfoPhaseFreqTable(
        TLTEDS_DECODE_CONTEXT *ctx, TLTEDS_LCARD_INFO_TMPLT_PHASE_FREQ *info) {
    DWORD val;
    INT err;

    err =  LTEDS_GetStructArraySize(ctx, 7, &val);
    if (err == LTEDS_OK) {
        BYTE pt_idx, pt_cnt;
        pt_cnt = val & 0x7F;
        if (info != NULL)
            info->DataSetCount = pt_cnt;
        for (pt_idx = 0; (pt_idx < pt_cnt) && (err == LTEDS_OK); pt_idx++) {
            TLTEDS_LCARD_INFO_PHASE_FREQ_DATASET dset;
            err = LTEDS_GetConRelRes(ctx, 15, 1, 0.000215, &dset.Frequency);
            if (err == LTEDS_OK)
                err = LTEDS_GetConRes(ctx, 19, -180, 0.001, &dset.PhaseShift);
            if ((err == LTEDS_OK) && info)
                info->DataSets[pt_idx] = dset;
        }
    }
    return err;
}

LTEDSAPI_DllExport(INT) LTEDS_PutLCardTmpltInfoPhaseFreqTable(
        TLTEDS_ENCODE_CONTEXT *ctx, const TLTEDS_LCARD_INFO_TMPLT_PHASE_FREQ *info) {
    INT err;

    err =  LTEDS_PutStructArraySize(ctx, 7, info->DataSetCount);
    if (err == LTEDS_OK) {
        BYTE pt_idx;
        for (pt_idx = 0; (pt_idx < info->DataSetCount) && (err == LTEDS_OK); pt_idx++) {
            err = LTEDS_PutConRelRes(ctx, 15, 1, 0.000215, info->DataSets[pt_idx].Frequency);
            if (err == LTEDS_OK)
                err = LTEDS_PutConRes(ctx, 19, -180, 0.001, info->DataSets[pt_idx].PhaseShift);
        }
    }
    return err;

}


