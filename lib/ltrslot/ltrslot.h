/*
 * ltrslotconfig.h
 *
 *  Created on: 15 окт. 2013 г.
 *      Author: konstantin
 */

#ifndef LTRSLOT_H_
#define LTRSLOT_H_


#include <stddef.h>

#include "ltrapi.h"


/*================================================================================================*/
#define LTRSLOT_CONN_INVAL (NULL)

#define LTRSLOT_STATUSFLG_CONFIG (1ul << 1)
#define LTRSLOT_STATUSFLG_RUN (1ul << 31)

#define LTRSLOT_NCONTROLLER_SLOT (0xFFu)

#define LTRSLOT_MAX_INFOSIZE (32)


/*================================================================================================*/
struct LTRSlotConn {
    TLTR h_channel;
    WORD n_slot;
};
typedef struct LTRSlotConn * t_ltrslot_conn;

#pragma pack(4)
struct LTRSlotInfo {
    DWORD status;
    WORD card_id;
    BYTE start_mode;
    BYTE padding[LTRSLOT_MAX_INFOSIZE-7];
};
#pragma pack()


/*================================================================================================*/
INT ltrslot_create_connection(DWORD netaddr, WORD netport, const CHAR serial[], WORD slot,
    t_ltrslot_conn *h_conn);
INT ltrslot_replicate_connection(const TLTR *h_base_conn, t_ltrslot_conn *h_conn);
INT ltrslot_destroy_connection(t_ltrslot_conn h_conn);

INT ltrslot_check_support(t_ltrslot_conn h_conn, int *has_support);
INT ltrslot_check_support_ltr(TLTR *h_ltr, int *has_support);

INT ltrslot_get_info(t_ltrslot_conn h_conn, struct LTRSlotInfo *info);
INT ltrslot_get_config(t_ltrslot_conn h_conn, DWORD sz, void *cfg);

INT ltrslot_put_info(t_ltrslot_conn h_conn, const struct LTRSlotInfo *info);
INT ltrslot_put_config(t_ltrslot_conn h_conn, DWORD sz, const void *cfg);

INT ltrslot_store_config(void *h_card, TLTR *ltr_conn, DWORD cfg_sz, WORD card_id,
    TLTR_CARD_START_MODE start_mode, void (*convert_hcardtocfg)(const void *, void *));
INT ltrslot_update_config(t_ltrslot_conn h_conn, WORD offs, DWORD sz, const void *payload);
INT ltrslot_restore_config(void *h_card, DWORD net_addr, WORD net_port, const CHAR crate_serial[],
    WORD slot, DWORD cfg_sz, void (*convert_to_hcard)(const void *, void *), DWORD *out_flags);

INT ltrslot_save_settings(t_ltrslot_conn h_conn);
INT ltrslot_save_slot_settings(TLTR *h_ltr, WORD nslot);

INT ltrslot_start_wconfig(void *h_card, TLTR *ltr_conn, DWORD cfg_sz, WORD card_id,
    void (*conv_hcardtocfg)(const void *, void *));
INT ltrslot_stop(TLTR *ltr_conn);

INT ltrslot_write(TLTR *h_ltr, WORD nslot, WORD offs, DWORD sz, const void *data);


#endif /* LTRSLOT_H_ */
