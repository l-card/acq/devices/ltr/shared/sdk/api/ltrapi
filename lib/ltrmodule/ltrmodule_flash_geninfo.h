#ifndef LTRMODULE_FLASH_GENINFO_H
#define LTRMODULE_FLASH_GENINFO_H

#include "lwintypes.h"

#define LTR_FLASH_GENINFO_SIGN           0xA55A1919
#define LTR_FLASH_GENINFO_CRC_SIZE       2
#define LTR_FLASH_GENINFO_HDR_SIZE       16


#define LTR_FLASH_GENINFO_NAME_SIZE          8
#define LTR_FLASH_GENINFO_SERIAL_SIZE       16

#define LTR_FLASH_GENINFO_NAME_EX_SIZE      16
#define LTR_FLASH_GENINFO_SERIAL_EX_SIZE    24


typedef struct {
    DWORD sign;
    DWORD size;
    DWORD format;
    DWORD flags;
} t_ltr_flash_geninfo_hdr;

typedef struct {
    CHAR name[LTR_FLASH_GENINFO_NAME_SIZE];
    CHAR serial[LTR_FLASH_GENINFO_SERIAL_SIZE];
} t_ltr_flash_geninfo_module;

typedef struct {
    CHAR name[LTR_FLASH_GENINFO_NAME_EX_SIZE];
    CHAR serial[LTR_FLASH_GENINFO_SERIAL_EX_SIZE];
} t_ltr_flash_geninfo_module_ex;


#endif // LTRMODULE_FLASH_GENINFO_H
