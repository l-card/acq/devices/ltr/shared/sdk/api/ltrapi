#ifndef LTRMODULE_AT93C86A_H
#define LTRMODULE_AT93C86A_H

#include "ltrapi.h"

INT ltr_at93c86a_read(TLTR *ltr, WORD addr, BYTE *data, WORD size);
INT ltr_at93c86a_write(TLTR *ltr, WORD addr, const BYTE *data, WORD size);

#endif // LTRMODULE_AT93C86A_H
