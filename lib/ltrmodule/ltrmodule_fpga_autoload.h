#ifndef LTRMODULE_FPGA_AUTOLOAD_H
#define LTRMODULE_FPGA_AUTOLOAD_H

#include "ltrapi.h"

INT  ltrmodule_fpga_check_load(TLTR *hnd, BYTE *state);
BOOL ltrmodule_fpga_is_enabled(e_LTR_FPGA_STATE state);
INT  ltrmodule_fpga_enable(TLTR *hnd, BOOL en, BYTE *state);

#endif // LTRMODULE_FPGA_AUTOLOAD_H
