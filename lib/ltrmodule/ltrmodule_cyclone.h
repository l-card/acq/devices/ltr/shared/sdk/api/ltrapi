/** @file ltrmodule_cyclone.h

    Вспомогательный файл, использующийся библиотеками модулей, для которых
    нужно загружать прошивку ПЛИС Cyclone III по LTR-интерфейсу.

    Данный файл содержит определение функций для работы с прошивкой Cyclone III */

#ifndef LTR_MODULE_CYCLONE_H__
#define LTR_MODULE_CYCLONE_H__

#include "ltrapi.h"


typedef void (APIENTRY *t_ltr_fpga_load_cb)(void* cb_data, void* hltr, DWORD done_size, DWORD full_size);

/** Данная функция используется для загрузки прошивки ПЛИС Cyclone III
    в формате .rbf в ПЛИС модуля

    @param[in] hnd        Опистель канала связи
    @param[in] filename   Путь к файлу с прошивкой или ресурсу
    @param[in] hInst      Хендл DLL (только для Windows)
    @param[in] defname    Имя, используемое, если filename=NULL или пустая строка.
                          Для Windows - имя ресурса, для Linux - дефолтный путь.
    @param[in] cb         Указатель на вызываемую функцию для индикации прогресса (может быть NULL)
    @param[in] cb_data    Данные, передаваемые в функцию для индикации прогресса (может быть NULL)
    @param[in] hltr       Указатель на описатель самого модуля (структура верхнего уровня), передается в cb
    @return               Код ошибки */
INT ltr_module_fpga_load(TLTR* hnd, const char* filename, HINSTANCE hInst, const char* defname, t_ltr_fpga_load_cb cb, void* cb_data, void *hltr);

/** Разрешение или запрещение работы ПЛИС. Если ПЛИС разрешен, то он
    может обрабатывать команды и высылать ответы. Если запрещен - то
    обычно команды обрабатывает спецальный CPLD.

    @param[in] hnd        Опистель канала связи
    @param[in] en         0 - запрет работы ПЛИС, иначе - разрешение
    @return               Код ошибки */
INT ltr_module_fpga_enable(TLTR *hnd, INT en);

/** Проверка, загружен ли ПЛИС. Если нет => возвращает код
    #LTR_ERROR_FPGA_IS_NOT_LOADED.

    @param[in] hnd        Опистель канала связи
    @return               Код ошибки */
INT ltr_module_fpga_is_loaded(TLTR *hnd);

#endif
