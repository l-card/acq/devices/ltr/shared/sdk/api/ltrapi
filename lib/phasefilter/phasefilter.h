#ifndef PHASEFILTER_H
#define PHASEFILTER_H

typedef struct {
    int is_valid;
    double a;
    double b0, b1;
    double prev_x;
    double prev_y;    
    int pev_valid;
} t_phasefilter_state;

double phasefilter_calc_c(double r1, double r2, double phi, double f0, double fd);
double phasefilter_calc_phi(double r1, double r2, double c, double f0, double fd);

void phasefilter_init(t_phasefilter_state *state, double r1, double r2,  double phi, double f0, double fd);
void phasefilter_reset(t_phasefilter_state *state);
double phasefilter_process_point(t_phasefilter_state *state, double x);


#endif // PHASEFILTER_H
