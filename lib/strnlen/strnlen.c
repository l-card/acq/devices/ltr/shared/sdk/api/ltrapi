#include <config.h>

#if !defined (HAVE_STRNLEN)

#include <string.h>

size_t strnlen(const char *s, size_t maxlen)
{
  register const char *e;
  size_t n;

  for (e = s, n = 0; *e && n < maxlen; e++, n++)
  {}

  return n;
}
#endif
