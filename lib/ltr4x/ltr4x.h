#ifndef LTR_4X_H_
#define LTR_4X_H_


#include "ltrapi.h"
#include "ltr4x_cfg.h"

#define LTR4X_START_PULSE_CFG_VER          0x200


#define PARAM_RS485_RESPONSE_TOUT_L                   0x1
#define PARAM_RS485_RESPONSE_TOUT_H                   0x2
#define PARAM_RS485_INTERVAL_TOUT                     0x3
#define PARAM_RS485_TX_ACTIVE_SOP_INTERVAL            0x4
#define PARAM_RS485_TX_ACTIVE_EOP_INTERVAL            0x5
#define PARAM_START_MARK_SCALER                       0x6
#define PARAM_START_MARK_CNTR_L                       0x7
#define PARAM_START_MARK_CNTR_H                       0x8
#define PARAM_STACK_SIZE                              0xFF


/*================================================================================================*/
INT LTR4X_Open(TLTR *h_ltr, INT net_addr, WORD net_port, const CHAR *crate_sn, INT slot_num,
    TLTR4X_MODULE_INFO* info, WORD* ver);
INT LTR4X_OpenEx(TLTR *h_ltr, INT net_addr, WORD net_port, const CHAR *crate_sn, INT slot_num,
    DWORD mflags, TLTR4X_MODULE_INFO *info, WORD *ver);
INT LTR4X_ReadConfigRecord(TLTR *hnd, CHAR *version, CHAR *date, CHAR *name, CHAR *serial,
    WORD *firm_ver);
INT LTR4X_GetConfig(TLTR *h_ltr, TLTR4X_MODULE_INFO *info, WORD *ver);
INT LTR4X_CheckAck(DWORD ack, DWORD cmd);
INT LTR4X_SendCmdWithAck(TLTR *hnd, const DWORD *cmd, DWORD *ack, DWORD size);
INT LTR4X_EvalScales(double *Freq, WORD *Prescaler, BYTE *PrescalerCode, BYTE *Scaler);
#ifdef STOP_STREAM_READ
    INT LTR4X_StopStreamRead(TLTR *hnd);
#endif
INT LTR4X_WriteEEPROM(TLTR *hnd, INT Address, BYTE val);
INT LTR4X_ReadEEPROM(TLTR *hnd, INT Address, BYTE *val);
INT LTR4X_SetStartMarkPulseTime(TLTR *hnd, DWORD time_mks, const char *ver_str);


#endif
