###############################################################################
# Данный файл является основой для создания файлов проектов для всех библиотек
#   из ltrapi
# Проект библиотеки должен установить нужные переменные и включить данный файл.
# По-умолчанию подразумевается, что библиотека должна включать файлы
# <имя библиотеки>.c, <имя библиотеки>.h (устанавливаемый заголовок) и
# <имя библиотеки>.def (используется только в Win x86), ltrmodule.c, а также
# линкуется с библиотекой ltrapi
#
#
# Возможные переменные для установки:
#    LTRAPI_MODULE_NAME                 - название модуля (для библиотек для работы с модулями)
#    LTRAPI_MODULE_PROJECT              - название проекта (при наличии LTRAPI_MODULE_NAME
#                                           используется имя с добавлением api)
#    LTRAPI_MODULE_SETUP_HEADERS        - список заголовков для установки, помимо
#                                           соответствующего названию библиотеки
#    LTRAPI_MODULE_HEADERS              - список дополнительных приватных заголовков
#    LTRAPI_MODULE_SOURCES              - список дополнительных исходников (без
#                                           исходника с названием соответствующим названию библиотеки)
#    LTRAPI_MODULE_LIBS                 - дополнительные библиотеки для подключения
#                                           (помимо ltrapi)
#    LTRAPI_MODULE_INSTALL_DATA_FILES   - файлы данных для установки (как правило
#                                           прошивки для модулей)
#    LTRAPI_MODULE_BUILD_DEPENDENCIES   - список дополнительных зависимостей при
#                                           сборке
#    LTRAPI_COMPILE_DEFINITIONS         - дополнительные определения компилятора
#    LTRAPI_MODULE_NO_STD_SRC           - признак, что не нужно включать файлы <имя библиотеки>.c,
#                                           <имя библиотеки>.h и <имя библиотеки>.def
#    LTRAPI_MODULE_NO_LTRAPI            - библиотека не использует ltrapi
#    LTRAPI_MODULE_USE_MATH             - модуль использует стандартную математическую
#                                           библиотеку
#    LTRAPI_MODULE_USE_CRC              - использование файлов с реализацией CRC16
#    LTRAPI_MODULE_USE_TIMER            - использование таймера
#    LTRAPI_MODULE_USE_CYCLONE          - включение кода для загрузки ПЛИС Cyclone
#    LTRAPI_MODULE_USE_FLASH_GENINFO    - включение кода определения нового общего формата информации во flash-памяти
#    LTRAPI_MODULE_USE_FLASH_AT25DF     - включение кода для работы с флеш-памятью AT25DF
#    LTRAPI_MODULE_USE_FLASH_X25        - включение кода для работы с семейством микросхем памяти x25 (AT25SF и китайские аналоги)
#    LTRAPI_MODULE_USE_FLASH_AT45DB     - включение кода для работы с флеш-памятью AT45DB
#    LTRAPI_MODULE_USE_FLASH_SST25      - включение кода для работы с флеш-памятью SST25
#    LTRAPI_MODULE_USE_FLASH_SST26      - включение кода для работы с флеш-памятью SST26
#    LTRAPI_MODULE_USE_FLASH_AT93C86A   - включение кода для работы с флеш-памятью AT93C86A
#    LTRAPI_MODULE_USE_DOXYGEN_DOC      - создание цели для генерации документации
#                                           с помощью doxygen
#    LTRAPI_MODULE_DOC_ABOUT_FILE       - Использование указанного своего файла
#                                           с описанием введения (о чем документ),
#                                           вместо стандартной
#    LTRAPI_MODULE_DOC_INPUT_FILES      - файлы модуля для генерации документации
#    LTRAPI_MODULE_USE_KD_STORESLOTS            - модуль поддерживает сохранение/восстановление
#                                           настроек слота в крейте
#    LTRAPI_MODULE_EXPORT_SYMBOL        - переопределение символа для включение
#                                           слава для экспорта при компиляции библиотеки
#                                           (только Windows)
#    LTRAPI_MODULE_DISABLE_BORLAND      - запрет генерации библиотек Borland для данного
#                                           модуля (даже при глобальном разрешении)
#    LTRAPI_MODULE_DESCR_ENG            - описание библиотеки на английском (для файла
#                                           ресурсов в Windows)
#    LTRAPI_MODULE_DESCR_RUS            - описание библиотеки на русском (для файла
#                                           ресурсов в Windows)
#    LTRAPI_MODULE_LINKER_FLAGS         - дополнительные флаги линкера
################################################################################

cmake_policy(PUSH)

cmake_minimum_required(VERSION 2.8.12)

#название проекта должно быть определено явно, либо берется из названия модуля
if(NOT LTRAPI_MODULE_PROJECT AND NOT LTRAPI_MODULE_NAME)
    message(FATAL_ERROR "LTRAPI_MODULE_PROJECT or LTRAPI_MODULE_NAME must be specified!")
endif(NOT LTRAPI_MODULE_PROJECT AND NOT LTRAPI_MODULE_NAME)



#стандартно название библиотеки получается просто добавлением api к названию модуля
if(NOT LTRAPI_MODULE_PROJECT)
    string(TOLOWER ${LTRAPI_MODULE_NAME}api LTRAPI_MODULE_PROJECT)
endif(NOT LTRAPI_MODULE_PROJECT)

string(TOUPPER "${LTRAPI_MODULE_PROJECT}" LTRAPI_MODULE_PROJECT_UPPER)

project(${LTRAPI_MODULE_PROJECT} C)


message(STATUS "configure module project ${LTRAPI_MODULE_PROJECT}, module name = ${LTRAPI_MODULE_NAME}")


set(LTRAPI_COMPILE_DEFINITIONS ${LTRAPI_COMPILE_DEFINITIONS} HAVE_CONFIG_H)

#получение названия определения для определения функции (обычно LTRXXXAPI_DllExport")
if(NOT LTRAPI_MODULE_FUNC_DECLARATION)
    set(LTRAPI_MODULE_FUNC_DECLARATION "${LTRAPI_MODULE_PROJECT_UPPER}_DllExport")
endif(NOT LTRAPI_MODULE_FUNC_DECLARATION)

if(LTRAPI_MODULE_NAME)
    string(TOLOWER ${LTRAPI_MODULE_NAME} LTRAPI_MODULE_INSTALL_DATA_DIR)
    set(LTRAPI_COMPILE_DEFINITIONS ${LTRAPI_COMPILE_DEFINITIONS} LTRMODULE_INSTALL_DATA_FILE="${CMAKE_INSTALL_PREFIX}/${LTRAPI_INSTALL_DATA}/${LTRAPI_MODULE_INSTALL_DATA_DIR}/${LTRAPI_MODULE_INSTALL_DATA_FILES}")
endif(LTRAPI_MODULE_NAME)

configure_file(${LTRAPI_CONFIG_FILE} ${CMAKE_CURRENT_BINARY_DIR}/config.h)


if(NOT LTRAPI_MODULE_DESCR_ENG)
    set(LTRAPI_MODULE_DESCR_ENG "Interface library for module ${LTRAPI_MODULE_NAME}")
endif(NOT LTRAPI_MODULE_DESCR_ENG)
if(NOT LTRAPI_MODULE_DESCR_RUS)
    set(LTRAPI_MODULE_DESCR_RUS "Библиотека функций работы с модулем ${LTRAPI_MODULE_NAME}")
endif(NOT LTRAPI_MODULE_DESCR_RUS)


#для Windows конфигурируем файл ресурсов с версией и описанием библиотеки
if(WIN32)
    configure_file(${LTRAPI_LIB_DIR}/ltrmodule/ltrmodule.rc.in ${CMAKE_CURRENT_BINARY_DIR}/${LTRAPI_MODULE_PROJECT}.rc)
    set(LTRAPI_MODULE_SOURCES ${LTRAPI_MODULE_SOURCES} ${CMAKE_CURRENT_BINARY_DIR}/${LTRAPI_MODULE_PROJECT}.rc)
endif(WIN32)

#включаем стандартные пути с заголовками
include_directories(${LTRAPI_SOURCE_DIR} ${LTRAPI_SOURCE_DIR}/ltrapi ${LTRAPI_LIB_DIR}/ltrmodule
                    ${LTRAPI_LIB_DIR}/lbitfield ${CMAKE_CURRENT_BINARY_DIR} ${LTRAPI_INCLUDE_DIR})

set(LTRAPI_MODULE_HEADERS ${LTRAPI_MODULE_HEADERS} ${LTRAPI_LIB_DIR}/lbitfield/lbitfield.h)

#включаем стандартные заголовки и файлы исходников
if(NOT LTRAPI_MODULE_NO_STD_SRC)
    # таймер используется в стандартных функциях для открытия модуля и останова сбора,
    # поэтому включаем его всегда, если не запрещены стандартные файлы
    set(LTRAPI_MODULE_USE_TIMER ON)

    set(LTRAPI_MODULE_SETUP_HEADERS ${LTRAPI_MODULE_PROJECT}.h ${LTRAPI_MODULE_SETUP_HEADERS})

    set(LTRAPI_MODULE_SOURCES ${LTRAPI_MODULE_SOURCES} ${LTRAPI_MODULE_PROJECT}.c)

    if(MSVC)
        #def-файл используем только на 32-битной платформе, так как на 64-битной
        #уже при _dllexport имена без подчеркиваний
        if( CMAKE_SIZEOF_VOID_P EQUAL 4 )
             set(LTRAPI_MODULE_SOURCES ${LTRAPI_MODULE_SOURCES} ${PROJECT_SOURCE_DIR}/${LTRAPI_MODULE_PROJECT}.def)
        endif( CMAKE_SIZEOF_VOID_P EQUAL 4 )
    endif(MSVC)
endif()

if(NOT LTRAPI_MODULE_NO_STD_FUNC)
    set(LTRAPI_MODULE_SOURCES ${LTRAPI_MODULE_SOURCES} ${LTRAPI_LIB_DIR}/ltrmodule/ltrmodule.c)
    set(LTRAPI_MODULE_HEADERS ${LTRAPI_MODULE_HEADERS} ${LTRAPI_LIB_DIR}/ltrmodule/ltrmodule.h)
endif(NOT LTRAPI_MODULE_NO_STD_FUNC)

if(NOT LTRAPI_MODULE_NO_LTRAPI)
    set(LTRAPI_MODULE_LIBS    ${LTRAPI_MODULE_LIBS} ltrapi)
endif(NOT LTRAPI_MODULE_NO_LTRAPI)

if(LTRAPI_MODULE_REQ_MODULES)
    foreach(LTRAPI_MODULE_REQ_MODULE ${LTRAPI_MODULE_REQ_MODULES})
        include_directories(${LTRAPI_SOURCE_DIR}/${LTRAPI_MODULE_REQ_MODULE})
        set(LTRAPI_MODULE_LIBS ${LTRAPI_MODULE_LIBS} ${LTRAPI_MODULE_REQ_MODULE})
    endforeach()
endif(LTRAPI_MODULE_REQ_MODULES)


if(NOT HAVE_STRLEN)
    set(LTRAPI_MODULE_SOURCES ${LTRAPI_MODULE_SOURCES} ${LTRAPI_LIB_DIR}/strnlen/strnlen.c)
endif(NOT HAVE_STRLEN)

if(NOT LTRAPI_HAVE_STDINT_H)
    include_directories(${LTRAPI_LIB_DIR}/stdint)
endif(NOT LTRAPI_HAVE_STDINT_H)

#---------- Анализ опций и добавление дополнительных путей ------------------

#подключение стандартной математической библиотеки
if(LTRAPI_MODULE_USE_MATH)
    if(CMAKE_COMPILER_IS_GNUCC OR CMAKE_C_COMPILER MATCHES "clang")
        set(LTRAPI_MODULE_LIBS ${LTRAPI_MODULE_LIBS} m)
    endif(CMAKE_COMPILER_IS_GNUCC OR CMAKE_C_COMPILER MATCHES "clang")
endif(LTRAPI_MODULE_USE_MATH)

#файлы для рассчета CRC16
if(LTRAPI_MODULE_USE_CRC)
    set(LTRAPI_MODULE_SOURCES ${LTRAPI_MODULE_SOURCES} ${LTRAPI_LIB_DIR}/crc/crc.c)
    set(LTRAPI_MODULE_HEADERS ${LTRAPI_MODULE_HEADERS} ${LTRAPI_LIB_DIR}/crc/crc.h ${LTRAPI_LIB_DIR}/ltrmodule/crclib_cfg.h)
    include_directories(${LTRAPI_LIB_DIR}/crc ${PROJECT_SOURCE_DIR})
endif(LTRAPI_MODULE_USE_CRC)

if(LTRAPI_MODULE_USE_KD_STORESLOTS AND LTRAPI_USE_KD_STORESLOTS)
  set(LTRAPI_MODULE_SOURCES ${LTRAPI_MODULE_SOURCES} ${LTRAPI_LIB_DIR}/ltrslot/ltrslot.c)
  set(LTRAPI_MODULE_HEADERS ${LTRAPI_MODULE_HEADERS} ${LTRAPI_LIB_DIR}/ltrslot/ltrslot.h)
  include_directories(${LTRAPI_LIB_DIR}/ltrslot)
endif(LTRAPI_MODULE_USE_KD_STORESLOTS AND LTRAPI_USE_KD_STORESLOTS)

if(LTRAPI_MODULE_USE_TIMER)
    set(LTIMER_DIR       ${LTRAPI_LIB_DIR}/ltimer)
    set(LCSPEC_DIR       ${LTRAPI_LIB_DIR}/lcspec)
    include(${LTIMER_DIR}/ltimer.cmake)
    set(LTRAPI_MODULE_SOURCES ${LTRAPI_MODULE_SOURCES} ${LTIMER_SOURCES})
    set(LTRAPI_MODULE_HEADERS ${LTRAPI_MODULE_HEADERS} ${LTIMER_HEADERS})
    set(LTRAPI_MODULE_LIBS    ${LTRAPI_MODULE_LIBS}    ${LTIMER_LIBS})
endif(LTRAPI_MODULE_USE_TIMER)

#файлы для загрузки прошивки ПЛИС Cyclone
if(LTRAPI_MODULE_USE_CYCLONE)
    set(LTRAPI_MODULE_SOURCES ${LTRAPI_MODULE_SOURCES} ${LTRAPI_LIB_DIR}/ltrmodule/ltrmodule_cyclone.c)
endif(LTRAPI_MODULE_USE_CYCLONE)

#файлы для управления ПЛИС, загружающейся автоматически железом
if(LTRAPI_MODULE_USE_FPGA_AUTOLOAD)
    set(LTRAPI_MODULE_SOURCES ${LTRAPI_MODULE_SOURCES} ${LTRAPI_LIB_DIR}/ltrmodule/ltrmodule_fpga_autoload.c)
    set(LTRAPI_MODULE_HEADERS ${LTRAPI_MODULE_HEADERS} ${LTRAPI_LIB_DIR}/ltrmodule/ltrmodule_fpga_autoload.h)
endif(LTRAPI_MODULE_USE_FPGA_AUTOLOAD)

if(LTRAPI_MODULE_USE_PHASE_FILTER)
    set(LTRAPI_MODULE_SOURCES ${LTRAPI_MODULE_SOURCES} ${LTRAPI_LIB_DIR}/phasefilter/phasefilter.c)
    set(LTRAPI_MODULE_HEADERS ${LTRAPI_MODULE_HEADERS} ${LTRAPI_LIB_DIR}/phasefilter/phasefilter.h)    
    include_directories(${LTRAPI_LIB_DIR}/phasefilter)
endif(LTRAPI_MODULE_USE_PHASE_FILTER)


if(LTRAPI_MODULE_USE_FLASH_GENINFO)
    set(LTRAPI_MODULE_HEADERS ${LTRAPI_MODULE_HEADERS} ${LTRAPI_LIB_DIR}/ltrmodule/ltrmodule_flash_geninfo.h)
endif(LTRAPI_MODULE_USE_FLASH_GENINFO)

if(LTRAPI_MODULE_USE_FLASH_AT25DF)
    set(LTRAPI_MODULE_USE_FLAH ON)
    set(LTRAPI_MODULE_SOURCES ${LTRAPI_MODULE_SOURCES} ${LTRAPI_LIB_DIR}/flash/devices/flash_dev_at25df.c)
    set(LTRAPI_MODULE_HEADERS ${LTRAPI_MODULE_HEADERS} ${LTRAPI_LIB_DIR}/flash/devices/flash_dev_at25df.h)
endif(LTRAPI_MODULE_USE_FLASH_AT25DF)

if(LTRAPI_MODULE_USE_FLASH_X25)
    set(LTRAPI_MODULE_USE_FLAH ON)    
    set(LTRAPI_MODULE_SOURCES ${LTRAPI_MODULE_SOURCES}
        ${LTRAPI_LIB_DIR}/flash/devices/x25/flash_dev_x25.c
        ${LTRAPI_LIB_DIR}/flash/devices/x25/flash_dev_at25sf.c
        ${LTRAPI_LIB_DIR}/flash/devices/x25/flash_dev_p25q40sh.c
    )
    set(LTRAPI_MODULE_HEADERS ${LTRAPI_MODULE_HEADERS}
        ${LTRAPI_LIB_DIR}/flash/devices/x25/flash_dev_x25.h
        ${LTRAPI_LIB_DIR}/flash/devices/x25/flash_dev_at25sf.h
        ${LTRAPI_LIB_DIR}/flash/devices/x25/flash_dev_p25q40sh.h
    )
endif(LTRAPI_MODULE_USE_FLASH_X25)

if(LTRAPI_MODULE_USE_FLASH_AT45DB)
    set(LTRAPI_MODULE_USE_FLAH ON)
    set(LTRAPI_MODULE_SOURCES ${LTRAPI_MODULE_SOURCES} ${LTRAPI_LIB_DIR}/flash/devices/flash_dev_at45db.c)
    set(LTRAPI_MODULE_HEADERS ${LTRAPI_MODULE_HEADERS} ${LTRAPI_LIB_DIR}/flash/devices/flash_dev_at45db.h)
endif(LTRAPI_MODULE_USE_FLASH_AT45DB)

if(LTRAPI_MODULE_USE_FLASH_SST25)
    set(LTRAPI_MODULE_USE_FLAH ON)
    set(LTRAPI_MODULE_SOURCES ${LTRAPI_MODULE_SOURCES} ${LTRAPI_LIB_DIR}/flash/devices/flash_dev_sst25.c)
    set(LTRAPI_MODULE_HEADERS ${LTRAPI_MODULE_HEADERS} ${LTRAPI_LIB_DIR}/flash/devices/flash_dev_sst25.h)
endif(LTRAPI_MODULE_USE_FLASH_SST25)

if(LTRAPI_MODULE_USE_FLASH_SST26)
    set(LTRAPI_MODULE_USE_FLAH ON)
    set(LTRAPI_MODULE_SOURCES ${LTRAPI_MODULE_SOURCES} ${LTRAPI_LIB_DIR}/flash/devices/flash_dev_sst26.c)
    set(LTRAPI_MODULE_HEADERS ${LTRAPI_MODULE_HEADERS} ${LTRAPI_LIB_DIR}/flash/devices/flash_dev_sst26.h)
endif(LTRAPI_MODULE_USE_FLASH_SST26)

if(LTRAPI_MODULE_USE_FLAH)
    set(LTRAPI_MODULE_SOURCES ${LTRAPI_MODULE_SOURCES} ${LTRAPI_LIB_DIR}/flash/ports/ltr/flash_iface_ltr.c ${LTRAPI_LIB_DIR}/flash/flash.c ${LTRAPI_LIB_DIR}/flash/flash_sendrecv.c)
    set(LTRAPI_MODULE_HEADERS ${LTRAPI_MODULE_HEADERS} ${LTRAPI_LIB_DIR}/flash/ports/ltr/flash_iface_ltr.h ${LTRAPI_LIB_DIR}/flash/flash.h ${LTRAPI_LIB_DIR}/flash/flash_sendrecv.h)
    include_directories(${LTRAPI_LIB_DIR}/flash)
endif(LTRAPI_MODULE_USE_FLAH)

if(LTRAPI_MODULE_USE_FLASH_AT93C86A)
    set(LTRAPI_MODULE_SOURCES ${LTRAPI_MODULE_SOURCES} ${LTRAPI_LIB_DIR}/ltrmodule/ltrmodule_at93c86a.c)
    set(LTRAPI_MODULE_HEADERS ${LTRAPI_MODULE_HEADERS} ${LTRAPI_LIB_DIR}/ltrmodule/ltrmodule_at93c86a.h)
endif(LTRAPI_MODULE_USE_FLASH_AT93C86A)




#добавляем доп. зависимости сборки, если были указаны
if(LTRAPI_MODULE_BUILD_DEPENDENCIES)
    set_source_files_properties(${LTRAPI_MODULE_SOURCES} PROPERTIES OBJECT_DEPENDS ${LTRAPI_MODULE_BUILD_DEPENDENCIES})
endif(LTRAPI_MODULE_BUILD_DEPENDENCIES)


# Собираем динамическую или статическую библиотеку, в зависимости от настроек
if(LTRAPI_BUILD_STATIC)
    add_library(${LTRAPI_MODULE_PROJECT} STATIC ${LTRAPI_MODULE_HEADERS} ${LTRAPI_MODULE_SETUP_HEADERS} ${LTRAPI_MODULE_SOURCES})
else(LTRAPI_BUILD_STATIC)
    add_library(${LTRAPI_MODULE_PROJECT} SHARED ${LTRAPI_MODULE_HEADERS} ${LTRAPI_MODULE_SETUP_HEADERS} ${LTRAPI_MODULE_SOURCES})
endif(LTRAPI_BUILD_STATIC)

# устанавливаем версии (действует только для .so, для .dll нужно через ресурсы
set_target_properties(${LTRAPI_MODULE_PROJECT} PROPERTIES VERSION ${LTRAPI_VERSION})
set_target_properties(${LTRAPI_MODULE_PROJECT} PROPERTIES SOVERSION 1)




# добавляем дополнительные определения
set_target_properties(${LTRAPI_MODULE_PROJECT} PROPERTIES COMPILE_DEFINITIONS "${LTRAPI_COMPILE_DEFINITIONS}")

# для Windows нежно определение, чтобы понять, собираем ли мы библиотеку или используем
# заголовок в другой программе, чтобы менять объявления dllexport/dllimport
if(WIN32)
    if(NOT LTRAPI_MODULE_EXPORT_SYMBOL)
        string(TOUPPER "${LTRAPI_MODULE_PROJECT}_EXPORTS" LTRAPI_MODULE_EXPORT_SYMBOL)
    endif(NOT LTRAPI_MODULE_EXPORT_SYMBOL)
    set_target_properties(${LTRAPI_MODULE_PROJECT} PROPERTIES DEFINE_SYMBOL ${LTRAPI_MODULE_EXPORT_SYMBOL})
endif(WIN32)

if(LTRAPI_BUILD_BORLAND_LIBS AND NOT LTRAPI_MODULE_DISABLE_BORLAND)
    #создание цели для создания библиотеки для borland C++/C++ Builder

    set(LTRAPI_MODULE_BORLAND  ${LTRAPI_MODULE_PROJECT}_borland)
    set(LTRAPI_BORLAND_LIBS ${LTRAPI_MODULE_BORLAND} ${LTRAPI_BORLAND_LIBS} PARENT_SCOPE)
    #создаем lib-файл для Borland C++ (лучше использовать последнюю версию,
    #так как lib созданный с помощью Borland Builder 6 может не подойти к Embarcadero)

    if( CMAKE_SIZEOF_VOID_P EQUAL 4 )
        #для 32-битных библиотек используем implib и файл с расширением .lib
        set(LTRAPI_MODULE_BORLAND_EXTENSION lib)
        set(LTRAPI_MODULE_BORLAND_LIBFILE   ${LTRAPI_MODULE_BORLAND}.${LTRAPI_MODULE_BORLAND_EXTENSION})
        add_custom_command(OUTPUT ${LTRAPI_MODULE_BORLAND_LIBFILE} ALL
            COMMAND "${BORLAND_IMPLIB_EXEC}" -f ${LTRAPI_MODULE_BORLAND} ${LTRAPI_MODULE_PROJECT}.dll
            DEPENDS ${LTRAPI_MODULE_PROJECT}
            COMMENT "make borland library for ${LTRAPI_MODULE_PROJECT}")
    else( CMAKE_SIZEOF_VOID_P EQUAL 4 )
        #для 64-битных библиотек используем mkexp и файл с расширением .a
        set(LTRAPI_MODULE_BORLAND_EXTENSION a)
        set(LTRAPI_MODULE_BORLAND_LIBFILE   ${LTRAPI_MODULE_BORLAND}.${LTRAPI_MODULE_BORLAND_EXTENSION})
        add_custom_command(OUTPUT ${LTRAPI_MODULE_BORLAND_LIBFILE} ALL
            COMMAND "${BORLAND_MKEXP_EXEC}" ${LTRAPI_MODULE_BORLAND_LIBFILE} ${LTRAPI_MODULE_PROJECT}.dll
            DEPENDS ${LTRAPI_MODULE_PROJECT}
            COMMENT "make 64-bit borland library for ${LTRAPI_MODULE_PROJECT}")
    endif( CMAKE_SIZEOF_VOID_P EQUAL 4 )

    add_custom_target(${LTRAPI_MODULE_BORLAND} ALL
                    DEPENDS ${LTRAPI_MODULE_BORLAND_LIBFILE})
    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${LTRAPI_MODULE_BORLAND_LIBFILE}
            DESTINATION ${LTRAPI_INSTALL_LIB}/borland
            RENAME ${LTRAPI_MODULE_PROJECT}.${LTRAPI_MODULE_BORLAND_EXTENSION})
endif(LTRAPI_BUILD_BORLAND_LIBS AND NOT LTRAPI_MODULE_DISABLE_BORLAND)

if(LTRAPI_BUILD_MINGW_LIBS)
    set(LTRAPI_MODULE_MINGW           ${LTRAPI_MODULE_PROJECT}_mingw)
    set(LTRAPI_MODULE_MINGW_LIBFILE   ${LTRAPI_MODULE_MINGW}.a)

    set(LTRAPI_MINGW_LIBS ${LTRAPI_MODULE_MINGW} ${LTRAPI_MINGW_LIBS} PARENT_SCOPE)

    if( CMAKE_SIZEOF_VOID_P EQUAL 4 )
        set(LTRAPI_MODULE_MINGW_MACHINE  i386)
    else( CMAKE_SIZEOF_VOID_P EQUAL 4 )
        set(LTRAPI_MODULE_MINGW_MACHINE  i386:x86-64)
    endif( CMAKE_SIZEOF_VOID_P EQUAL 4 )

    if(LTRAPI_MODULE_MINGW_MANUAL_DEF AND ( CMAKE_SIZEOF_VOID_P EQUAL 4))
        set(LTRAPI_MODULE_MINGW_DEF ${LTRAPI_MODULE_MINGW_MANUAL_DEF})
        message("${LTRAPI_MODULE_PROJECT} custom def: ${LTRAPI_MODULE_MINGW_DEF}")
    else()
        set(LTRAPI_MODULE_MINGW_DEF ${LTRAPI_MODULE_MINGW}.def)
        add_custom_command(OUTPUT ${LTRAPI_MODULE_MINGW_DEF}
            COMMAND "${GENDEF_EXEC}" - -a  ${LTRAPI_MODULE_PROJECT}.dll > ${LTRAPI_MODULE_MINGW}_tmp.def
            #удаляем дубликат экспортируемых функций с префиксами, оставленных для совместимости в старых библиотеках
            COMMAND "${SED_EXEC}" '/^_LTR/d' ${LTRAPI_MODULE_MINGW}_tmp.def > ${LTRAPI_MODULE_MINGW_DEF}
            DEPENDS ${LTRAPI_MODULE_PROJECT}
            COMMENT "make mingw def for ${LTRAPI_MODULE_PROJECT}")

        message("${LTRAPI_MODULE_PROJECT} default def: ${LTRAPI_MODULE_MINGW_DEF}")
    endif()

    add_custom_command(OUTPUT ${LTRAPI_MODULE_MINGW_LIBFILE} ALL
        COMMAND "${DLLTOOL_EXEC}" -m ${LTRAPI_MODULE_MINGW_MACHINE} -l ${LTRAPI_MODULE_MINGW_LIBFILE} -k -d ${LTRAPI_MODULE_MINGW_DEF}
        DEPENDS ${LTRAPI_MODULE_MINGW_DEF}
        COMMENT "make mingw library for ${LTRAPI_MODULE_PROJECT}")
    add_custom_target(${LTRAPI_MODULE_MINGW} ALL
                    DEPENDS ${LTRAPI_MODULE_MINGW_LIBFILE})
    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${LTRAPI_MODULE_MINGW_LIBFILE}
            DESTINATION ${LTRAPI_INSTALL_LIB}/mingw
            RENAME lib${LTRAPI_MODULE_PROJECT}.a)
endif(LTRAPI_BUILD_MINGW_LIBS)

# Для GCC делаем символы по-умолчанию не видимыми, чтобы не экспортировать
# внутренние функции
if(CMAKE_COMPILER_IS_GNUCC)
    set_target_properties(${LTRAPI_MODULE_PROJECT} PROPERTIES COMPILE_FLAGS "-fvisibility=hidden")
endif(CMAKE_COMPILER_IS_GNUCC)

if(LTRAPI_MODULE_LINKER_FLAGS)
    set_target_properties(${LTRAPI_MODULE_PROJECT} PROPERTIES LINK_FLAGS ${LTRAPI_MODULE_LINKER_FLAGS})
endif(LTRAPI_MODULE_LINKER_FLAGS)

# подключение необходимых библиотек
target_link_libraries(${LTRAPI_MODULE_PROJECT} ${LTRAPI_MODULE_LIBS})


###################  установка необходимых файлов ##############################

if(NOT LTRAPI_MODULE_DISABLE_INSTALL)
    if(WIN32)
        #для Windows устанавливаем .lib файл отдельно от .dll в поддиректорию msvc
        install(TARGETS ${LTRAPI_MODULE_PROJECT} RUNTIME DESTINATION ${LTRAPI_INSTALL_LIB})
        install(TARGETS ${LTRAPI_MODULE_PROJECT} ARCHIVE DESTINATION ${LTRAPI_INSTALL_LIB}/msvc)
    else(WIN32)
        install(TARGETS ${LTRAPI_MODULE_PROJECT} DESTINATION ${LTRAPI_INSTALL_LIB})
    endif(WIN32)
    install(FILES ${LTRAPI_MODULE_SETUP_HEADERS} DESTINATION ${LTRAPI_INSTALL_INCLUDE})


    if(LTRAPI_MODULE_INSTALL_DATA_FILES)
        install(FILES ${LTRAPI_MODULE_INSTALL_DATA_FILES}
                DESTINATION ${LTRAPI_INSTALL_DATA}/${LTRAPI_MODULE_INSTALL_DATA_DIR})
    endif(LTRAPI_MODULE_INSTALL_DATA_FILES)

    #установка файлов Pascal/Delphi из поддиректории pascal
    if(LTRAPI_INSTALL_PASCAL)
        if(IS_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/pascal)
            install(DIRECTORY pascal/ DESTINATION ${LTRAPI_INSTALL_PASCAL} FILES_MATCHING PATTERN "*.pas")
        endif(IS_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/pascal)
    endif(LTRAPI_INSTALL_PASCAL)


    if(LTRAPI_PREPARE_HEADERS)
        foreach(PREP_FILE ${LTRAPI_MODULE_SETUP_HEADERS})
            get_filename_component(PREP_FILENAME ${PREP_FILE} NAME)
            add_custom_command(TARGET ${LTRAPI_MODULE_PROJECT}
                               POST_BUILD
                               COMMAND cmake -E copy "${PREP_FILE}" "${LTRAPI_INCLUDE_DIR}/ltr/include/${PREP_FILENAME}"
                               WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
                              )
        endforeach()
    endif(LTRAPI_PREPARE_HEADERS)
endif(NOT LTRAPI_MODULE_DISABLE_INSTALL)



#цели для сборки документации
if(LTRAPI_MODULE_USE_DOXYGEN_DOC AND LTR_BUILD_DOC)
    if(NOT LTRAPI_MODULE_DOC_ABOUT_FILE)
        set(LTRAPI_MODULE_DOC_ABOUT_FILE ${LTRAPI_LIB_DIR}/ltrmodule/doc/about.md.in)
    endif(NOT LTRAPI_MODULE_DOC_ABOUT_FILE)

    configure_file(${LTRAPI_MODULE_DOC_ABOUT_FILE} ${CMAKE_CURRENT_BINARY_DIR}/doc/about.md)
    configure_file(${LTRAPI_LIB_DIR}/ltrmodule/doc/setup.md.in ${CMAKE_CURRENT_BINARY_DIR}/doc/setup.md)
    set(DOC_DOXYGEN_PRJ_FILE ${LTRAPI_LIB_DIR}/ltrmodule/doc/Doxyfile.in)
    include(${LTR_DOCGEN_DIR}/LDoxyToPdf.cmake)
    set(DOXYGEN_INPUT_FILES ${LTRAPI_MODULE_PROJECT}.h ${LTRAPI_MODULE_DOC_INPUT_FILES}
                            ${CMAKE_CURRENT_BINARY_DIR}/doc/about.md
                            ${CMAKE_CURRENT_BINARY_DIR}/doc/setup.md)
    ADD_DOXY_TO_PDF_TARGET(${LTRAPI_MODULE_PROJECT})
endif()


source_group("Sources" FILES  ${LTRAPI_MODULE_HEADERS} ${LTRAPI_MODULE_SETUP_HEADERS} ${LTRAPI_MODULE_SOURCES})

cmake_policy(POP)
