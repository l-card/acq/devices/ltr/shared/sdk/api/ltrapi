cmake_minimum_required(VERSION 2.8.12)

set(LTRAPI_MODULE_PROJECT       ltravrapi)
set(LTRAPI_MODULE_USE_TIMER     ON)
set(LTRAPI_MODULE_USE_CRC       OFF)
set(LTRAPI_MODULE_LIBS          ltr010api)
set(LTRAPI_MODULE_DESCR_ENG     "Library for update firmware for AVR microcontrollers in LTR modules")
set(LTRAPI_MODULE_DESCR_RUS     "Библиотека с функциями для обновления прошивок микроконтроллеров AVR в модулях LTR")


include_directories(${LTRAPI_SOURCE_DIR}/ltr010api)

include(${LTRAPI_MODULE_CMAKE_TEMPLATE})

