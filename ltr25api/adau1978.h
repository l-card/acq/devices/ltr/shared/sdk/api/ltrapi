#ifndef ADAU1978_H
#define ADAU1978_H


#define ADAU1978_REG_MPOWER                 0x00
#define ADAU1978_REG_PLLCTL                 0x01
#define ADAU1978_REG_BPOWER_SAI             0x04
#define ADAU1978_REG_SAI_CTRL0              0x05
#define ADAU1978_REG_SAI_CTRL1              0x06
#define ADAU1978_REG_SAI_CMAP12             0x07
#define ADAU1978_REG_SAI_CMAP34             0x08
#define ADAU1978_REG_SAI_OVERTEMP           0x09
#define ADAU1978_REG_POSTADC_GAIN1          0x0A
#define ADAU1978_REG_POSTADC_GAIN2          0x0B
#define ADAU1978_REG_POSTADC_GAIN3          0x0C
#define ADAU1978_REG_POSTADC_GAIN4          0x0D
#define ADAU1978_REG_MISC                   0x0E
#define ADAU1978_REG_ASDC_CLIP              0x19
#define ADAU1978_REG_DC_HPF_CAL             0x1A



#define ADAU1978_REGBIT_MPOWER_PWUP         (0x1UL << 0)
#define ADAU1978_REGBIT_MPOWER_SRST         (0x1UL << 7)


#define ADAU1978_REGBIT_PLLCTL_MCS          (0x7UL << 0)
#define ADAU1978_REGBIT_PLLCTL_CLKS         (0x1UL << 4)
#define ADAU1978_REGBIT_PLLCTL_PLLMUTE      (0x1UL << 6)
#define ADAU1978_REGBIT_PLLCTL_PLLLOCK      (0x1UL << 7)


#define ADAU1978_PLLCTL_MCS_256             (1 << 0)
#define ADAU1978_PLLCTL_MCS_384             (2 << 0)
#define ADAU1978_PLLCTL_MCS_512             (3 << 0)
#define ADAU1978_PLLCTL_MCS_768             (4 << 0)
#define ADAU1978_PLLCTL_MCS_128             (0 << 0)


#define ADAU1978_REGBIT_BPOWER_SAI_ADC_EN1  (0x1UL << 0)
#define ADAU1978_REGBIT_BPOWER_SAI_ADC_EN2  (0x1UL << 1)
#define ADAU1978_REGBIT_BPOWER_SAI_ADC_EN3  (0x1UL << 2)
#define ADAU1978_REGBIT_BPOWER_SAI_ADC_EN4  (0x1UL << 3)
#define ADAU1978_REGBIT_BPOWER_SAI_VREF_EN  (0x1UL << 4)
#define ADAU1978_REGBIT_BPOWER_SAI_LDO_EN   (0x1UL << 5)
#define ADAU1978_REGBIT_BPOWER_SAI_BCLKEDGE (0x1UL << 6)
#define ADAU1978_REGBIT_BPOWER_SAI_LRPOL    (0x1UL << 7)



#define ADAU1987_REGBIT_SAI_CTRL0_FS        (0x7UL << 0)
#define ADAU1987_REGBIT_SAI_CTRL0_SAI       (0x7UL << 3)
#define ADAU1987_REGBIT_SAI_CTRL0_SDATA_FMT (0x3UL << 6)

#define ADAU1987_SAI_CTRL0_FS_8_12          (0 << 0)
#define ADAU1987_SAI_CTRL0_FS_16_24         (1 << 0)
#define ADAU1987_SAI_CTRL0_FS_32_48         (2 << 0)
#define ADAU1987_SAI_CTRL0_FS_64_96         (3 << 0)
#define ADAU1987_SAI_CTRL0_FS_128_192       (4 << 0)

#define ADAU1987_SAI_CTRL0_SAI_STEREO       (0 << 3)
#define ADAU1987_SAI_CTRL0_SAI_TDM2         (1 << 3)
#define ADAU1987_SAI_CTRL0_SAI_TDM4         (2 << 3)
#define ADAU1987_SAI_CTRL0_SAI_TDM8         (3 << 3)
#define ADAU1987_SAI_CTRL0_SAI_TDM16        (4 << 3)

#define ADAU1987_SAI_CTRL0_SDATA_FMT_I2S    (0 << 6)
#define ADAU1987_SAI_CTRL0_SDATA_FMT_LJ     (1 << 6)
#define ADAU1987_SAI_CTRL0_SDATA_FMT_RJ24   (2 << 6)
#define ADAU1987_SAI_CTRL0_SDATA_FMT_RJ16   (3 << 6)





#define ADAU1987_REGBIT_SAI_CTRL1_MS            (0x1UL << 0) /* Master */
#define ADAU1987_REGBIT_SAI_CTRL1_BCLKRATE      (0x1UL << 1) /* 32/16 BCLK/ch */
#define ADAU1987_REGBIT_SAI_CTRL1_MSB           (0x1UL << 2) /* MSB/LSB first */
#define ADAU1987_REGBIT_SAI_CTRL1_LRMODE        (0x1UL << 3) /* LRCLK 50% or pulse */
#define ADAU1987_REGBIT_SAI_CTRL1_DATA_WIDTH    (0x1UL << 4) /* 24-bit or 16-bit */
#define ADAU1987_REGBIT_SAI_CTRL1_SLOT_WIDTH    (0x3UL << 5) /* Slot clk in TDM */
#define ADAU1987_REGBIT_SAI_CTRL1_SDATA_SEL     (0x1UL << 7) /* SDATAOUTx sel in TDM */


#define ADAU1978_REGBIT_SAI_OVERTEMP_OT         (0x1UL << 0)
#define ADAU1978_REGBIT_SAI_OVERTEMP_DRV_HIZ    (0x1UL << 3)
#define ADAU1978_REGBIT_SAI_OVERTEMP_DRV_C1     (0x1UL << 4)
#define ADAU1978_REGBIT_SAI_OVERTEMP_DRV_C2     (0x1UL << 5)
#define ADAU1978_REGBIT_SAI_OVERTEMP_DRV_C3     (0x1UL << 6)
#define ADAU1978_REGBIT_SAI_OVERTEMP_DRV_C4     (0x1UL << 7)

#define ADAU1978_REGBIT_MISC_DC_CAL             (0x1UL << 0)
#define ADAU1978_REGBIT_MISC_MMUTE              (0x1UL << 4)
#define ADAU1978_REGBIT_MISC_SUM_MODE           (0x3UL << 6)

#define ADAU1978_REGBIT_DC_HPF_CAL_HPF_C1       (0x1UL << 0)
#define ADAU1978_REGBIT_DC_HPF_CAL_HPF_C2       (0x1UL << 1)
#define ADAU1978_REGBIT_DC_HPF_CAL_HPF_C3       (0x1UL << 2)
#define ADAU1978_REGBIT_DC_HPF_CAL_HPF_C4       (0x1UL << 3)
#define ADAU1978_REGBIT_DC_HPF_CAL_SUB_C1       (0x1UL << 4)
#define ADAU1978_REGBIT_DC_HPF_CAL_SUB_C2       (0x1UL << 5)
#define ADAU1978_REGBIT_DC_HPF_CAL_SUB_C3       (0x1UL << 6)
#define ADAU1978_REGBIT_DC_HPF_CAL_SUB_C4       (0x1UL << 7)







#endif // ADAU1978_H
